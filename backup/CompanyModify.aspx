﻿<%@ Page Title="Company" MetaDescription="ข้อมูลบริษัท/โรงงาน/ผู้ประกอบการ" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CompanyModify.aspx.vb" Inherits="iLA.CompanyModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-config icon-gradient bg-success"></i>
                        </div>
                        <div><%: Title %>    
                            <div class="page-title-subheading"><%: MetaDescription %>   </div>
                        </div>
                    </div>
                </div>
            </div>   

    <section class="content">   
         <div class="main-card mb-3 card">
    <div class="card-header"><i class="header-icon lnr-user icon-gradient bg-primary">
            </i>ข้อมูลทั่วไป
            <div class="btn-actions-pane-right"><asp:HiddenField ID="hdCompanyUID" runat="server" />
            </div>
        </div>
        <div class="card-body">         

                 <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>Code</label>
                  <asp:TextBox ID="txtCompanyID" runat="server" cssclass="form-control text-center" ></asp:TextBox>   
                 </div> 
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>Name (ไทย)</label> <asp:TextBox ID="txtNameTH" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
                 </div> 
            </div>
                     <div class="col-md-5">
              <div class="form-group">
                <label>Name (English)</label> <asp:TextBox ID="txtNameEN" runat="server" cssclass="form-control" placeholder="English Name"></asp:TextBox>
                 </div>
            </div>
                     </div>
     <div class="row">
                      <div class="col-md-6">
              <div class="form-group">
                <label>Branch</label> <asp:TextBox ID="txtBranch" runat="server" cssclass="form-control" placeholder="สาขา"></asp:TextBox>
                 </div>
            </div>
 <div class="col-md-6">
              <div class="form-group">
                <label>TAX ID</label> <asp:TextBox ID="txtTaxID" runat="server" cssclass="form-control" placeholder="เลขประจำตัวผู้เสียภาษี"></asp:TextBox>
                 </div>
            </div> 

</div>
                <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>Address no.</label> <asp:TextBox ID="txtAddressNo" runat="server" cssclass="form-control" placeholder="บ้านเลขที่"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Lane</label> <asp:TextBox ID="txtLane" runat="server" cssclass="form-control" placeholder="ซอย"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Road</label> <asp:TextBox ID="txtRoad" runat="server" cssclass="form-control" placeholder="ถนน"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Sub District</label> <asp:TextBox ID="txtSubDistrict" runat="server" cssclass="form-control" placeholder="ตำบล/แขวง"></asp:TextBox>
                 </div>
            </div>
            </div>
                   <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>district</label> <asp:TextBox ID="txtDistrict" runat="server" cssclass="form-control" placeholder="อำเภอ"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Province</label> 
                  <asp:DropDownList ID="ddlProvince" runat="server" cssclass="form-control select2" Width="100%" ></asp:DropDownList>
                   
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Zip Code</label> <asp:TextBox ID="txtZipcode" runat="server" cssclass="form-control" placeholder="รหัสไปรษณีย์"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Country</label> 
                  <asp:DropDownList ID="ddlCountry" runat="server" cssclass="form-control select2" Width="100%" >
                      <asp:ListItem Selected="True" Value="TH">ประเทศไทย</asp:ListItem>
                  </asp:DropDownList>                   
                 </div>
            </div>     
            </div>
             <div class="row">                          
              <div class="col-md-12">
                <div class="form-group">
                     <div class="text-bold text-blue"><label>ชื่อและที่อยู่สำหรับออกใบเสร็จ</label> </div>
                   <asp:TextBox ID="txtAddressInvoice" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100"></asp:TextBox>
                </div>
              </div>
               
   </div> 
            <div class="row">             
          
               <div class="col-md-3">
              <div class="form-group">
                <label>Telephone</label> <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Fax</label> <asp:TextBox ID="txtFax" runat="server" cssclass="form-control" placeholder="แฟกซ์"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>E-mail</label> <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
                 </div>
            </div>
 
                     <div class="col-md-3">
              <div class="form-group">
                <label>Website</label> <asp:TextBox ID="txtWebsite" runat="server" cssclass="form-control" placeholder="เว็บไซต์"></asp:TextBox>
                 </div>
            </div> 
                  
            </div>         
        
                <div class="row">
                     <div class="col-md-3">
              <div class="form-group">
                <label>Status</label>  <asp:CheckBox ID="chkStatus" runat="server" Checked="True" Text="Active" />
                 </div>
            </div> 
                </div>
                
            </div> 
             <div class="card-footer  text-center"> 
               
              </div> 
</div>
  <div class="main-card mb-3 card">
    <div class="card-header"><i class="header-icon lnr-user icon-gradient bg-primary">
            </i>ข้อมูลกิจการ
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">     
              <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group mailbox-messages">
                                            <label>ประเภทธุรกิจ</label>
                                            <asp:DropDownList ID="ddlBusinessType" runat="server" CssClass="form-control select2">
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                   <div class="col-md-3">
                                        <div class="form-group mailbox-messages">
                                            <label>โรงงานจำพวก</label>
                                            <asp:DropDownList ID="ddlBusinessGoup" runat="server" CssClass="form-control select2">
                                                 <asp:ListItem Value="1">โรงงานจำพวก 1</asp:ListItem>
                                                <asp:ListItem Value="2">โรงงานจำพวก 2</asp:ListItem>
                                                <asp:ListItem Value="3">โรงงานจำพวก 3</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                   <div class="col-md-2">
              <div class="form-group">
                <label>จำนวนพนักงาน</label>
                  <asp:TextBox ID="txtEmployeeQTY" runat="server" cssclass="form-control text-center" ></asp:TextBox>   
                 </div> 
            </div>

                                    </div>
             <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mailbox-messages">
                                            <label>ขอบเขต</label>
                                            <asp:CheckBoxList ID="chkCategory" runat="server"   RepeatColumns="5">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    </div>
            <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Implementation phase</label>
                                            <asp:RadioButtonList ID="optPhase" runat="server"
                                                RepeatDirection="Vertical">
                                                <asp:ListItem Value="1">ระยะเตรียมการ,ประกอบการ</asp:ListItem>
                                                <asp:ListItem Value="2">ระยะก่อสร้างโรงงาน/ดัดแปลง/รื้อถอน</asp:ListItem>
                                                <asp:ListItem Value="3">ระยะการทดลองเดินเครื่องจักร</asp:ListItem>
                                                <asp:ListItem Value="4">ระยะประกอบกิจการ</asp:ListItem>
                                                <asp:ListItem Value="5">ระยะยกเลิก/เปลี่ยนแปลงการประกอบการ
                                                </asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>

                                    </div>
                                
            <div class="col-md-2">
              <div class="form-group">
                <label>User Limit</label>
                  <asp:TextBox ID="txtMaxPerson" runat="server" cssclass="form-control text-center" ></asp:TextBox>   
                 </div> 
            </div>

 </div> 
</div>
    </div> 

 <div class="main-card mb-3 card">
    <div class="card-header"><i class="header-icon lnr-user icon-gradient bg-primary">
            </i>Package การใช้งาน
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body"> 
             <div class="row"> 
               <div class="col-md-3">
              <div class="form-group">
                <label>Package</label>
                  <asp:DropDownList ID="ddlPackage" runat="server" cssclass="form-control select2" Width="100%" >
                      <asp:ListItem Selected="True" Value="1">Package 1</asp:ListItem>
                      <asp:ListItem Value="2">Package 2</asp:ListItem>
                  </asp:DropDownList>
                 </div>
            </div>
  </div> 
             <div class="card-footer  text-center"> 
               
              </div> 
</div>
     </div> 
     <div class="text-center">
  <asp:Button ID="cmdSave" cssclass="btn btn-primary" runat="server" Text="Save"  data-dismiss="modal" Width="120px" />  
                  <asp:Button ID="cmdDelete" cssclass="btn btn-danger" runat="server" Text="Delete"  data-dismiss="modal" Width="120px" />  
        </div>

      </section>
</asp:Content>