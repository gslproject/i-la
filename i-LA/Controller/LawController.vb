﻿Imports Microsoft.ApplicationBlocks.Data
Public Class LawController
    Inherits BaseClass
    Public ds As New DataSet

#Region "Legal"
    Public Function Legal_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_Get"))
        Return ds.Tables(0)
    End Function

    Public Function Legal_GetLast() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetLast"))
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetBySearch(Ministry As String, LawType As String, Lyear As String, BusinessType As String, Keyword As String, KeySearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetBySearch"), Ministry, LawType, Lyear, BusinessType, Keyword, KeySearch)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByLawType(PhaseUID As String, TypeUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByLawType"), PhaseUID, TypeUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByLawMaster(PhaseUID As String, MasterUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByLawMaster"), PhaseUID, MasterUID)
        Return ds.Tables(0)
    End Function

    Public Function Legal_GetByStatus(Status As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByStatus"), Status)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByCompany(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByCompany"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function

    Public Function Legal_GetForAssessment(CompanyUID As Integer, UserID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetForAssessment"), CompanyUID, UserID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetForApproval(CompanyUID As Integer, UserID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetForApproval"), CompanyUID, UserID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByPhaseUID(PhaseUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByPhaseUID"), PhaseUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByMinistryUID(PhaseUID As String, MinistryUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByMinistryUID"), PhaseUID, MinistryUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByYear(PhaseUID As String, LegalYear As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByYear"), PhaseUID, LegalYear)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByBusinessTypeUID(PhaseUID As String, BusinessTypeUID As String, FactoryTypeUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByBusinessTypeUID"), PhaseUID, BusinessTypeUID, FactoryTypeUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByKeyword(PhaseUID As String, sKeyword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByKeyword"), PhaseUID, sKeyword)
        Return ds.Tables(0)
    End Function

    Public Function Law_GetByCompany(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Law_GetByCompany"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function Law_GetByUser(UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Law_GetByUser"), UserID)
        Return ds.Tables(0)
    End Function
    Public Function Law_GetActive(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Law_GetActive"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function LegalYear_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalYear_Get"))
        Return ds.Tables(0)
    End Function
    Public Function LegalYear_Get4Select() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalYear_Get4Select"))
        Return ds.Tables(0)
    End Function
    Public Function Legal_CheckDuplicate(Code As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Legal_CheckDuplicate", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            If String.Concat(ds.Tables(0).Rows(0)(0)) <> "0" Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function Legal_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetDetail(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetDetail"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_Save(ByVal UID As Integer, ByVal Code As String, ByVal LegalName As String, ByVal TypeUID As Integer, ByVal LawMasterUID As Integer, AreaUID As String, IssueDate As String, StartDate As String, RegisDate As String, LYear As String, BusinessTypeUID As Integer, MinistryUID As Integer, Keyword As String, ByVal Descriptions As String, LawStatus As Integer, StatusFlag As String, ByVal UpdBy As Integer, FactoryTypeUID As Integer, FactoryGroupUID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Legal_Save"), UID, Code, LegalName, TypeUID, LawMasterUID, AreaUID, IssueDate, StartDate, RegisDate, LYear, BusinessTypeUID, MinistryUID, Keyword, Descriptions, LawStatus, StatusFlag, UpdBy, FactoryTypeUID, FactoryGroupUID)
    End Function

    Public Function Legal_SaveByImport(ByVal Code As String, ByVal LegalName As String, ByVal LawYear As Integer, ByVal TypeUID As Integer, ByVal LawMasterUID As Integer, AreaUID As String, FactoryGroupUID As Integer, BusinessTypeUID As Integer, FactoryTypeUID As Integer, MinistryUID As Integer, PhaseID As Integer, LawStatus As Integer, IssueDate As String, StartDate As String, RegisterDate As String, Keyword As String, ByVal Descriptions As String, ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Legal_SaveByImport"), Code, LegalName, LawYear, TypeUID, LawMasterUID, AreaUID, FactoryGroupUID, BusinessTypeUID, FactoryTypeUID, MinistryUID, PhaseID, LawStatus, IssueDate, StartDate, RegisterDate, Keyword, Descriptions, UpdBy)
    End Function


    Public Function Legal_Approve(ByVal UID As Integer, ActionStatus As String, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Legal_Approve"), UID, ActionStatus, UpdBy)
    End Function
    Public Function Legal_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Legal_Delete"), UID)
    End Function

    Public Function Practice_Get(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Practice_Get"), LegalUID)
        Return ds.Tables(0)
    End Function
    Public Function Practice_GetForAssessment(LegalUID As Integer, UserID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Practice_GetForAssessment"), LegalUID, UserID, PeriodID)
        Return ds.Tables(0)
    End Function

    Public Function Practice_GetAssessmentResult(ByVal CompanyUID As Integer, PeriodID As Integer, LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Practice_GetAssessmentResult"), CompanyUID, PeriodID, LegalUID)
        Return ds.Tables(0)
    End Function

    Public Function LegalAction_GetProgression(ByVal CompanyUID As Integer, PeriodID As Integer, LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalAction_GetProgression"), CompanyUID, PeriodID, LegalUID)
        Return ds.Tables(0)
    End Function

    Public Function Practice_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Practice_GetByUID"), UID)
        Return ds.Tables(0)
    End Function


    Public Function Practice_Delete(ByVal UID As Integer, LegalUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Practice_Delete"), UID, LegalUID)
    End Function
    Public Function Practice_Save(ByVal UID As Integer, LegalUID As Integer, Code As String, Description As String, Recurrence As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Practice_Save"), UID, LegalUID, Code, Description, Recurrence)
    End Function

    Public Function Practice_SaveByImport(LegalCode As String, PracticeCode As String, Description As String, Recurrence As String, CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Practice_SaveByImport"), LegalCode, PracticeCode, Description, Recurrence, CUser)
    End Function
    Public Function Practice_CheckDuplicate(LegalCode As String, Code As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Practice_CheckDuplicate", LegalCode, Code)
        If ds.Tables(0).Rows.Count > 0 Then
            If String.Concat(ds.Tables(0).Rows(0)(0)) <> "0" Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function Legal_GetUID(pCode As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetUID"), pCode)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function Legal_UpdateFile(ByVal UID As Integer, ByVal FilePath As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("Legal_UpdateFile"), UID, FilePath, CUser)
    End Function
#End Region

#Region "Legal Category Detail"
    Public Function LegalCategoryDetail_Delete(ByVal LegalUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalCategoryDetail_Delete"), LegalUID)
    End Function

    Public Function LegalCategoryDetail_Save(ByVal LegalUID As Integer, ByVal CateUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalCategoryDetail_Save"), LegalUID, CateUID, UpdBy)
    End Function
    Public Function LegalCategoryDetail_GetByLegalUID(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalCategoryDetail_GetByLegalUID"), LegalUID)
        Return ds.Tables(0)
    End Function

#End Region
#Region "Legal Area"
    Public Function LegalArea_Delete(ByVal LegalUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalArea_Delete"), LegalUID)
    End Function

    Public Function LegalArea_Save(ByVal LegalUID As Integer, ByVal AreaUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalArea_Save"), LegalUID, AreaUID, UpdBy)
    End Function
    Public Function LegalArea_GetByLegalUID(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalArea_GetByLegalUID"), LegalUID)
        Return ds.Tables(0)
    End Function

#End Region
#Region "Legal BusinessType"
    Public Function LegalBusinessType_Delete(ByVal LegalUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalBusinessType_Delete"), LegalUID)
    End Function

    Public Function LegalBusinessType_Save(ByVal LegalUID As Integer, ByVal BusinessTypeUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalBusinessType_Save"), LegalUID, BusinessTypeUID, UpdBy)
    End Function
    Public Function LegalBusinessType_GetByLegalUID(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalBusinessType_GetByLegalUID"), LegalUID)
        Return ds.Tables(0)
    End Function

#End Region

#Region "Legal FactoryType"
    Public Function LegalFactoryType_Delete(ByVal LegalUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalFactoryType_Delete"), LegalUID)
    End Function

    Public Function LegalFactoryType_Save(ByVal LegalUID As Integer, ByVal FactoryTypeUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalFactoryType_Save"), LegalUID, FactoryTypeUID, UpdBy)
    End Function
    Public Function LegalFactoryType_GetByLegalUID(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalFactoryType_GetByLegalUID"), LegalUID)
        Return ds.Tables(0)
    End Function

#End Region

#Region "Legal Keyword"
    Public Function LegalKeyword_Delete(ByVal LegalUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalKeyword_Delete"), LegalUID)
    End Function

    Public Function LegalKeyword_Save(ByVal LegalUID As Integer, ByVal KeywordUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalKeyword_Save"), LegalUID, KeywordUID, UpdBy)
    End Function
    Public Function LegalKeyword_GetByLegalUID(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalKeyword_GetByLegalUID"), LegalUID)
        Return ds.Tables(0)
    End Function

#End Region


    Public Function Law_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Law_Delete"), UID)
    End Function


    Public Function Law_UpdateFileName(ByVal UID As Integer, ByVal LawNo As String, ByVal Name As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Law_UpdateFileName"), UID, LawNo, Name)
    End Function

    Public Function LegalImage_Get(LawUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalImage_Get"), LawUID)
        Return ds.Tables(0)
    End Function

    Public Function LegalImage_Delete(ByVal UID As Integer, ByVal LawNo As String, ByVal Name As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalImage_Delete"), UID, LawNo, Name)
    End Function
    'Public Function Law_UpdateFileName(ByVal UID As Integer, ByVal LawNo As String, ByVal Name As String) As Integer
    '    Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Law_UpdateFileName"), UID, LawNo, Name)
    'End Function


    Public Function LegalRelease_GetByLegal(CompanyUID As Integer, LawUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalRelease_GetByLegal"), CompanyUID, LawUID)
        Return ds.Tables(0)
    End Function

    Public Function LegalRelease_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalRelease_GetByUID"), UID)
        Return ds.Tables(0)
    End Function

    Public Function LegalRelease_Save(ByVal UID As Integer, ByVal LawUID As Integer, ByVal PersonUID As Integer, CompanyUID As Integer, LevelID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalRelease_Save"), UID, LawUID, PersonUID, CompanyUID, LevelID)
    End Function

    Public Function LegalRelease_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalRelease_Delete"), UID)
    End Function

    Public Function Law_Problem_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Law_Problem_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function LawAction_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawAction_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function LawOwner_GetEmailAlert(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawOwner_GetEmailAlert"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function SendAlert_Save(ByVal CompanyUID As Integer, LawUID As Integer, PersonUID As Integer, email As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SendAlert_Save"), CompanyUID, LawUID, PersonUID, email, Status)
    End Function
    Public Function SendAlert_UpdateStatus(ByVal CompanyUID As Integer, LawUID As Integer, PersonUID As Integer, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SendAlert_UpdateStatus"), CompanyUID, LawUID, PersonUID, Status)
    End Function

    Public Function Legal_Reject(ByVal CompanyUID As Integer, PeriodID As Integer, ByVal LegalUID As Integer, ByVal Reason As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Legal_Reject"), CompanyUID, PeriodID, LegalUID, Reason, CUser)
    End Function

    Public Function LawAction_GetByLawUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawAction_GetByLawUID"), pUID)
        Return ds.Tables(0)
    End Function


    Public Function LawAction_Save(ByVal UID As Integer, ByVal CompanyUID As Integer, ByVal LawUID As Integer, ByVal ActionUID As String, ByVal OwnerUID As String, ByVal Duedate As String, ActionStatus As String, Comment As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LawAction_Save"), UID, CompanyUID, LawUID, ActionUID, OwnerUID, Duedate, ActionStatus, Comment)
    End Function

    Public Function Event_GetByAdmin(ByVal CompanyUID As Integer, ByVal PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Event_GetByAdmin"), CompanyUID, PersonUID)
        Return ds.Tables(0)
    End Function
    Public Function Event_GetByUser(ByVal CompanyUID As Integer, ByVal PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Event_GetByUser"), CompanyUID, PersonUID)
        Return ds.Tables(0)
    End Function


    Public Function RPT_Law_Assessment(ByVal CompanyUID As Integer, Bdate As Integer, Edate As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Law_Assessment"), CompanyUID, Bdate, Edate)
        Return ds.Tables(0)
    End Function

#Region "Law Master"
    Public Function LawMaster_GetActive() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawMaster_GetActive"))
        Return ds.Tables(0)
    End Function
    Public Function LawMaster_GetBySearch(KeySearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawMaster_GetBySearch"), KeySearch)
        Return ds.Tables(0)
    End Function

    Public Function LawMaster_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawMaster_GetByUID"), UID)
        Return ds.Tables(0)
    End Function
    Public Function LawMaster_Delete(pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LawMaster_Delete", pID)
    End Function

    Public Function LawMaster_Save(UID As Integer, Name As String, Sort As Integer, StatusFlag As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LawMaster_Save", UID, Name, Sort, StatusFlag)
    End Function

#End Region

#Region "Law Type"

    Public Function LawType_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawType_Get"))
        Return ds.Tables(0)
    End Function
    Public Function LawType_GetBySearch(KeySearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawType_GetBySearch"), KeySearch)
        Return ds.Tables(0)
    End Function

    Public Function LawType_Get4Select() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawType_Get4Select"))
        Return ds.Tables(0)
    End Function
    Public Function LawType_GetByLegalID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawType_GetByLegalID"), UID)
        Return ds.Tables(0)
    End Function

    Public Function LawType_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawType_GetByUID"), UID)
        Return ds.Tables(0)
    End Function
    Public Function LawType_Delete(pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LawType_Delete", pID)
    End Function
    Public Function LawType_Save(UID As Integer, Name As String, Sort As Integer, StatusFlag As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LawType_Save", UID, Name, Sort, StatusFlag)
    End Function
#End Region

#Region "Law Category"
    Public Function LawCategory_GetByLegalID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawCategory_GetByLegalID"), UID)
        Return ds.Tables(0)
    End Function
    Public Function LawCategory_GetBySearch(KeySearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawCategory_GetBySearch"), KeySearch)
        Return ds.Tables(0)
    End Function

    Public Function LawCategory_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LawCategory_GetByUID"), UID)
        Return ds.Tables(0)
    End Function
    Public Function LawCategory_Delete(pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LawCategory_Delete", pID)
    End Function
    Public Function LawCategory_Save(UID As Integer, Name As String, Sort As Integer, StatusFlag As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LawCategory_Save", UID, Name, Sort, StatusFlag)
    End Function
#End Region

#Region "Law Release"
    Public Function LawRelease_Save(ByVal LegalUID As Integer, ByVal PracticeUID As Integer, CompanyUID As Integer, ByVal UserID As Integer, PeriodID As Integer, StatusFlag As String, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LawRelease_Save"), LegalUID, PracticeUID, CompanyUID, UserID, PeriodID, StatusFlag, UpdBy)
    End Function
    Public Function LawRelease_Delete(pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LawRelease_Delete", pID)
    End Function

#End Region

#Region "Law Action"
    Public Function LegalAction_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalAction_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function LegalAction_Save(ByVal UID As Integer, ByVal LegalUID As Integer, ByVal PracticeUID As Integer, CompanyUID As Integer, ByVal UserID As Integer, PeriodID As Integer, Descriptions As String, ByVal AsmResult As String, FilePath As String, MUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalAction_Save"), UID _
           , LegalUID _
           , PracticeUID _
           , CompanyUID _
           , UserID _
           , PeriodID _
           , Descriptions _
           , AsmResult _
           , FilePath _
           , MUser)
    End Function

    Public Function LegalAction_UpdateActionStatus(ByVal UID As Integer, ApproveStatus As String, ActionStatus As String, Comment As String, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalAction_UpdateActionStatus"), UID, ApproveStatus, ActionStatus, Comment, UpdBy)
    End Function
    Public Function LegalAction_UpdateApproveStatus(ByVal UID As Integer, ApproveStatus As String, ActionStatus As String, Comment As String, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalAction_UpdateApproveStatus"), UID, ApproveStatus, ActionStatus, Comment, UpdBy)
    End Function

    Public Function LegalAction_GetForApproval(LegalUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalAction_GetForApproval"), LegalUID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function LegalAction_GetByAsmStatus(CompanyUID As Integer, PeriodID As Integer, AsmStatus As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalAction_GetByAsmStatus"), CompanyUID, PeriodID, AsmStatus)
        Return ds.Tables(0)
    End Function

#End Region


#Region "Legal Phase"
    Public Function LegalPhase_Delete(ByVal LegalUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalPhase_Delete"), LegalUID)
    End Function

    Public Function LegalPhase_Save(ByVal LegalUID As Integer, ByVal PhaseUID As Integer, ByVal UpdBy As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LegalPhase_Save"), LegalUID, PhaseUID, UpdBy)
    End Function
    Public Function LegalPhase_GetByLegalUID(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalPhase_GetByLegalUID"), LegalUID)
        Return ds.Tables(0)
    End Function

#End Region


End Class
