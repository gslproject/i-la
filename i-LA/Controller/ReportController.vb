﻿Imports Microsoft.ApplicationBlocks.Data

Public Class ReportController
    Inherits BaseClass
    Dim ds As New DataSet
#Region "Law"
    Public Function RPT_LegalAction_Assessment(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_LegalAction_Assessment"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function

    Public Function RPT_LegalAction_Assessment_ForChart(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_LegalAction_Assessment_ForChart"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_LegalAction_AssessmentByDepartment_ForChart(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_LegalAction_AssessmentByDepartment_ForChart"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Legal_ByStatus_ForChart(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Legal_ByStatus_ForChart"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Legal_ByType_ForChart(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Legal_ByType_ForChart_Package1"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Legal_ByType_ForChart(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Legal_ByType_ForChart"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Practice_GetByLegal(LegalUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Practice_GetByLegal"), LegalUID)
        Return ds.Tables(0)
    End Function

    Public Function Legal_GetLastMonthCountByType() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetLastMonthCountByType"))
        Return ds.Tables(0)
    End Function

    Public Function Legal_GetCountByType() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Legal_GetCountByType"))
        Return ds.Tables(0)
    End Function

    Public Function Legal_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Legal_Get"))
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetByCompany(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Legal_GetByCompany"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function Legal_GetLast() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Legal_GetLast"))
        Return ds.Tables(0)
    End Function

    Public Function LegalAction_GetRelate(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalAction_GetRelate"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function LegalAction_GetByAssessmented(CompanyUID As Integer, PeriodID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalAction_GetByAssessmented"), CompanyUID, PeriodID)
        Return ds.Tables(0)
    End Function
    Public Function LegalAction_GetByAsmResult(CompanyUID As Integer, PeriodID As Integer, Status As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LegalAction_GetByAsmResult"), CompanyUID, PeriodID, Status)
        Return ds.Tables(0)
    End Function
#End Region


End Class
