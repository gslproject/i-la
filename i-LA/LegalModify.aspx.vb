﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class LegalModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LawController
    Dim ctlM As New MasterController
    Dim ctlU As New UserController
    Private Const UploadDirectory As String = "~/imgLegal/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            grdPractice.PageIndex = 0
            cmdApprove.Visible = False
            cmdDelete.Visible = False
            pnPractice.Visible = False
            'Request.Cookies("iLaw")("Legalimg")="blankimage.png"
            'Request.Cookies("iLaw")("Legalimg1") = ""
            'Request.Cookies("iLaw")("picname1") = ""
            'Request.Cookies("iLaw")("Legalimg2") = ""
            'Request.Cookies("iLaw")("picname2") = ""
            'Request.Cookies("iLaw")("Legalimg3") = ""
            'Request.Cookies("iLaw")("picname3") = ""
            LoadLawType()
            LoadLawMaster()
            'LoadOrganize()
            LoadArea()
            'LoadCategory()
            LoadPhase()
            LoadKeyword()
            LoadFactoryType()
            LoadBusinessType()
            LoadMinistry()
            LoadRecurence()

            If Request("lid") Is Nothing Then
                'txtCode.Text = ctlM.RunningNumber_New(CODE_LEGAL)
            Else
                LoadLegalData()
            End If

            'If Request.Cookies("iLaw")("ROLE_ADM") = False And Request.Cookies("iLaw")("ROLE_SPA") = False Then
            '    ddlOrganize.Enabled = False
            'Else
            '    ddlOrganize.Enabled = True
            'End If

        End If

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")

    End Sub
    Private Sub LoadLawType()
        ddlType.DataSource = ctlL.LawType_Get
        ddlType.DataTextField = "Name"
        ddlType.DataValueField = "UID"
        ddlType.DataBind()
    End Sub
    Private Sub LoadLawMaster()
        ddlLawMaster.DataSource = ctlL.LawMaster_GetActive()
        ddlLawMaster.DataTextField = "Name"
        ddlLawMaster.DataValueField = "UID"
        ddlLawMaster.DataBind()
    End Sub
    'Private Sub LoadOrganize()
    '    ddlOrganize.DataSource = ctlM.Organize_GetActive()
    '    ddlOrganize.DataTextField = "Name"
    '    ddlOrganize.DataValueField = "UID"
    '    ddlOrganize.DataBind()
    'End Sub

    Private Sub LoadArea()
        'ddlArea.DataSource = ctlM.Area_Get()
        'ddlArea.DataTextField = "AreaName"
        'ddlArea.DataValueField = "AreaUID"
        'ddlArea.DataBind()

        chkArea.DataSource = ctlM.Area_Get()
        chkArea.DataTextField = "AreaName"
        chkArea.DataValueField = "AreaUID"
        chkArea.DataBind()

    End Sub
    Private Sub LoadFactoryType()
        'ddlFactoryType.DataSource = ctlM.FactoryType_Get()
        'ddlFactoryType.DataTextField = "Name"
        'ddlFactoryType.DataValueField = "UID"
        'ddlFactoryType.DataBind()

        chkFactoryType.DataSource = ctlM.FactoryType_GetForLaw()
        chkFactoryType.DataTextField = "Name"
        chkFactoryType.DataValueField = "UID"
        chkFactoryType.DataBind()

    End Sub
    'Private Sub LoadCategory()
    '    chkCategory.DataSource = ctlM.Category_GetActive()
    '    chkCategory.DataTextField = "Name"
    '    chkCategory.DataValueField = "UID"
    '    chkCategory.DataBind()
    'End Sub
    Private Sub LoadKeyword()
        chkKeyword.DataSource = ctlM.Keyword_Get()
        chkKeyword.DataTextField = "Name"
        chkKeyword.DataValueField = "UID"
        chkKeyword.DataBind()
    End Sub
    Private Sub LoadPhase()
        chkPhase.DataSource = ctlM.Phase_Get()
        chkPhase.DataTextField = "PhaseName"
        chkPhase.DataValueField = "UID"
        chkPhase.DataBind()
    End Sub
    Private Sub LoadBusinessType()
        'ddlBusinessType.DataSource = ctlM.BusinessType_Get()
        'ddlBusinessType.DataTextField = "Name"
        'ddlBusinessType.DataValueField = "UID"
        'ddlBusinessType.DataBind()

        chkBusinessType.DataSource = ctlM.BusinessType_GetForLaw()
        chkBusinessType.DataTextField = "Name"
        chkBusinessType.DataValueField = "UID"
        chkBusinessType.DataBind()

    End Sub
    Private Sub LoadMinistry()
        ddlMinistry.DataSource = ctlM.Ministry_Get()
        ddlMinistry.DataTextField = "Name"
        ddlMinistry.DataValueField = "UID"
        ddlMinistry.DataBind()
    End Sub
    Private Sub LoadRecurence()
        Dim ctlR As New ReferenceValueController
        ddlRecurence.DataSource = ctlR.ReferenceValue_GetByDomainCode("RECUR")
        ddlRecurence.DataTextField = "DisplayName"
        ddlRecurence.DataValueField = "ValueCode"
        ddlRecurence.DataBind()
    End Sub
    Private Sub LoadLegalData()
        dt = ctlL.Legal_GetByUID(Request("lid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdLegalUID.Value = String.Concat(.Item("UID"))
                txtCode.Text = String.Concat(.Item("Code"))
                txtName.Text = String.Concat(.Item("LegalName"))
                ddlLawMaster.SelectedValue = String.Concat(.Item("LawMasterUID"))
                'ddlOrganize.SelectedValue = String.Concat(.Item("OrganizeUID"))
                'ddlArea.SelectedValue = String.Concat(.Item("AreaUID"))
                ddlType.SelectedValue = String.Concat(.Item("TypeUID"))
                txtDescription.Text = String.Concat(.Item("Descriptions"))
                'chkPhase.SelectedValue = String.Concat(.Item("PhaseUID"))
                txtIssueDate.Text = DisplayShortDateTH(String.Concat(.Item("IssueDate")))
                txtStartDate.Text = DisplayShortDateTH(String.Concat(.Item("StartDate")))
                txtRegisDate.Text = DisplayShortDateTH(String.Concat(.Item("RegisDate")))

                txtYear.Text = String.Concat(.Item("LegalYear"))
                'ddlBusinessType.SelectedValue = String.Concat(.Item("BusinessTypeUID"))
                'ddlFactoryType.SelectedValue = String.Concat(.Item("FactoryTypeUID"))
                If String.Concat(.Item("FactoryGroupUID")) = "1" Then
                    optFactoryGroup.Checked = True
                Else
                    optFactoryGroup.Checked = False
                End If
                'optFactoryGroup.SelectedValue = String.Concat(.Item("FactoryGroupUID"))
                ddlMinistry.SelectedValue = String.Concat(.Item("MinistryUID"))

                optLawStatus.SelectedValue = String.Concat(.Item("LegalStatus"))

                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(.Item("StatusFlag")))

                hlnkDoc.Text = DBNull2Str(dt.Rows(0)("filePath"))
                hlnkDoc.NavigateUrl = DocumentLegal & "/" & DBNull2Str(dt.Rows(0)("filePath"))
                hlnkDoc.Visible = True

                'LoadLegalImage(DBNull2Zero(.Item("UID")))
                'LoadLegalCategoryDetail(String.Concat(.Item("UID")))
                If String.Concat(.Item("AreaUID")) = "0" Then
                    chkAllArea.Checked = True
                    chkArea.Enabled = False
                Else
                    chkArea.Enabled = True
                    chkAllArea.Checked = False
                    LoadLegalArea(String.Concat(.Item("UID")))
                End If

                If String.Concat(.Item("BusinessTypeUID")) = "0" Then
                    chkAllBusinessType.Checked = True
                    chkBusinessType.Enabled = False
                Else
                    chkBusinessType.Enabled = True
                    chkAllBusinessType.Checked = False
                    LoadLegalBusinessType(String.Concat(.Item("UID")))
                End If

                If String.Concat(.Item("FactoryTypeUID")) = "0" Then
                    chkAllFactoryType.Checked = True
                    chkFactoryType.Enabled = False
                Else
                    chkFactoryType.Enabled = True
                    chkAllFactoryType.Checked = False
                    LoadLegalFactoryType(String.Concat(.Item("UID")))
                End If

                LoadLegalKeyword(String.Concat(.Item("UID")))
                LoadLegalPhase(String.Concat(.Item("UID")))

                pnPractice.Visible = True
                LoadPracticeToGrid()

                If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = True Then
                    cmdSave.Visible = True
                    cmdDelete.Visible = True
                Else
                    cmdSave.Visible = False
                    cmdDelete.Visible = False
                End If
                If ctlU.User_IsApprover(Request.Cookies("iLaw")("UserID")) = True Then
                    cmdApprove.Visible = True
                Else
                    cmdApprove.Visible = False
                End If
            End With
        Else
        End If
    End Sub

    'Private Sub LoadLegalCategoryDetail(LUID As Integer)
    '    Dim dtC As New DataTable
    '    dtC = ctlL.LegalCategoryDetail_GetByLegalUID(LUID)
    '    If dtC.Rows.Count > 0 Then
    '        chkCategory.ClearSelection()
    '        For i = 0 To chkCategory.Items.Count - 1
    '            For n = 0 To dtC.Rows.Count - 1
    '                If chkCategory.Items(i).Value = dtC.Rows(n)("CategoryUID") Then
    '                    chkCategory.Items(i).Selected = True
    '                End If
    '            Next
    '        Next
    '    End If
    'End Sub
    Private Sub LoadLegalArea(LUID As Integer)
        Dim dtC As New DataTable
        dtC = ctlL.LegalArea_GetByLegalUID(LUID)
        If dtC.Rows.Count > 0 Then
            chkArea.ClearSelection()
            For i = 0 To chkArea.Items.Count - 1
                For n = 0 To dtC.Rows.Count - 1
                    If chkArea.Items(i).Value = dtC.Rows(n)("AreaUID") Then
                        chkArea.Items(i).Selected = True
                    End If
                Next
            Next
        End If
    End Sub

    Private Sub LoadLegalBusinessType(LUID As Integer)
        Dim dtC As New DataTable
        dtC = ctlL.LegalBusinessType_GetByLegalUID(LUID)
        If dtC.Rows.Count > 0 Then
            chkBusinessType.ClearSelection()
            For i = 0 To chkBusinessType.Items.Count - 1
                For n = 0 To dtC.Rows.Count - 1
                    If chkBusinessType.Items(i).Value = dtC.Rows(n)("BusinessTypeUID") Then
                        chkBusinessType.Items(i).Selected = True
                    End If
                Next
            Next
        End If
    End Sub
    Private Sub LoadLegalFactoryType(LUID As Integer)
        Dim dtC As New DataTable
        dtC = ctlL.LegalFactoryType_GetByLegalUID(LUID)
        If dtC.Rows.Count > 0 Then
            chkFactoryType.ClearSelection()
            For i = 0 To chkFactoryType.Items.Count - 1
                For n = 0 To dtC.Rows.Count - 1
                    If chkFactoryType.Items(i).Value = dtC.Rows(n)("FactoryTypeUID") Then
                        chkFactoryType.Items(i).Selected = True
                    End If
                Next
            Next
        End If
    End Sub

    Private Sub LoadLegalKeyword(LUID As Integer)
        Dim dtC As New DataTable
        dtC = ctlL.Legalkeyword_GetByLegalUID(LUID)
        If dtC.Rows.Count > 0 Then
            chkKeyword.ClearSelection()
            For i = 0 To chkKeyword.Items.Count - 1
                For n = 0 To dtC.Rows.Count - 1
                    If chkKeyword.Items(i).Value = dtC.Rows(n)("KeywordUID") Then
                        chkKeyword.Items(i).Selected = True
                    End If
                Next
            Next
        End If
    End Sub
    Private Sub LoadLegalPhase(LUID As Integer)
        Dim dtC As New DataTable
        dtC = ctlL.LegalPhase_GetByLegalUID(LUID)
        If dtC.Rows.Count > 0 Then
            chkPhase.ClearSelection()
            For i = 0 To chkPhase.Items.Count - 1
                For n = 0 To dtC.Rows.Count - 1
                    If chkPhase.Items(i).Value = dtC.Rows(n)("PhaseUID") Then
                        chkPhase.Items(i).Selected = True
                    End If
                Next
            Next
        End If
    End Sub

    'Private Sub LoadLegalImage(LegalUID As Integer)
    '    Dim dtPic As New DataTable
    '    Dim str() As String
    '    dtPic = ctlL.LegalImage_Get(LegalUID)
    '    If dtPic.Rows.Count > 0 Then
    '        For i = 0 To dtPic.Rows.Count - 1

    '            If String.Concat(dtPic.Rows(i)("ImagePath")) <> "" Then
    '                str = Split(dtPic.Rows(i)("ImagePath"), ".")

    '                Select Case Right(str(0), 1)
    '                    Case "1"
    '                        Request.Cookies("iLaw")("Legalimg1") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                        Request.Cookies("iLaw")("picname1") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                    Case "2"
    '                        Request.Cookies("iLaw")("Legalimg2") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                        Request.Cookies("iLaw")("picname2") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                    Case "3"
    '                        Request.Cookies("iLaw")("Legalimg3") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                        Request.Cookies("iLaw")("picname3") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                End Select
    '            End If
    '        Next
    '    End If

    'End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        'Dim BDate As String = ddlDay.SelectedValue
        'BDate = ddlYear.SelectedValue & ddlMonth.SelectedValue & BDate
        'If txtGPAX.Text <> "" Then
        '    If Not IsNumeric(txtGPAX.Text) Then
        '        DisplayMessage(Me.Page, "GPAX ไม่ถูกต้อง")
        '        Exit Sub
        '    End If
        'End If
        If txtCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ระบุ รหัสกฏหมาย');", True)
            Exit Sub
        Else
            If StrNull2Zero(hdLegalUID.Value) = 0 Then
                If ctlL.Legal_CheckDuplicate(txtCode.Text) Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านตั้งรหัสกฎหมายซ้ำ /รหัสกฏหมายนี้มีในฐานข้อมูลแล้ว');", True)
                    Exit Sub
                End If
            End If
        End If

        If txtName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ระบุ Legal name');", True)
            Exit Sub
        End If

        'If hlnkDoc.Text = "" Then
        '    If FileUpload1.PostedFile.FileName = "" Then
        '        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้เลือกไฟล์สำหรับอัพโหลด');", True)
        '        Exit Sub
        '    End If
        'End If

        Dim sKeyword As String = ""

        For i = 0 To chkKeyword.Items.Count - 1
            If chkKeyword.Items(i).Selected Then
                sKeyword = sKeyword + chkKeyword.Items(i).Text & "|"
            End If
        Next
        sKeyword = Left(sKeyword, sKeyword.Length - 1)

        Dim sPhase As String = ""
        For i = 0 To chkPhase.Items.Count - 1
            If chkPhase.Items(i).Selected Then
                sPhase = sPhase + chkPhase.Items(i).Value.ToString() + ","
            End If

        Next
        sPhase = Left(sPhase, sPhase.Length - 1)
        Dim FactoryGroup As Integer
        If optFactoryGroup.Checked = True Then
            FactoryGroup = 1
        Else
            FactoryGroup = 0
        End If

        Dim AreaUID, FactoryTypeUID, BusinessTypeUID As Integer
        If chkAllArea.Checked Then
            AreaUID = 0
        Else
            AreaUID = 1
        End If
        If chkAllBusinessType.Checked Then
            BusinessTypeUID = 0
        Else
            BusinessTypeUID = 1
        End If
        If chkAllFactoryType.Checked Then
            FactoryTypeUID = 0
        Else
            FactoryTypeUID = 1
        End If

        ctlL.Legal_Save(StrNull2Zero(hdLegalUID.Value), txtCode.Text, txtName.Text, StrNull2Zero(ddlType.SelectedValue), StrNull2Zero(ddlLawMaster.SelectedValue), AreaUID, txtIssueDate.Text, txtStartDate.Text, txtRegisDate.Text, StrNull2Zero(txtYear.Text), BusinessTypeUID, StrNull2Zero(ddlMinistry.SelectedValue), sKeyword, txtDescription.Text, StrNull2Zero(optLawStatus.SelectedValue), ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("iLaw")("userid"), FactoryTypeUID, FactoryGroup)

        Dim LegalUID As Integer

        If StrNull2Zero(hdLegalUID.Value) = 0 Then
            'ctlM.RunningNumber_Update(CODE_LEGAL)
            LegalUID = ctlL.Legal_GetUID(txtCode.Text)
        Else
            LegalUID = hdLegalUID.Value
        End If

        'ctlL.LegalCategoryDetail_Delete(LegalUID)

        'For i = 0 To chkCategory.Items.Count - 1
        '    If chkCategory.Items(i).Selected Then
        '        ctlL.LegalCategoryDetail_Save(LegalUID, chkCategory.Items(i).Value, Request.Cookies("iLaw")("userid"))
        '    End If
        'Next
        ctlL.LegalArea_Delete(LegalUID)
        If chkAllArea.Checked Then
            ctlL.LegalArea_Save(LegalUID, 0, Request.Cookies("iLaw")("userid"))
        Else
            For i = 0 To chkArea.Items.Count - 1
                If chkArea.Items(i).Selected Then
                    ctlL.LegalArea_Save(LegalUID, chkArea.Items(i).Value, Request.Cookies("iLaw")("userid"))
                End If
            Next
        End If

        ctlL.LegalBusinessType_Delete(LegalUID)
        If chkAllBusinessType.Checked Then
            ctlL.LegalBusinessType_Save(LegalUID, 0, Request.Cookies("iLaw")("userid"))
        Else
            For i = 0 To chkBusinessType.Items.Count - 1
                If chkBusinessType.Items(i).Selected Then
                    ctlL.LegalBusinessType_Save(LegalUID, chkBusinessType.Items(i).Value, Request.Cookies("iLaw")("userid"))
                End If
            Next
        End If

        ctlL.LegalFactoryType_Delete(LegalUID)
        If chkAllFactoryType.Checked Then
            ctlL.LegalFactoryType_Save(LegalUID, 0, Request.Cookies("iLaw")("userid"))
        Else
            For i = 0 To chkFactoryType.Items.Count - 1
                If chkFactoryType.Items(i).Selected Then
                    ctlL.LegalFactoryType_Save(LegalUID, chkFactoryType.Items(i).Value, Request.Cookies("iLaw")("userid"))
                End If
            Next
        End If

        ctlL.LegalKeyword_Delete(LegalUID)

        For i = 0 To chkKeyword.Items.Count - 1
            If chkKeyword.Items(i).Selected Then
                ctlL.LegalKeyword_Save(LegalUID, chkKeyword.Items(i).Value, Request.Cookies("iLaw")("userid"))
            End If
        Next

        ctlL.LegalPhase_Delete(LegalUID)

        For i = 0 To chkPhase.Items.Count - 1
            If chkPhase.Items(i).Selected Then
                ctlL.LegalPhase_Save(LegalUID, chkPhase.Items(i).Value, Request.Cookies("iLaw")("userid"))
            End If
        Next

        Dim f_Path As String 'f_Extension,f_Icon,, cate 
        'f_Icon = ""
        'f_Extension = ""
        f_Path = ""
        'cate = ""

        If FileUpload1.HasFile Then
            f_Path = txtCode.Text & Path.GetExtension(FileUpload1.PostedFile.FileName)
            'f_Extension = Path.GetExtension(FileUpload1.PostedFile.FileName)
            'Select Case f_Extension.ToLower()
            '    Case ".pdf"
            '        f_Icon = "pdf.png"
            '    Case ".doc", ".docx"
            '        f_Icon = "word.png"
            '    Case ".jpg", ".jpeg", ".png"
            '        f_Icon = "jpg.png"
            '    Case ".ppt", ".pptx"
            '        f_Icon = "ppt.png"
            '    Case Else
            '        f_Icon = ""
            'End Select

            ctlL.Legal_UpdateFile(LegalUID, f_Path, Request.Cookies("iLaw")("userid"))
            UploadFile(FileUpload1, f_Path)

        End If


        'Dim imgName As String

        'Dim LegalUID As Integer
        'LegalUID = ctlE.Legal_GetUIDByCode(Request.Cookies("iLaw")("LoginCompanyUID"), txtLegalNo.Text)

        'imgName = txtLegalNo.Text & Format(Request.Cookies("iLaw")("LoginCompanyUID"), "00#")

        'If Not Request.Cookies("iLaw")("picname1") Is Nothing And Request.Cookies("iLaw")("picname1") <> "" Then
        '    ctlE.Legal_UpdateFileName(LegalUID, txtLegalNo.Text, imgName & "01" & Path.GetExtension(Request.Cookies("iLaw")("picname1")))
        '    'UploadFile(FileUpload1, lblPersonID.Text & Path.GetExtension(FileUpload1.PostedFile.FileName))
        '    RenamePictureName(imgName & "01" & Path.GetExtension(Request.Cookies("iLaw")("picname1")), Request.Cookies("iLaw")("picname1"))
        'Else
        '    ctlE.LegalImage_Delete(LegalUID, txtLegalNo.Text, imgName & "01" & Path.GetExtension(Request.Cookies("iLaw")("picname1")))
        'End If

        'If Not Request.Cookies("iLaw")("picname2") Is Nothing And Request.Cookies("iLaw")("picname2") <> "" Then
        '    ctlE.Legal_UpdateFileName(LegalUID, txtLegalNo.Text, imgName & "02" & Path.GetExtension(Request.Cookies("iLaw")("picname2")))
        '    'UploadFile(FileUpload1, lblPersonID.Text & Path.GetExtension(FileUpload1.PostedFile.FileName))
        '    RenamePictureName(imgName & "02" & Path.GetExtension(Request.Cookies("iLaw")("picname2")), Request.Cookies("iLaw")("picname2"))
        'Else
        '    ctlE.LegalImage_Delete(LegalUID, txtLegalNo.Text, imgName & "02" & Path.GetExtension(Request.Cookies("iLaw")("picname2")))
        'End If

        'If Not Request.Cookies("iLaw")("picname3") Is Nothing And Request.Cookies("iLaw")("picname3") <> "" Then
        '    ctlE.Legal_UpdateFileName(LegalUID, txtLegalNo.Text, imgName & "03" & Path.GetExtension(Request.Cookies("iLaw")("picname3")))
        '    'UploadFile(FileUpload1, lblPersonID.Text & Path.GetExtension(FileUpload1.PostedFile.FileName))
        '    RenamePictureName(imgName & "03" & Path.GetExtension(Request.Cookies("iLaw")("picname3")), Request.Cookies("iLaw")("picname3"))
        'Else
        '    ctlE.LegalImage_Delete(LegalUID, txtLegalNo.Text, imgName & "03" & Path.GetExtension(Request.Cookies("iLaw")("picname3")))
        'End If


        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Legal", "บันทึก/แก้ไข Legal :{LegalUID=" & hdLegalUID.Value & "}{LegalCode=" & txtCode.Text & "}", "")

        If Not hdLegalUID.Value = Nothing Then
            cmdSave.Visible = True
            cmdDelete.Visible = True
        Else
            cmdSave.Visible = False
            cmdDelete.Visible = False
        End If
        If ctlU.User_IsApprover(Request.Cookies("iLaw")("UserID")) = True Then
            cmdApprove.Visible = True
        Else
            cmdApprove.Visible = False
        End If

        pnPractice.Visible = True
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        'Response.Redirect("Legal.aspx?ActionType=tsk")
    End Sub

    'Private Sub RenamePictureName(fname As String, pname As String)

    '    If pname <> "" Then
    '        Dim Path As String = Server.MapPath(UploadDirectory)
    '        Dim Fromfile As String = Path + pname
    '        Dim Tofile As String = Path + fname


    '        Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & PictureLegal & "/" & fname))
    '        If fname <> pname Then
    '            If objfile.Exists Then
    '                objfile.Delete()
    '            End If

    '            File.Move(Fromfile, Tofile)
    '        End If
    '    End If
    'End Sub
    Sub UploadFile(ByVal Fileupload As Object, sName As String)
        'Dim FileFullName As String = Fileupload.PostedFile.FileName
        'Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As FileInfo = New FileInfo(Server.MapPath(DocumentLegal & "/" & sName))
        If objfile.Exists Then
                objfile.Delete()
            End If


        'If FileNameInfo <> "" Then
        Fileupload.PostedFile.SaveAs(Server.MapPath(DocumentLegal & "/" & sName))
        'End If




        'objfile = Nothing
    End Sub


    'Protected Sub UploadControl_FileUploadComplete1(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)

    '    If chkFileExist(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname1"))) Then
    '        File.Delete(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname1")))
    '    End If

    '    e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory, Path.GetRandomFileName())
    '    Request.Cookies("iLaw")("picname1") = e.CallbackData
    'End Sub

    'Protected Sub UploadControl_FileUploadComplete2(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)

    '    If chkFileExist(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname2"))) Then
    '        File.Delete(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname2")))
    '    End If

    '    e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory, Path.GetRandomFileName())
    '    Request.Cookies("iLaw")("picname2") = e.CallbackData
    'End Sub

    'Protected Sub UploadControl_FileUploadComplete3(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)

    '    If chkFileExist(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname3"))) Then
    '        File.Delete(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname3")))
    '    End If

    '    e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory, Path.GetRandomFileName())
    '    Request.Cookies("iLaw")("picname3") = e.CallbackData
    'End Sub

    Protected Function SavePostedFile(ByVal uploadedFile As UploadedFile, ByVal UploadPath As String, ByVal UploadFileName As String) As String
        If (Not uploadedFile.IsValid) Then
            Return String.Empty
        End If

        Dim fileName As String = Path.ChangeExtension(UploadFileName, ".jpg")

        Dim fullFileName As String = CombinePath(UploadPath, fileName)
        Using original As Image = Image.FromStream(uploadedFile.FileContent)
            Using thumbnail As Image = New ImageThumbnailCreator(original).CreateImageThumbnail(New Size(350, 350))
                ImageUtils.SaveToJpeg(CType(thumbnail, Bitmap), fullFileName)
            End Using
        End Using
        UploadingUtils.RemoveFileWithDelay(fileName, fullFileName, 5)
        Return fileName
    End Function
    Protected Function CombinePath(ByVal UploadPath As String, ByVal fileName As String) As String
        Return Path.Combine(Server.MapPath(UploadPath), fileName)
    End Function

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        'ctlE.Legal_Delete(StrNull2Zero(hdLegalUID.Value))
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบ Legal เรียบร้อย');", True)
    End Sub

    'Protected Sub cmdApprove_Click(sender As Object, e As EventArgs) Handles cmdApprove.Click
    '    ctlL.Legal_Approve(hdLegalUID.Value, ACTION_APPROVED, Request.Cookies("iLaw")("userid"))
    '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','กฎหมายถูกอนุมัติเรียบร้อย');", True)
    'End Sub

    Protected Sub cmdAddPractice_Click(sender As Object, e As EventArgs) Handles cmdAddPractice.Click
        If txtPracticeDescription.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','กรุณระบุข้อสาระสำคัญก่อน');", True)
            Exit Sub
        End If

        ctlL.Practice_Save(StrNull2Zero(hdPracticeUID.Value), StrNull2Zero(hdLegalUID.Value), txtPracticeCode.Text, txtPracticeDescription.Text, ddlRecurence.SelectedValue)
        txtPracticeDescription.Text = ""
        LoadPracticeToGrid()
    End Sub

    Private Sub LoadPracticeToGrid()
        dt = ctlL.Practice_Get(StrNull2Zero(hdLegalUID.Value))
        With grdPractice
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With
    End Sub

    Protected Sub grdPractice_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdPractice.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditPractice(e.CommandArgument())
                Case "imgDel"
                    If ctlL.Practice_Delete(e.CommandArgument, StrNull2Zero(hdLegalUID.Value)) Then
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadPracticeToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If
            End Select


        End If
    End Sub
    Private Sub EditPractice(ByVal pID As String)
        dt = ctlL.Practice_GetByUID(pID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdPracticeUID.Value = DBNull2Str(dt.Rows(0)("UID"))
                txtPracticeCode.Text = DBNull2Str(dt.Rows(0)("Code"))
                txtPracticeDescription.Text = DBNull2Str(dt.Rows(0)("Descriptions"))
                ddlRecurence.SelectedValue = String.Concat(dt.Rows(0)("Recurrence"))
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub grdPractice_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdPractice.RowDataBound

        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As ImageButton = e.Row.Cells(3).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d9edf7';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    Private Sub grdPractice_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdPractice.PageIndexChanging
        grdPractice.PageIndex = e.NewPageIndex
        LoadPracticeToGrid()
    End Sub

    Private Sub chkAllBusinessType_CheckedChanged(sender As Object, e As EventArgs) Handles chkAllBusinessType.CheckedChanged
        If chkAllBusinessType.Checked Then
            chkBusinessType.ClearSelection()
            chkBusinessType.Enabled = False
        Else
            chkBusinessType.Enabled = True
        End If
    End Sub

    Private Sub chkAllFactoryType_CheckedChanged(sender As Object, e As EventArgs) Handles chkAllFactoryType.CheckedChanged
        If chkAllFactoryType.Checked Then
            chkFactoryType.ClearSelection()
            chkFactoryType.Enabled = False
        Else
            chkFactoryType.Enabled = True
        End If
    End Sub

    Private Sub chkAllArea_CheckedChanged(sender As Object, e As EventArgs) Handles chkAllArea.CheckedChanged
        If chkAllArea.Checked Then
            chkArea.ClearSelection()
            chkArea.Enabled = False
        Else
            chkArea.Enabled = True
        End If
    End Sub
End Class