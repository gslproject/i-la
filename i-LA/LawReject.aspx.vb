﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class LawReject
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LawController
    Dim ctlU As New UserController
    Private Const UploadDirectory As String = "~/imgLegal/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadLegalData()
        End If

    End Sub
    Private Sub LoadLegalData()
        dt = ctlL.Legal_GetByUID(Request("lid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdLegalUID.Value = String.Concat(.Item("UID"))
                lblLegalName.Text = String.Concat(.Item("LegalName"))
                lblLegalCode.Text = String.Concat(.Item("Code"))
            End With
        Else
            Response.Redirect("Legal.aspx?m=l&s=reg")
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtReason.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ระบุ เหตุผล');", True)
            Exit Sub
        End If
        ctlL.Legal_Reject(StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iLaw2")("PeriodID")), StrNull2Zero(hdLegalUID.Value), txtReason.Text, StrNull2Zero(Request.Cookies("iLaw")("userid")))

        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Legal", "Legal Reject:{LegalUID=" & hdLegalUID.Value & "}", "")
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ทำการ Reject กฏหมายนี้เรียบร้อย');", True)
        cmdCancel.Text = "กลับหน้าหลัก"
    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Response.Redirect("Legal.aspx?m=l&s=reg")
    End Sub
End Class