﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LawApproveList.aspx.vb"    Inherits="iLA.LawApproveList" %>

        <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
   
        </asp:Content>
        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>อนุมัติการประเมินฯ 
                            <div class="page-title-subheading">อนุมัติการประเมินความสอดคล้องของกฎหมาย</div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <section class="col-lg-12">                          
             <div class="app-page-header">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">                      
                        <div><asp:label ID="lblLegalName" runat="server"></asp:label><asp:HiddenField ID="hdLegalUID" runat="server" />
                            <div class="page-title-subheading">รหัสกฎหมาย <asp:label ID="lblLegalCode" runat="server"></asp:label></div>
                        </div>
                    </div>
                </div>
            </div>
     </section>
         
                    <section class="col-lg-12">
                        <div class="main-card mb-3 card">
                            <div class="card-header">รายการกฎหมายที่ต้องอนุมัติ</div>
                            <div class="card-body">                              
         
<asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" DataKeyNames="UID" GridLines="None" cssclass="table table-hover table-responsive" Width="100%">
                                    <RowStyle HorizontalAlign="Center" />
                                    <columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Code" HeaderText="รหัส">
                                        <itemstyle HorizontalAlign="Center" Width="30px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PracticeDescriptions" HeaderText="สาระสำคัญ (ข้อปฏิบัติ Practice)">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="400px" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="ความถี่" DataField="RecurrenceText" />
                                        <asp:BoundField HeaderText="กฎหมายแม่บท"  DataField="LawMasterName"  />
                                        <asp:BoundField HeaderText="ผลการปฏิบัติ"  DataField="ActionDescriptions" />
                                        <asp:TemplateField HeaderText="เอกสาร">
                                            <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="90px" />
                                            <ItemTemplate>                                              
                                                <asp:HyperLink ID="hlnkDoc" runat="server" target="_blank"  CssClass="font-icon-button" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "HasFile") %>'><i class="fa fa-file-pdf text-primary" aria-hidden="true"></i></asp:HyperLink>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="90px" />
                                        </asp:TemplateField>                                       
                                        <asp:BoundField HeaderText="ผู้บันทึกผลปฏิบัติ" DataField="AssessorName"  />
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlAction" runat="server" CssClass="form-control select2" Width="100px">
                                                    <asp:ListItem Value="Y">อนุมัติ</asp:ListItem>
                                                    <asp:ListItem Value="N">ไม่อนุมัติ</asp:ListItem> 
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="110px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comment">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtComment" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Comment") %>' placeholder="Comment" CssClass="form-control" Height="50px" TextMode="MultiLine"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="300px" />
                                        </asp:TemplateField>
                                    </columns>
                                    <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" />
                                </asp:GridView>



                                                            </div>
                            <div class="box-footer text-center">
                         <asp:Button ID="cmdSave" CssClass="btn btn-success" runat="server" Text="Save" />
                            </div>
                        </div>                      
                                         
                    </section>
                </div>

               

            </section>
        </asp:Content>