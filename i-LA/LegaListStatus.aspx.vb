﻿Imports System.Data
Public Class LegaListStatus
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    Public dtLegal As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If
        If Request("s") = "1" Then

        Else
            lblReportTitle.Text = "Work Flow Overview"
        End If
        If Not IsPostBack Then
            Select Case Request("st")
                Case "all"
                    lblTitle.Text = "รายการกฎหมายที่เกี่ยวข้องทั้งหมด"
                    dtLegal = ctlR.LegalAction_GetRelate(Request.Cookies("iLaw")("LoginCompanyUID"), Request.Cookies("iLaw2")("PeriodID"))
                Case "asm"
                    lblTitle.Text = "รายการกฎหมายที่ประเมินความสอดคล้องแล้ว"
                    dtLegal = ctlR.LegalAction_GetByAssessmented(Request.Cookies("iLaw")("LoginCompanyUID"), Request.Cookies("iLaw2")("PeriodID"))
                Case "no"
                    lblTitle.Text = "รายการกฎหมายที่ไม่สอดคล้อง"
                    dtLegal = ctlR.LegalAction_GetByAsmResult(Request.Cookies("iLaw")("LoginCompanyUID"), Request.Cookies("iLaw2")("PeriodID"), "N")
                Case "yes"
                    lblTitle.Text = "รายการกฎหมายที่สอดคล้อง"
                    dtLegal = ctlR.LegalAction_GetByAsmResult(Request.Cookies("iLaw")("LoginCompanyUID"), Request.Cookies("iLaw2")("PeriodID"), "Y")
                Case Else
                    Response.Redirect("Home.aspx")
            End Select

        End If
    End Sub
End Class