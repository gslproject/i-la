﻿Imports System.Data
Public Class HRA
    Inherits System.Web.UI.Page
    Dim ctlA As New AssessmentController
    Public dtPersonRisk As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadPerson()
        End If
    End Sub
    Private Sub LoadPerson()
        If Request.Cookies("iLaw")("ROLE_CUS") = True Then
            'dtPersonRisk = ctlA.AsmHRA_GetByPerson(Request.Cookies("iLaw")("LoginPersonUID"))
            Response.Redirect("HRAResult?ActionType=hrarpt&pid=" & Request.Cookies("iLaw")("LoginPersonUID"))
        Else
            dtPersonRisk = ctlA.AsmHRA_Get(Request.Cookies("iLaw")("LoginCompanyUID"))
        End If


    End Sub

End Class