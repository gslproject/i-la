﻿Imports System.Net.Mail
Imports System.Web
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Net

Public Class InvoiceAlert
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New CompanyController
    'Dim ctlU As New UserController
    'Dim enc As New CryptographyEngine
    Dim ReportFileName As String
    'Public dtInvA As New DataTable
    Public dtCus As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblAlert.Visible = False
            lblNo.Visible = False
            LoadInvoiceAlert()
        End If
        'dtInvA = ctlE.Invoice_GetToday
        dtCus = ctlE.Customer_GetRemainExpire

        'txtUserLimit.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtEmployeeQTY.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

    End Sub

    Private Sub LoadInvoiceAlert()
        Dim CompanyCode, CompanyName, InvNo, InvDate, ItemDesc, ContactName, ContactMail, PackageNo, InvType, mailSubject As String
        Dim CompanyUID As Integer
        lblAlert.Visible = False
        dt = ctlE.Customer_GetRemainExpire()
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                CompanyUID = 0
                CompanyCode = ""
                CompanyName = ""
                InvNo = ""
                InvDate = ""
                ItemDesc = ""
                ContactName = ""
                ContactMail = ""
                PackageNo = ""
                InvType = ""
                mailSubject = ""

                CompanyUID = dt.Rows(i)("CompanyUID")
                CompanyCode = String.Concat(dt.Rows(i)("CompanyCode"))
                CompanyName = String.Concat(dt.Rows(i)("CompanyName"))
                InvDate = ConvertStrDate2DBDate(dt.Rows(i)("InvoiceDate"))
                ContactName = String.Concat(dt.Rows(i)("ContactName"))
                ContactMail = String.Concat(dt.Rows(i)("ContactMail"))
                PackageNo = String.Concat(dt.Rows(i)("PackageNo"))
                InvType = String.Concat(dt.Rows(i)("InvoiceType"))

                InvNo = Year(InvDate).ToString() + Month(InvDate).ToString("0#") + Day(InvDate).ToString("0#") + "_" + CompanyCode  'ctlM.RunningNumber_New(CODE_INVOICE) 'Year + Month + CustomerID

                ItemDesc = "ค่าต่ออายุสมาชิก i-Law ระบบบริหารจัดการกฎหมาย Package " & PackageNo & " ระยะเวลา 1 ปี"

                Dim Username, Password As String
                Username = ""
                Password = ""
                'dt = ctlU.User_GetMainUserAdmin(CompanyUID)
                'If dt.Rows.Count > 0 Then
                '    Username = String.Concat(dt.Rows(0)("Username"))
                '    Password = enc.DecryptString(String.Concat(dt.Rows(0)("Passwords")), True)
                'End If

                GenInvoice(CompanyUID, CompanyCode, InvType, InvNo, InvDate, ItemDesc)

                Select Case InvType
                    Case "1"
                        mailSubject = ""
                    Case "2"
                        mailSubject = "แจ้งเตือนสมาชิกใกล้หมดอายุ อีก 15 วัน : " & CompanyName
                    Case "3"
                        mailSubject = "แจ้งเตือนสมาชิกใกล้หมดอายุ อีก 30 วัน : " & CompanyName
                    Case "4"
                        mailSubject = "แจ้งเตือนสมาชิกใกล้หมดอายุ อีก 3 เดือน : " & CompanyName
                End Select

                If ContactMail <> "" Then
                    SendEmailInvoice(CompanyUID, CompanyCode, CompanyName, InvNo, ContactName, Username, Password, ContactMail, mailSubject)
                End If


            Next
            lblAlert.Visible = True
            lblNo.Visible = False
        Else
            lblNo.Visible = True
            lblAlert.Visible = False
        End If
    End Sub


    Private Sub GenPayin(CustomerID As Integer, CompanyCode As String)
        LoadReport("payin", CompanyCode, CustomerID, CompanyCode)
    End Sub
    Private Sub GenInvoice(CustomerID As Integer, CompanyCode As String, InvType As String, InvNo As String, InvDate As String, ItemDesc As String)
        Dim dtPk As New DataTable
        Dim UnitPrice, Vat, Amount, NetTotal As Double
        dtPk = ctlE.Company_GetPackageDetail(CustomerID)
        NetTotal = dtPk.Rows(0)("Amount")
        'มูลค่า VAT = ราคารวม VAT x 7/107
        'ราคาก่อน VAT = ราคารวม VAT x 100/107
        UnitPrice = Math.Round(NetTotal * 100 / 107, 2)
        Vat = Math.Round(NetTotal * 7 / 107, 2)
        Amount = UnitPrice

        ctlE.Invoice_Add(InvNo, InvDate, CustomerID, InvType, ItemDesc, UnitPrice, Vat, Amount, NetTotal, 1)

        'ctlM.RunningNumber_Update(CODE_INVOICE)

        LoadReport("inv", InvNo, CustomerID, CompanyCode)
    End Sub
    Private Sub LoadReport(DocumentType As String, DocumentNo As String, CompanyUID As Integer, CompanyCode As String)

        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True

        Dim xParam As New List(Of ReportParameter)

        Dim filePath As String = ""
        Select Case DocumentType
            Case "payin"
                filePath = "Invoice\Payin\"
                ReportFileName = "Payin" & CompanyCode
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("Payin")
                xParam.Add(New ReportParameter("CompanyUID", CompanyUID.ToString()))
            Case "inv"
                filePath = "Invoice\Invoice\"
                ReportFileName = "ใบแจ้งค่าบริการInvoice" & DocumentNo
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("Invoice")
                xParam.Add(New ReportParameter("InvoiceNo", DocumentNo))
            Case Else
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("")
                xParam.Add(New ReportParameter("UserID", Request.Cookies("iLaw")("userid").ToString()))
        End Select

        ReportViewer1.ServerReport.SetParameters(xParam)

        Dim warnings As Warning()
        Dim streamIds As String()
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim extension As String = String.Empty

        Dim result As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)

        Try
            Dim objfile As FileInfo = New FileInfo(Server.MapPath(filePath & ReportFileName & ".pdf"))
            If objfile.Exists Then
                objfile.Delete()
            End If

            Dim stream As FileStream = File.Create(Server.MapPath(filePath & ReportFileName & ".pdf"), result.Length)
            stream.Write(result, 0, result.Length)
            stream.Close()

        Catch e As Exception

        End Try
    End Sub

    Private Sub SendEmailInvoice(CompanyUID As Integer, CompanyCode As String, CompanyName As String, DocumentNo As String, PersonName As String, Username As String, Password As String, sTo As String, mailSubject As String)
        Dim SenderDisplayName As String = "i-Law : Legal Management System"
        Dim MySubject As String = mailSubject
        Dim MyMessageBody As String = ""

        MyMessageBody = " <font size='4'>
    
  <p> เรียน คุณ " & PersonName & "</p>
  <p> เรื่อง การแจ้งเตือนต่ออายุสมาชิกโปรแกรม i-Law : Legal Management System </p>
  <p> บริษัทฯ NPC S&E ขอขอบพระคุณท่านเป็นอย่างสูงที่ได้ไว้วางใจในการเป็นสมาชิก i-Law : Legal Management System ของเราด้วยดีตลอดมา โดยบริษัทฯ ขอเสนอบริการระบบฐานข้อมูลการบริหารจัดการกฎหมายด้านความปลอดภัย อาชีวอนามัย สิ่งแวดล้อมและพลังงาน  <br /> <br /> 
โดยท่านสามารถดูรายละเอียดเพิ่มเติมได้ที่ <a href='https://npc-legal.com/MemberBenefits'>https://npc-legal.com/MemberBenefits </a> <br /> <br /> 

และบริษัทฯ ขอเรียนให้ท่านทราบว่าอายุการเป็นสมาชิกของท่านจะสิ้นสุดในวันที่ 15/02/2565 <br /> <br /> 

เพื่อให้การใช้งานโปรแกรมของท่านได้อย่างต่อเนื่อง ท่านสามารถชำระค่าสมาชิก/ต่ออายุสมาชิกได้ตามช่องทาง ดังนี้ <br /> <br /> 

1.ชำระผ่านระบบธนาคาร <br /> 
1.1 นำใบชำระเงิน (Pay-in Slip) ที่แนบมาพร้อมอีเมลล์ฉบับนี้ และนำไปชำระพร้อมเงินสดได้ที่ เคาน์เตอร์ธนาคารกรุงเทพ ทุกสาขา <br /> 

1.2 โอนเงินเข้าบัญชีบริษัทฯ ชื่อบัญชี บริษัท เอ็นพีซี เซฟตี้ แอนด์ เอ็นไวเมนทอล เซอร์วิส จำกัด <br /> <br /> 

ธนาคารกรุงเทพ สาขามาบตาพุด เลขที่บัญชี 443-409850-7 <br /> <br /> 

1.3 ชำระผ่านระบบพร้อมเพย์หมายแลข 0105548019031  หรือแสกนผ่าน QR code นี้  <br /> <br />
  <img src='http://147.50.248.35/ila/images/qrprompay.jpg' /><br /> <br /> 

        2.ชำระด้วยเงินสด ณ สำนักงานบริษัทฯ วันจันทร์ – ศุกร์ เวลา 08:00 น. – 17:00 น. <br /><br /> 
        3.การหักภาษี ณ ที่จ่าย กรณีหักภาษี ณ ที่จ่าย ให้ส่งหนังสือรับรองการหักภาษี ณ ที่จ่าย มาที่ Email :
        <a href='mailtonpclegal@npc - se.co.th'>npclegal@npc-se.co.th</a>  หรือ ทางโทรสาร (FAX) 038 977701 ทันทีที่มีการชำระค่าบริการทุกช่องทาง <br /> <br />
</p>
หากท่านต้องการติดต่อสอบถามข้อมูลเพิ่มเติมสามารถติดต่อได้ตามช่องทางต่อไปนี้ <br />

        1. ติดต่อสอบถามเรื่องวิธีการสมัคร / ใบแจ้งค่าบริการ <br /><br />
        โทร. 061-345-4646 หรือ 098-324-4192 หรือ 092-594-6659 <br />
        อีเมลล์ <a href='mailto: npclegal@npc - se.co.th'>npclegal@npc-se.co.th</a> <br />
        Line id : @NPCi-LA (มี @ ด้วย) หรือ แสกน QR Code นี้ <br /><br />
        <img src='http://147.50.248.35/ila/images/qrline.jpg' />

        <br /><br />
        2.ติดต่อเรื่องใบเสร็จรับเงิน โทร. 038-977-641 หรือ 038-977-671 <br /> <br />

        บริษัทฯ ขอขอบพระคุณที่ท่านใช้บริการ i-Law : Legal Management System <br />
        ขอแสดงความนับถือ <br />
        บริษัท เอ็นพีซี เซฟตี้ แอนด์ เอ็นไวรอนเมนทอลเซอร์วิส จำกัด
    </font>"
        Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailMessage.From = New MailAddress("npcsafetyservice@gmail.com", SenderDisplayName)  'thaiergonomic
        mailMessage.Subject = MySubject
        mailMessage.Body = MyMessageBody
        mailMessage.IsBodyHtml = True
        mailMessage.[To].Add(New MailAddress(sTo))
        'Dim fStream As FileStream
        Dim dir As DirectoryInfo = New DirectoryInfo(Server.MapPath("Invoice\Payin\Payin" & CompanyCode & ".pdf"))

        Dim objfile1 As FileInfo = New FileInfo(Server.MapPath("Invoice\Payin\Payin" & CompanyCode & ".pdf"))
        If objfile1.Exists Then
            mailMessage.Attachments.Add(New Attachment(Server.MapPath("Invoice\Payin\Payin" & CompanyCode & ".pdf")))
        End If

        Dim objfile2 As FileInfo = New FileInfo(Server.MapPath("Invoice\Invoice\ใบแจ้งค่าบริการInvoice" & DocumentNo & ".pdf"))
        If objfile2.Exists Then
            mailMessage.Attachments.Add(New Attachment(Server.MapPath("Invoice\Invoice\ใบแจ้งค่าบริการInvoice" & DocumentNo & ".pdf")))
        End If

        mailMessage.Attachments.Add(New Attachment(Server.MapPath("Invoice\20NPCS&E1.pdf")))

        Dim smtp As SmtpClient = New SmtpClient()
        smtp.Host = "smtp.gmail.com"
        smtp.EnableSsl = True
        Dim NetworkCred As NetworkCredential = New NetworkCredential()
        NetworkCred.UserName = mailMessage.From.Address
        NetworkCred.Password = "Qazxsw21"
        smtp.UseDefaultCredentials = True
        smtp.Credentials = NetworkCred
        smtp.Port = 587
        smtp.Send(mailMessage)
    End Sub


End Class