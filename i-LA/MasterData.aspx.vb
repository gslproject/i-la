﻿
Public Class MasterData
    Inherits System.Web.UI.Page
    Dim ctlL As New LawController
    Dim ctlM As New MasterController
    Dim dt As New DataTable
    Dim acc As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            ClearData()
            txtUID.Text = ""
            LoadMasterDataToGrid()
        End If

    End Sub

    'Private Sub LoadDivision()
    '    ddlDivision.DataSource = ctlM.Division_Get(Request.Cookies("iLaw")("LoginCompanyUID"))
    '    ddlDivision.DataTextField = "DivisionName"
    '    ddlDivision.DataValueField = "DivisionUID"
    '    ddlDivision.DataBind()
    'End Sub

    Private Sub LoadMasterDataToGrid()

        Select Case Request("m")
            Case "t"
                lblTitle.Text = "ประเภทกฎหมาย"
                lblTitleDesc.Text = "จัดการรายการประเภทกฎหมาย"
                dt = ctlL.LawType_GetBySearch(txtSearch.Text)
            Case "m"
                lblTitle.Text = "กฎหมายแม่บท"
                lblTitleDesc.Text = "จัดการรายการกฎหมายแม่บท"
                dt = ctlL.LawMaster_GetBySearch(txtSearch.Text)
            Case "f"
                lblTitle.Text = "หน่วยงานที่บังคับใช้กฎหมาย"
                lblTitleDesc.Text = "จัดการรายการหน่วยงานที่บังคับใช้กฎหมาย"
                dt = ctlM.Organize_GetSearch(txtSearch.Text)
            Case "pv"
                lblTitle.Text = "พื้นที่บังคับใช้"
                lblTitleDesc.Text = "จัดการรายการพื้นที่บังคับใช้กฎหมาย"
                dt = ctlM.Area_GetBySearch(txtSearch.Text)
            'Case "sc"
            '    lblTitle.Text = "ขอบเขต"
            '    lblTitleDesc.Text = "จัดการรายการขอบเขตกฎหมาย"
            '    dt = ctlL.LawCategory_GetBySearch(txtSearch.Text)
            Case "bu"
                lblTitle.Text = "ประเภทธุรกิจ"
                lblTitleDesc.Text = "จัดการข้อมูลประเภทธุรกิจ"
                dt = ctlM.BusinessType_GetBySearch(txtSearch.Text)
            Case "fac"
                lblTitle.Text = "ประเภทโรงงาน"
                lblTitleDesc.Text = "จัดการข้อมูลประเภทโรงงาน"
                dt = ctlM.FactoryType_GetBySearch(txtSearch.Text)
            Case "min"
                lblTitle.Text = "หน่วยงานที่ออกกฎหมาย"
                lblTitleDesc.Text = "จัดการ กระทรวง/ทบวง/กรม/หน่วยงานที่ออกกฎหมาย"
                dt = ctlM.Ministry_GetBySearch(txtSearch.Text)
            Case "kw"
                lblTitle.Text = "Keyword"
                lblTitleDesc.Text = "จัดการคำ Keyword ที่เกี่ยวข้องกับกฎหมาย"
                dt = ctlM.Keyword_GetBySearch(txtSearch.Text)
            Case "ph"
                lblTitle.Text = "Implementation Phase"
                lblTitleDesc.Text = "จัดการคำ Implementation Phase"
                dt = ctlM.Phase_GetBySearch(txtSearch.Text)

        End Select

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With
        Me.Title = lblTitle.Text
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If DeleteData(e.CommandArgument) Then
                        acc.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_DEL, GetTableName(Request("m")), "UID=" & e.CommandArgument, "ลบ Master")
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadMasterDataToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If
            End Select
        End If
    End Sub
    Private Sub EditData(ByVal pID As String)
        Select Case Request("m")
            Case "t"
                dt = ctlL.LawType_GetByUID(pID)
            Case "m"
                dt = ctlL.LawMaster_GetByUID(pID)
            Case "f"
                dt = ctlM.Organize_GetSearch(pID)
            Case "pv"
                dt = ctlM.Area_GetByUID(pID)
            Case "sc"
                dt = ctlL.LawCategory_GetByUID(pID)
            Case "bu"
                dt = ctlM.BusinessType_GetByUID(pID)
            Case "fac"
                dt = ctlM.FactoryType_GetByUID(pID)
            Case "min"
                dt = ctlM.Ministry_GetByUID(pID)
            Case "kw"
                dt = ctlM.Keyword_GetByUID(pID)
            Case "ph"
                dt = ctlM.Phase_GetByUID(pID)
        End Select

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                Me.txtUID.Text = DBNull2Str(dt.Rows(0)("UID"))
                'txtCode.Text = String.Concat(dt.Rows(0)("Code"))
                txtName.Text = DBNull2Str(dt.Rows(0)("Name"))
                txtSort.Text = DBNull2Str(dt.Rows(0)("Sort"))
                chkStatus.Checked = ConvertStatusFlag2CHK(dt.Rows(0)("StatusFlag"))
            End With
        End If
        dt = Nothing
    End Sub
    Function DeleteData(ByVal pID As String) As Boolean
        Dim d As Integer = 0
        Select Case Request("m")
            Case "t"
                d = ctlL.LawType_Delete(pID)
            Case "m"
                d = ctlL.LawMaster_Delete(pID)
            Case "f"
                d = ctlM.Organize_Delete(pID)
            Case "pv"
                d = ctlM.Area_Delete(pID)
            Case "sc"
                d = ctlL.LawCategory_Delete(pID)
            Case "bu"
                d = ctlM.BusinessType_Delete(pID)
            Case "fac"
                d = ctlM.FactoryType_Delete(pID)
            Case "min"
                d = ctlM.Ministry_Delete(pID)
            Case "kw"
                d = ctlM.Keyword_Delete(pID)
            Case "ph"
                d = ctlM.Phase_Delete(pID)
        End Select
        Return Convert.ToBoolean(d)
    End Function
    Private Sub ClearData()
        Me.txtUID.Text = ""
        txtUID.Text = ""
        txtName.Text = ""
        txtSort.Text = "0"
        chkStatus.Checked = True
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(4).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d9edf7';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลให้ครบถ้วน');", True)
            Exit Sub
        End If

        Dim d As Integer = 0
        Dim TblName As String = ""
        Select Case Request("m")
            Case "t"
                TblName = "LawType"
                d = ctlL.LawType_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
            Case "m"
                TblName = "LawMaster"
                d = ctlL.LawMaster_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
            Case "f"
                TblName = "Organize"
                d = ctlM.Organize_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
            Case "pv"
                TblName = "Area"
                d = ctlM.Area_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
            Case "sc"
                TblName = "LawCategory"
                d = ctlL.LawCategory_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
            Case "bu"
                TblName = "BusinessType"
                d = ctlM.BusinessType_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
            Case "fac"
                TblName = "FactoryType"
                d = ctlM.FactoryType_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
            Case "min"
                TblName = "Ministry"
                d = ctlM.Ministry_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
            Case "kw"
                TblName = "Keyword"
                d = ctlM.Keyword_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
            Case "ph"
                TblName = "Phase"
                d = ctlM.Phase_Save(StrNull2Zero(txtUID.Text), txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
        End Select

        acc.User_GenLogfile(Request.Cookies("iLaw")("username"), "Add/Update", TblName, "{UID=" & txtUID.Text & "}{Name=" & txtName.Text & "}{Sort=" & txtSort.Text & "}{StatusFlag=" & ConvertBoolean2StatusFlag(chkStatus.Checked), lblTitle.Text)

        LoadMasterDataToGrid()
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

    Function GetTableName(M As String) As String
        Dim TblName As String = ""
        Select Case M
            Case "t"
                TblName = "LawType"
            Case "m"
                TblName = "LawMaster"
            Case "f"
                TblName = "Organize"
            Case "pv"
                TblName = "Area"
            Case "sc"
                TblName = "LawCategory"
            Case "bu"
                TblName = "BusinessType"

            Case "fac"
                TblName = "FactoryType"
            Case "min"
                TblName = "Ministry"
            Case "kw"
                TblName = "Keyword"
            Case "ph"
                TblName = "Phase"
        End Select
        Return TblName
    End Function

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        LoadMasterDataToGrid()
    End Sub

    Protected Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadMasterDataToGrid()
    End Sub
End Class

