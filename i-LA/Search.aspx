﻿<%@ Page Title="ค้นหากฎหมาย" Language="vb" AutoEventWireup="false" CodeBehind="Search.aspx.vb" Inherits="iLA.Search" %>
<%@ Import Namespace="System.Data" %> 
 <!DOCTYPE html>

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title> Login</title>
        <link href="https://fonts.googleapis.com/css?family=Sarabun:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!-- Bootstrap 3.3.7 
  < link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">-->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

        <link href="css/sb-admin.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/rajchasistyles.css">

           <link rel="stylesheet" href="css/main.css">


         <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
     <!-- Select2 -->
  <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">

        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <style type="text/css">
            .modal-confirm {
                color: #434e65;
                width: 525px;
            }
            
            .modal-confirm .modal-content {
                padding: 20px;
                font-size: 16px;
                border-radius: 5px;
                border: none;
            }
            
            .modal-confirm .modal-header {
                background: #e85e6c;
                border-bottom: none;
                position: relative;
                text-align: center;
                margin: -20px -20px 0;
                border-radius: 5px 5px 0 0;
                padding: 35px;
            }
            
            .modal-confirm h4 {
                text-align: center;
                font-size: 36px;
                margin: 10px 0;
            }
            
            .modal-confirm .form-control,
            .modal-confirm .btn {
                min-height: 40px;
                border-radius: 3px;
            }
            
            .modal-confirm .close {
                position: absolute;
                top: 15px;
                right: 15px;
                color: #fff;
                text-shadow: none;
                opacity: 0.5;
            }
            
            .modal-confirm .close:hover {
                opacity: 0.8;
            }
            
            .modal-confirm .icon-box {
                color: #fff;
                width: 95px;
                height: 95px;
                display: inline-block;
                border-radius: 50%;
                z-index: 9;
                border: 5px solid #fff;
                padding: 15px;
                text-align: center;
            }
            
            .modal-confirm .icon-box i {
                font-size: 58px;
                margin: -2px 0 0 -2px;
            }
            
            .modal-confirm.modal-dialog {
                margin-top: 80px;
            }
            
            .modal-confirm .btn {
                color: #fff;
                border-radius: 4px;
                background: #eeb711;
                text-decoration: none;
                transition: all 0.4s;
                line-height: normal;
                border-radius: 30px;
                margin-top: 10px;
                padding: 6px 20px;
                min-width: 150px;
                border: none;
            }
            
            .modal-confirm .btn:hover,
            .modal-confirm .btn:focus {
                background: #eda645;
                outline: none;
            }
            
            .trigger-btn {
                display: inline-block;
                margin: 100px auto;
            }
        </style>
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min-login.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            function openModals(sender, title, message) {
                $("#spnTitle").text(title);
                $("#spnMsg").text(message);
                $('#modalPopUp').modal('show');
                $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
                return false;
            }
        </script>

        <!-- End modal -->                        
         
   
    </head>

    <body>

                <form id="form1" class="user" runat="server">
            <div class="container-login">
                <div class="row">

                     <section class="col-lg-12 connectedSortable">
                        <div class="row">
                            <div class="col-md-12">
                                <table>
                                    <tr>
                                        <td class="logo">
                                            <img src="images/lalogo3.png" alt="" width="150">
                                        </td>
                                        <td>
                                            <div class="header-logo">i-Law</div>
                                            <div class="header-appname">
                                                ระบบบริหารจัดการกฎหมาย
                                            </div>
                                            <div class="header-text">
                                                บริษัท เอ็นพีซี เซฟตี้ แอนด์ เอ็นไวรอนเมนทอล เซอร์วิส จำกัด
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
                <br /><br />

        <div class="row justify-content-center">


 <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-search icon-gradient bg-primary"></i>
                        </div>
                        <h3 class="text-blue text-bold">Legal Search / ค้นหากฎหมาย   </h3>
                    </div>
                </div>
            </div>
          </div>

    <section class="content"> 
      <div class="row">  
           <div class="col-md-12">
            <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon pe-7s-search icon-gradient bg-success">
            </i>ค้นหา
            <div class="btn-actions-pane-right">               
            </div>
        </div>
        <div class="card-body">
             <div class="row">
                  <div class="col-md-3">
                      <div class="form-group">
                        <label>ประเภท</label>
                        <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                  </div>
               
                 <div class="col-md-4">
                      <div class="form-group">
                        <label>กระทรวง</label>
                        <asp:DropDownList ID="ddlMinistry" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                  </div>
               
                  <div class="col-md-2">
                     <div class="form-group">
                        <label>ปี</label>
                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control select2"></asp:DropDownList>
                     </div>
                  </div>
                
                 <div class="col-md-3">
                      <div class="form-group">
                        <label>ประเภทธุรกิจ</label>
                        <asp:DropDownList ID="ddlBusinessType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                  </div>                
               
                 <div class="col-md-4">
                      <div class="form-group">
                        <label>คำสำคัญ</label>
                          <asp:DropDownList ID="ddlKeyword" runat="server" CssClass="form-control select2"></asp:DropDownList>
                      </div>
                  </div>     
                  <div class="col-md-4">
                      <div class="form-group">
                        <label>คำค้นหา</label>
                          <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                      </div>
                  </div>      

             
                  <div class="col-md-4">
                     <div class="form-group">
                         <br />
                        <asp:Button ID="cmdSearch" CssClass="btn btn-primary" runat="server" Text="ค้นหา" Width="120px" />
                         <asp:Button ID="cmdCancel" CssClass="btn btn-secondary" runat="server" Text="ยกเลิก" Width="120px" />
                     </div>
                  </div>

            </div>
        </div>
       </div>
</div>
        </div>
  
     <div class="row">  
          <div class="col-md-12">

        <div class="box box-primary">
        <div class="box-header"><i class="header-icon lnr-diamond icon-gradient bg-success">
            </i>กฎหมายที่เกี่ยวข้อง
            <div class="btn-actions-pane-right">
               
            </div>
        </div>
        <div class="box-body">  
            <div class="table-responsive">
              <table id="tbdata" class="table table-hover table-bordered">
                <thead>
                <tr>                   
                  <th class="sorting_asc_disabled sorting_desc_disabled text-center">ลำดับ</th>
                  <th class="text-center">ชื่อกฎหมาย</th>
                  <th class="text-center">ประเภท</th> 
                     <th class="text-center">กฎหมายแม่บท</th>
                     <th class="text-center">กระทรวง</th>                
                  <th class="text-center">สถานะ</th>
                      <th class="text-center">วันที่ประกาศ</th>
                      <th class="text-center">วันที่มีผลบังคับใช้</th>
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLegal.Rows %>
                <tr>                  
                  <td  width="40" class="text-center"><% =String.Concat(row("RowNo")) %></td>
                  <td><% =String.Concat(row("LegalName")) %></td>
                  <td><% =String.Concat(row("LawTypeName")) %></td>                    
                         <td class="text-center"><% =String.Concat(row("LawMaster")) %></td>
                         <td class="text-center"><% =String.Concat(row("MinistryName")) %></td>
                      <td class="text-center"><% =IIf(String.Concat(row("StatusFlag")) = "A", "<img src='images/icon-ok.png'>", "") %> </td>  
                  <td class="text-center"><% =String.Concat(row("IssueDate")) %></td>
                   <td class="text-center"><% =String.Concat(row("StartDate")) %></td>
                  
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
           </div>
                </div> 
          </div>
 </div>
</div>
    </section>

                    <div class="footer text-center">
                    <footer class="main-footer-login">
                        <div class="text-center hidden-xs">
                            <strong>Copyright &copy; 2021-2022 <a href="https://www.npc-se.co.th">NPC S&E</a>.</strong> All rights reserved.
                            <br /> <b>Version</b>
                            <asp:Label ID="Label1" runat="server" Text="1.0.0"></asp:Label>&nbsp;&nbsp;<b>Release</b> 2020.12.28
                        </div>


                    </footer>
                </div>
            </div>

        </form>

           <!-- jQuery 3 -->
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
          
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>

        <!-- iCheck -->
        <script src="plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('.select2').select2()  
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' /* optional */
                });
            });
        </script>

        
<script>
    $(function () {
        $('#tbdata').DataTable()
        $('#tbdata2').DataTable()
        $('#MainContent_grdData').DataTable()
        $('#grdData2').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script> 


         </body>

    </html>
