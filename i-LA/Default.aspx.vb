﻿
Public Class _Default
    Inherits Page
    Dim dt As New DataTable
    Dim acc As New UserController
    Dim enc As New CryptographyEngine
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Request("logout") = "y" Then
            Session.Contents.RemoveAll()
            Session.Abandon()
            Session.RemoveAll()
            Response.Cookies.Clear()

            Dim delCookie1 As HttpCookie
            delCookie1 = New HttpCookie("iLaw")
            delCookie1.Expires = DateTime.Now.AddDays(-1D)
            Response.Cookies.Add(delCookie1)
        Else
            'RenewSessionFromCookie()
        End If

        If Not IsPostBack Then
            If Request("logout") = "y" Then
                Session.Abandon()
                Request.Cookies("iLaw")("userid") = Nothing
            End If
            txtUsername.Focus()
        End If

        Session.Timeout = 360
        'Response.Redirect("Home")

    End Sub

    Private Sub RenewSessionFromCookie()
        If IsNothing(Request.Cookies("iLaw")) Then
            Exit Sub
        End If

        dt = acc.User_GetByUserID(StrNull2Zero(Request.Cookies("iLaw")("userid")))

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item("StatusFlag") <> "A" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ID ของท่านไม่สามารถใช้งานได้ชั่วคราว');", True)
                Exit Sub
            End If

            'Response.Cookies("iLaw")("userid") = ""
            'Response.Cookies("iLaw")("username") = ""
            'Response.Cookies("iLaw").Expires = acc.GET_DATE_SERVER().AddDays(1)


            Dim iLawCookie As HttpCookie = New HttpCookie("iLaw")
            iLawCookie("userid") = dt.Rows(0).Item("UserID")
            iLawCookie("username") = String.Concat(dt.Rows(0).Item("USERNAME")).ToString()
            iLawCookie("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORDs"), True)
            iLawCookie("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            iLawCookie("NameOfUser") = String.Concat(dt.Rows(0).Item("Name"))
            iLawCookie("LoginPersonUID") = dt.Rows(0).Item("UserID")
            iLawCookie("LoginCompanyUID") = dt.Rows(0).Item("CompanyUID")
            iLawCookie("ROLE_ADM") = False
            iLawCookie("ROLE_USR") = False
            iLawCookie("ROLE_APP") = False
            iLawCookie("ROLE_SPA") = False

            iLawCookie.Expires = acc.GET_DATE_SERVER().AddMinutes(60)
            Response.Cookies.Add(iLawCookie)

            'Request.Cookies("iLaw")("userid") = dt.Rows(0).Item("UserID")
            'Request.Cookies("iLaw")("username") = dt.Rows(0).Item("USERNAME")
            'Request.Cookies("iLaw")("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORDs"), True)
            'Request.Cookies("iLaw")("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            'Request.Cookies("iLaw")("NameOfUser") = String.Concat(dt.Rows(0).Item("Name"))
            'Request.Cookies("iLaw")("LoginPersonUID") = dt.Rows(0).Item("UserID")
            'Request.Cookies("iLaw")("LoginCompanyUID") = dt.Rows(0).Item("CompanyUID")
            'Request.Cookies("iLaw")("ROLE_USR") = False
            'Request.Cookies("iLaw")("ROLE_APP") = False
            'Request.Cookies("iLaw")("ROLE_ADM") = False
            'Request.Cookies("iLaw")("ROLE_SPA") = False


            Dim rd As New UserRoleController

            dt = rd.UserAccessGroup_GetByUID(Request.Cookies("iLaw")("userid"))
            If dt.Rows.Count > 0 Then

                For i = 0 To dt.Rows.Count - 1

                    Select Case dt.Rows(i)("UserGroupUID")
                        Case 1 'Customer Admin
                            iLawCookie("ROLE_ADM") = True
                            'Request.Cookies("iLaw")("ROLE_USR") = True
                        Case 2  'Customer User
                            iLawCookie("ROLE_USR") = True
                            'Request.Cookies("iLaw")("ROLE_USR") = True
                        Case 3 'Approver
                            iLawCookie("ROLE_APP") = True
                            'Request.Cookies("iLaw")("ROLE_SPA") = True
                        Case 9 'NPC-SE Admin
                            iLawCookie("ROLE_SPA") = True
                            'Request.Cookies("iLaw")("ROLE_SPA") = True
                    End Select
                Next

            End If

            Response.Redirect("LoginPeriod")

        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username  หรือ Password ไม่ถูกต้อง');", True)
            Exit Sub
        End If

    End Sub

    Private Sub CheckUser()
        dt = acc.User_CheckLogin(txtUsername.Text, enc.EncryptString(txtPassword.Text, True))

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item("StatusFlag") <> "A" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ID ของท่านไม่สามารถใช้งานได้ชั่วคราว');", True)
                Exit Sub
            End If

            '--------------------------------------------------------------------------------------------

            Dim iLawCookie As HttpCookie = New HttpCookie("iLaw")
            iLawCookie("userid") = dt.Rows(0).Item("UserID")
            iLawCookie("username") = String.Concat(dt.Rows(0).Item("USERNAME")).ToString()
            iLawCookie("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORDs"), True)
            iLawCookie("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            iLawCookie("NameOfUser") = String.Concat(dt.Rows(0).Item("Name"))
            iLawCookie("LoginPersonUID") = dt.Rows(0).Item("UserID")
            iLawCookie("LoginCompanyUID") = dt.Rows(0).Item("CompanyUID")
            iLawCookie("ROLE_ADM") = False
            iLawCookie("ROLE_USR") = False
            iLawCookie("ROLE_APP") = False
            iLawCookie("ROLE_SPA") = False
            iLawCookie("PACKAGENO") = dt.Rows(0).Item("PackageNo")

            iLawCookie.Expires = acc.GET_DATE_SERVER().AddMinutes(60)
            Response.Cookies.Add(iLawCookie)


            'If Request.Cookies("iLaw")("UserProfileID") <> 1 Then
            '    Dim ctlC As New CourseController
            '    Request.Cookies("iLaw")("ROLE_ADV") = ctlC.CoursesCoordinator_CheckStatus(Request.Cookies("iLaw")("ProfileID"))
            'End If

            'Dim ctlCfg As New SystemConfigController()
            'Request.Cookies("iLaw")("EDUYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_EDUYEAR)
            'Request.Cookies("iLaw")("ASSMYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_ASSMYEAR)

            genLastLog()
            acc.User_GenLogfile(txtUsername.Text, ACTTYPE_LOG, "Users", "", "")

            Dim rd As New UserRoleController

            dt = rd.UserAccessGroup_GetByUID(dt.Rows(0).Item("UserID"))
            If dt.Rows.Count > 0 Then

                For i = 0 To dt.Rows.Count - 1
                    Select Case dt.Rows(i)("UserGroupUID")
                        Case 1   'Customer Admin
                            iLawCookie("ROLE_ADM") = True
                            Request.Cookies("iLaw")("ROLE_ADM") = True
                        Case 2  'Customer User
                            iLawCookie("ROLE_USR") = True
                            Request.Cookies("iLaw")("ROLE_USR") = True
                        Case 3 'Approver
                            iLawCookie("ROLE_APP") = True
                            Request.Cookies("iLaw")("ROLE_APP") = True
                        Case 9  'NPC-SE Administrator
                            iLawCookie("ROLE_SPA") = True
                            Request.Cookies("iLaw")("ROLE_SPA") = True
                    End Select
                Next

            End If

            'Select Case Request.Cookies("iLaw")("UserProfileID")
            '    Case 1 'Customer
            '        Response.Redirect("Default.aspx")
            '    Case 2, 3 'Teacher
            '        Response.Redirect("Default.aspx")
            '    Case 4 'Admin
            Response.Redirect("LoginPeriod")
            'End Select

        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username  หรือ Password ไม่ถูกต้อง');", True)
            Exit Sub
        End If
    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        'txtUsername.Value = "admin"
        'txtPassword.Value = "112233"
        'Request.Cookies("iLaw")("userid") = 1
        'Request.Cookies("iLaw")("ROLE_SPA") = True
        'Response.Redirect("Home")
        If txtUsername.Text = "" Or txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน');", True)
            txtUsername.Focus()
            Exit Sub
        End If

        Session.Contents.RemoveAll()
        CheckUser()
    End Sub

    Private Sub genLastLog() ' เก็บวันเวลาที่ User เข้าใช้ระบบครั้งล่าสุด
        acc.User_LastLog_Update(txtUsername.Text)
    End Sub

End Class