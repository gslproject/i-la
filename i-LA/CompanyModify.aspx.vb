﻿'Imports System.Net.Mail
Imports System.Web
Imports System.IO
Imports Microsoft.Reporting.WebForms
'Imports System.Net

Public Class CompanyModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New CompanyController
    Dim ctlM As New MasterController

    Dim ReportFileName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Request.Cookies("iLaw")("ROLE_ADM") = False And Request.Cookies("iLaw")("ROLE_SPA") = False Then
            Response.Redirect("Error.aspx")
        End If

        If Not IsPostBack Then
            'txtCompanyCode.Text = "Auto Running"
            'txtCompanyCode.ReadOnly = True
            hdCompanyUID.Value = 0
            cmdDelete.Enabled = False
            LoadProvince()
            LoadBusinessType()
            LoadFactoryType()
            LoadKeyword()
            LoadPhase()
            LoadCompanyData()
        End If

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")
        txtUserLimit.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtEmployeeQTY.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

    End Sub
    Private Sub LoadBusinessType()
        ddlBusinessType.DataSource = ctlM.BusinessType_Get4Company()
        ddlBusinessType.DataTextField = "Name"
        ddlBusinessType.DataValueField = "UID"
        ddlBusinessType.DataBind()
    End Sub
    Private Sub LoadFactoryType()
        ddlFactoryType.DataSource = ctlM.FactoryType_Get4Company()
        ddlFactoryType.DataTextField = "Name"
        ddlFactoryType.DataValueField = "UID"
        ddlFactoryType.DataBind()
    End Sub
    Private Sub LoadProvince()
        ddlProvince.DataSource = ctlM.Province_Get4Company()
        ddlProvince.DataTextField = "ProvinceName"
        ddlProvince.DataValueField = "ProvinceID"
        ddlProvince.DataBind()
    End Sub
    Private Sub LoadKeyword()
        chkKeyword.DataSource = ctlM.Keyword_Get()
        chkKeyword.DataTextField = "Name"
        chkKeyword.DataValueField = "UID"
        chkKeyword.DataBind()
    End Sub
    Private Sub LoadPhase()
        ddlPhase.DataSource = ctlM.Phase_Get()
        ddlPhase.DataTextField = "PhaseName"
        ddlPhase.DataValueField = "UID"
        ddlPhase.DataBind()
    End Sub
    Private Sub LoadCompanyData()
        dt = ctlE.Company_GetByUID(Request("cid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Dim dr As DataRow = dt.Rows(0)

                hdCompanyUID.Value = String.Concat(dr("UID"))
                txtCompanyCode.Text = String.Concat(dr("CompanyCode"))
                txtNameTH.Text = String.Concat(dr("NameTH"))
                txtNameEN.Text = String.Concat(dr("NameEN"))
                txtBranch.Text = String.Concat(dr("Branch"))
                txtTaxID.Text = String.Concat(dr("VATID"))
                txtAddressNo.Text = String.Concat(dr("AddressNumber"))
                txtDistrict.Text = String.Concat(dr("District"))
                txtEmail.Text = String.Concat(dr("Email"))
                txtFax.Text = String.Concat(dr("Fax"))
                txtLane.Text = String.Concat(dr("Lane"))
                txtRoad.Text = String.Concat(dr("Road"))
                txtSubDistrict.Text = String.Concat(dr("SubDistrict"))
                txtTel.Text = String.Concat(dr("Telephone"))
                txtWebsite.Text = String.Concat(dr("Website"))
                txtZipcode.Text = String.Concat(dr("Zipcode"))
                ddlCountry.SelectedValue = String.Concat(dr("Country"))
                ddlProvince.SelectedValue = String.Concat(dr("ProvinceID"))

                If String.Concat(dr("StatusFlag")) = "A" Then
                    chkStatus.Checked = True
                Else
                    chkStatus.Checked = False
                End If

                ddlBusinessType.SelectedValue = String.Concat(dr("BusinessTypeUID"))
                ddlFactoryType.SelectedValue = String.Concat(dr("FactoryTypeUID"))
                'ddlFactoryGroup.SelectedValue = String.Concat(dr("FactoryGroupUID"))
                If String.Concat(.Item("FactoryGroupUID")) = "1" Then
                    optFactoryGroup.Checked = True
                Else
                    optFactoryGroup.Checked = False
                End If

                ddlPhase.SelectedValue = String.Concat(dr("PhaseUID"))
                txtUserLimit.Text = String.Concat(dr("MAXPERSON"))
                'txtEmployeeQTY.Text = String.Concat(dr("Employee"))

                txtAddressInvoice.Text = String.Concat(dr("AddressInvoice"))
                txtContactName.Text = String.Concat(dr("ContactName"))
                txtContactMail.Text = String.Concat(dr("ContactMail"))

                ddlPackage.SelectedValue = String.Concat(dr("PackageNo"))

                If String.Concat(.Item("StartDate")) <> "" Then
                    txtStartDate.Text = DisplayShortDateTH(String.Concat(.Item("StartDate")))
                End If

                If String.Concat(.Item("DueDate")) <> "" Then
                    txtDueDate.Text = DisplayShortDateTH(String.Concat(.Item("DueDate")))
                End If

                LoadCompanyKeyword(String.Concat(.Item("UID")))
                cmdDelete.Enabled = True

                If String.Concat(dr("UID")) = "1" Then
                    cmdDelete.Visible = False
                Else
                    cmdDelete.Visible = True
                End If

                dr = Nothing
            End With

        Else

        End If
    End Sub
    Private Sub LoadCompanyKeyword(CompanyUID As Integer)
        Dim dtC As New DataTable
        dtC = ctlE.CompanyKeyword_Get(CompanyUID)
        If dtC.Rows.Count > 0 Then
            chkKeyword.ClearSelection()

            For i = 0 To chkKeyword.Items.Count - 1
                For n = 0 To dtC.Rows.Count - 1
                    If chkKeyword.Items(i).Value = dtC.Rows(n)("KeywordUID") Then
                        chkKeyword.Items(i).Selected = True
                    End If
                Next
            Next
        End If
    End Sub
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtCompanyCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุรหัสบริษัท');", True)
            Exit Sub
        End If
        If txtNameTH.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อบริษัท');", True)
            Exit Sub
        End If

        If StrNull2Zero(txtUserLimit.Text) <= 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุจำนวนพนักงานสูงสุด (Max Person)');", True)
            Exit Sub
        End If
        Dim CompanyCode As String
        'If StrNull2Zero(hdCompanyUID.Value) = 0 Then
        '    CompanyCode = ctlM.RunningNumber_New(CODE_COMPANY)
        'Else
        CompanyCode = txtCompanyCode.Text
        'End If

        Dim FactoryGroup As Integer
        If optFactoryGroup.Checked = True Then
            FactoryGroup = 1
        Else
            FactoryGroup = 0
        End If

        ctlE.Company_Save(StrNull2Zero(hdCompanyUID.Value), CompanyCode, txtNameTH.Text, txtNameEN.Text, txtBranch.Text, txtTaxID.Text, txtAddressNo.Text, txtLane.Text, txtRoad.Text, txtSubDistrict.Text, txtDistrict.Text, ddlProvince.SelectedValue, txtZipcode.Text, ddlCountry.SelectedValue, txtTel.Text, txtFax.Text, txtEmail.Text, txtWebsite.Text, ConvertBoolean2StatusFlag(chkStatus.Checked), StrNull2Zero(ddlBusinessType.SelectedValue), StrNull2Zero(ddlPhase.SelectedValue), StrNull2Zero(txtUserLimit.Text), Request.Cookies("iLaw")("userid"), 0, StrNull2Zero(ddlFactoryType.SelectedValue), FactoryGroup, StrNull2Zero(ddlPackage.SelectedValue), txtContactName.Text, txtContactMail.Text, txtAddressInvoice.Text, ConvertStrDate2DBDate(txtStartDate.Text), ConvertStrDate2DBDate(txtDueDate.Text))

        Dim CompanyUID As Integer

        If StrNull2Zero(hdCompanyUID.Value) = 0 Then
            'ctlM.RunningNumber_Update(CODE_COMPANY)
            CompanyUID = ctlE.Company_GetUID(CompanyCode)
            'GenInvoice(CompanyUID)
        Else
            CompanyUID = hdCompanyUID.Value
        End If


        Dim ctlU As New UserController
        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Company", "บันทึก/แก้ไข บริษัท/โรงงาน :{uid=" & CompanyUID & "}{code=" & CompanyCode & "}", "")

        ctlE.CompanyKeyword_Delete(CompanyUID)

        For i = 0 To chkKeyword.Items.Count - 1
            If chkKeyword.Items(i).Selected Then
                ctlE.CompanyKeyword_Save(CompanyUID, chkKeyword.Items(i).Value, Request.Cookies("iLaw")("userid"))
            End If
        Next

        If StrNull2Zero(ddlPackage.SelectedValue) <> 1 Then
            ctlM.PeriodTime_Create(CompanyUID)
        End If

        'txtCompanyCode.Text = CompanyCode
        hdCompanyUID.Value = CompanyUID
        cmdDelete.Enabled = True
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        'Response.Redirect("Company.aspx?p=complete")
    End Sub
    Private Sub GenInvoice(CustomerID As Integer)
        Dim InvNo, InvDate, ItemDesc As String
        InvDate = ConvertStrDate2DBDate(ctlM.GET_DATE_SERVER())
        InvNo = Year(InvDate).ToString() + Month(InvDate).ToString("0#") + "_" + txtCompanyCode.Text  'ctlM.RunningNumber_New(CODE_INVOICE) 'Year + Month + CustomerID

        'If InvNo = "Not Config Running" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้กำหนดรหัส Running Invoice');", True)
        '    Exit Sub
        'End If

        ItemDesc = "ค่าสมัครสมาชิก i-Law ระบบบริหารจัดการกฎหมาย Package " & ddlPackage.SelectedValue & " ระยะเวลา 1 ปี"
        Dim dtPk As New DataTable
        Dim UnitPrice, Vat, Amount, NetTotal As Double
        dtPk = ctlE.Company_GetPackageDetail(CustomerID)
        NetTotal = dtPk.Rows(0)("Amount")
        'มูลค่า VAT = ราคารวม VAT x 7/107
        'ราคาก่อน VAT = ราคารวม VAT x 100/107
        UnitPrice = Math.Round(NetTotal * 100 / 107, 2)
        Vat = Math.Round(NetTotal * 7 / 107, 2)
        Amount = UnitPrice

        ctlE.Invoice_Add(InvNo, InvDate, CustomerID, "1", ItemDesc, UnitPrice, Vat, Amount, NetTotal, Request.Cookies("iLaw")("userid"))

        'ctlM.RunningNumber_Update(CODE_INVOICE)

        LoadReport("payin", txtCompanyCode.Text, CustomerID)
        LoadReport("inv", InvNo, CustomerID)
        'SendEmail(CustomerID, InvNo, txtContactName.Text, txtContactMail.Text)
    End Sub
    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlE.Company_Delete(StrNull2Zero(hdCompanyUID.Value))
    End Sub

    Private Sub LoadReport(DocumentType As String, DocumentNo As String, CompanyUID As Integer)

        'System.Threading.Thread.Sleep(1000)
        'UpdateProgress1.Visible = True

        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True

        Dim xParam As New List(Of ReportParameter)

        Dim filePath As String = ""
        Select Case DocumentType
            Case "payin"
                filePath = "Invoice\Payin\"
                ReportFileName = "Payin" & txtCompanyCode.Text
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("Payin")
                xParam.Add(New ReportParameter("CompanyUID", CompanyUID.ToString()))
            Case "inv"
                filePath = "Invoice\Invoice\"
                ReportFileName = "ใบแจ้งค่าบริการInvoice" & DocumentNo
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("Invoice")
                xParam.Add(New ReportParameter("InvoiceNo", DocumentNo))
            Case Else
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("")
                xParam.Add(New ReportParameter("UserID", Request.Cookies("iLaw")("userid").ToString()))
        End Select

        ReportViewer1.ServerReport.SetParameters(xParam)

        Dim warnings As Warning()
        Dim streamIds As String()
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim extension As String = String.Empty

        ' Setup the report viewer object and get the array of bytes

        Dim result As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)


        '' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
        'Response.Buffer = True
        'Response.Clear()
        'Response.ContentType = mimeType
        'Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=C:\" & ReportFileName & ".pdf"))
        'Response.BinaryWrite(result)
        '' create the file
        ''Response.Flush()
        '' send it to the client to download

        'Try
        '    result = rs.Render(ReportPath, Format, historyID, "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>", parameters, credentials, showHideToggle, encoding, mimeType, reportHistoryParameters, warnings, streamIds)
        'Catch e As Exception
        '    Console.WriteLine(e.Detail.OuterXml)
        '    Dim sw As StreamWriter = New StreamWriter("C:\ReportServerError.XML")
        '    sw.Write(e.Detail.OuterXml)
        '    sw.Flush()
        '    sw.Close()
        'End Try

        Try
            Dim objfile As FileInfo = New FileInfo(Server.MapPath(filePath & ReportFileName & ".pdf"))
            If objfile.Exists Then
                objfile.Delete()
            End If

            Dim stream As FileStream = File.Create(Server.MapPath(filePath & ReportFileName & ".pdf"), result.Length)
            stream.Write(result, 0, result.Length)
            stream.Close()

        Catch e As Exception

        End Try


        ' Setup the report viewer object and get the array of bytes



        'Dim fs As FileStream = New FileStream(Server.MapPath("Invoice\" & ReportFileName & ".pdf"), FileMode.Open)
        'Dim data As Byte() = New Byte(fs.Length - 1) {}
        'fs.Write(result, 0, result.Length)
        'fs.Close()
    End Sub


End Class