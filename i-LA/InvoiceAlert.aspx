﻿<%@ Page Title="Customer Manager" MetaDescription="จัดการสมาชิก" Language="vb" AutoEventWireup="false"   CodeBehind="InvoiceAlert.aspx.vb" Inherits="iLA.InvoiceAlert" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Import Namespace="System.Data" %>  
    <!DOCTYPE html>

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title></title>
        <link href="https://fonts.googleapis.com/css?family=Sarabun:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!-- Bootstrap 3.3.7 
  < link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">-->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

        <link href="css/sb-admin.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/rajchasistyles.css">

               <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <style type="text/css">
            .modal-confirm {
                color: #434e65;
                width: 525px;
            }
            
            .modal-confirm .modal-content {
                padding: 20px;
                font-size: 16px;
                border-radius: 5px;
                border: none;
            }
            
            .modal-confirm .modal-header {
                background: #e85e6c;
                border-bottom: none;
                position: relative;
                text-align: center;
                margin: -20px -20px 0;
                border-radius: 5px 5px 0 0;
                padding: 35px;
            }
            
            .modal-confirm h4 {
                text-align: center;
                font-size: 36px;
                margin: 10px 0;
            }
            
            .modal-confirm .form-control,
            .modal-confirm .btn {
                min-height: 40px;
                border-radius: 3px;
            }
            
            .modal-confirm .close {
                position: absolute;
                top: 15px;
                right: 15px;
                color: #fff;
                text-shadow: none;
                opacity: 0.5;
            }
            
            .modal-confirm .close:hover {
                opacity: 0.8;
            }
            
            .modal-confirm .icon-box {
                color: #fff;
                width: 95px;
                height: 95px;
                display: inline-block;
                border-radius: 50%;
                z-index: 9;
                border: 5px solid #fff;
                padding: 15px;
                text-align: center;
            }
            
            .modal-confirm .icon-box i {
                font-size: 58px;
                margin: -2px 0 0 -2px;
            }
            
            .modal-confirm.modal-dialog {
                margin-top: 80px;
            }
            
            .modal-confirm .btn {
                color: #fff;
                border-radius: 4px;
                background: #eeb711;
                text-decoration: none;
                transition: all 0.4s;
                line-height: normal;
                border-radius: 30px;
                margin-top: 10px;
                padding: 6px 20px;
                min-width: 150px;
                border: none;
            }
            
            .modal-confirm .btn:hover,
            .modal-confirm .btn:focus {
                background: #eda645;
                outline: none;
            }
            
            .trigger-btn {
                display: inline-block;
                margin: 100px auto;
            }
        </style>
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min-login.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            function openModals(sender, title, message) {
                $("#spnTitle").text(title);
                $("#spnMsg").text(message);
                $('#modalPopUp').modal('show');
                $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
                return false;
            }
        </script>

        <!-- End modal -->                          
         
    <link href="assets/styles.css" rel="stylesheet" />

   

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>

    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      // Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
      // Based on https://gist.github.com/blixt/f17b47c62508be59987b
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
    </head>

    <body>
        <form id="form2" class="user" runat="server">
            <div class="container-alert">
                <div class="row">
                    <section class="col-lg-12 connectedSortable">
                        <div class="row">
                            <div class="col-md-12">
                                <table>
                                    <tr>
                                        <td class="logo">
                                            <img src="images/lalogo3.png" alt="" width="150">
                                        </td>
                                        <td>
                                            <div class="header-logo">i-Law</div>
                                            <div class="header-appname">
                                                ระบบบริหารจัดการกฎหมาย
                                            </div>
                                            <div class="header-text">
                                                บริษัท เอ็นพีซี เซฟตี้ แอนด์ เอ็นไวรอนเมนทอล เซอร์วิส จำกัด
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="row">
      <section class="col-lg-12 connectedSortable">  
<br />   <br />  
          <div id="lblAlert" runat="server" class="alert alert-success text-center" role="alert"><h4>Send mail alert success.</h4></div>

           <div id="lblNo" runat="server" class="alert alert-danger text-center" role="alert"><h4>ไม่มีรายการแจ้งเตือนสำหรับวันนี้.</h4></div>
     
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-license icon-gradient bg-success">
            </i>รายชื่อสมาชิกที่ส่งแจ้งเตือนวันนี้
            <div class="btn-actions-pane-right">
              
            </div>
        </div>     
              <div class="card-body">   
                  <div class=" table-responsive">
             <table id="tbdata" class="table table-hover">
                <thead>
                <tr>
                 
                  <th class="text-center">รหัสสมาชิก</th>
                  <th class="text-center">ชื่อบริษัท</th>
                  <th class="text-center">ชื่อผู้ติดต่อ</th>
                     <th class="text-center">อีเมล์</th>
                     <th class="text-center">วันที่เริ่มใช้งาน</th> 
                      <th class="text-center">วันหมดอายุ</th>
                    <th class="text-center">วันที่แจ้งเตือน (15)</th>       
                    <th class="text-center">วันที่แจ้งเตือน (30)</th>      
                    <th class="text-center">วันที่แจ้งเตือน (90)</th>      
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtCus.Rows %>
                <tr>               
                  <td class="text-center"><% =String.Concat(row("CompanyCode")) %></td>
                  <td><% =String.Concat(row("CompanyName")) %></td>
                  <td><% =String.Concat(row("ContactName")) %></td>               
                  <td><% =String.Concat(row("ContactMail")) %></td>
                   <td class="text-center"><% =String.Concat(row("StartDate")) %></td>   
                    <td class="text-center"><% =String.Concat(row("DueDate")) %></td>   
                      <td class="text-center"><% =String.Concat(row("Expire15")) %></td>  
                      <td class="text-center"><% =String.Concat(row("Expire30")) %></td>  
                      <td class="text-center"><% =String.Concat(row("Expire90")) %></td>
                </tr>
            <%  Next %>
                </tbody>               
              </table>      
                      </div>
              </div>
           
          </div> 
   <div class="text-center">
           <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="100%" ProcessingMode="Remote" ShowParameterPrompts="False" ShowPrintButton="true" BackColor="White" DocumentMapWidth="100%" ZoomMode="PageWidth" Visible="false">
    </rsweb:ReportViewer>
         </div>
</section>
                </div>
      
                <div class="footer text-center">
                    <footer class="main-footer-login">
                        <div class="text-center hidden-xs">
                            <strong>Copyright &copy; 2021-2022 <a href="https://www.npc-se.co.th">NPC S&E</a>.</strong> All rights reserved.
                            <br /> <b>Version</b>
                            <asp:Label ID="Label1" runat="server" Text="1.0.0"></asp:Label>&nbsp;&nbsp;<b>Release</b> 2020.12.28
                        </div>


                    </footer>
                </div>
            </div>

        </form>

        <!-- jQuery 3 -->
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        
<script>
    $(function () {
        $('#tbdata').DataTable()
        $('#tbdata2').DataTable()
        $('#MainContent_grdData').DataTable()
        $('#grdData2').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script> 


    </body>

    </html>  