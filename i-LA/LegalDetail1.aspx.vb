﻿Public Class LegalDetail1
    Inherits System.Web.UI.Page
    Dim ctlL As New LawController
    Public dtLegal As New DataTable
    Public dtPt As New DataTable
    Dim dt As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If
        dtLegal = ctlL.Legal_GetByUID(StrNull2Zero(Request("id")))
        If dtLegal.Rows.Count > 0 Then
            lblLegalName.Text = String.Concat(dtLegal.Rows(0)("LegalName"))
        End If
        LoadPracticeToGrid()

    End Sub
    Private Sub LoadPracticeToGrid()
        dtPt = ctlL.Practice_Get(StrNull2Zero(Request("id")))
        'With grdPractice
        '    .Visible = True
        '    .DataSource = dt
        '    .DataBind()
        'End With
    End Sub

    Private Sub cmdDowload_Click(sender As Object, e As EventArgs) Handles cmdDowload.Click

        Response.Redirect("Report/rptLawPractice.aspx?r=pt&lid=" & Request("id") & "&comp=" & Request.Cookies("iLaw")("LoginCompanyUID"))
    End Sub
End Class