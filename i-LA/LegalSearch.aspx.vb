﻿Imports System.Data
Public Class LegalSearch
    Inherits System.Web.UI.Page
    Dim ctlL As New LawController
    Dim ctlM As New MasterController
    Public dtLegal As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadKeyword()
            LoadPhase()
            LoadBusinessType()
            LoadMinistry()
            LoadYear()
            LoadFactoryType()
            LoadLawType()
            LoadLawMaster()

            LoadLegal()

        End If
    End Sub
    Private Sub LoadLegal()
        Dim sPhase As String = ""
        For i = 0 To chkPhase.Items.Count - 1
            If chkPhase.Items(i).Selected Then
                sPhase = sPhase + chkPhase.Items(i).Value.ToString() + ","
            End If
        Next
        If sPhase.Length > 0 Then
        sPhase = Left(sPhase, sPhase.Length - 1)
        End If


        Select Case Request("s")
            Case "0"
                dtLegal = ctlL.Legal_GetByPhaseUID(sPhase)
            Case "1"
                dtLegal = ctlL.Legal_GetByLawType(sPhase, StrNull2Zero(ddlType.Selectedvalue))
            Case "2"
                dtLegal = ctlL.Legal_GetByMinistryUID(sPhase, StrNull2Zero(ddlMinistry.SelectedValue))
            Case "3"
                dtLegal = ctlL.Legal_GetByYear(sPhase, StrNull2Zero(ddlYear.SelectedValue))
            Case "4"
                dtLegal = ctlL.Legal_GetByBusinessTypeUID(sPhase, StrNull2Zero(ddlBusinessType.SelectedValue), StrNull2Zero(ddlFactoryType.SelectedValue))
            Case "5"
                Dim sKeyword As String = ""
                For i = 0 To chkKeyword.Items.Count - 1
                    If chkKeyword.Items(i).Selected Then
                        sKeyword = sKeyword + chkKeyword.Items(i).Value + ","
                    End If
                Next
                If sKeyword.Length > 1 Then
                    sKeyword = Left(sKeyword, sKeyword.Length - 1)
                End If

                dtLegal = ctlL.Legal_GetByKeyword(sPhase, sKeyword)
            Case "6"
                dtLegal = ctlL.Legal_GetByLawMaster(sPhase, StrNull2Zero(ddlLawMaster.SelectedValue))
        End Select

    End Sub
    Private Sub LoadKeyword()
        chkKeyword.DataSource = ctlM.Keyword_Get()
        chkKeyword.DataTextField = "Name"
        chkKeyword.DataValueField = "UID"
        chkKeyword.DataBind()
    End Sub
    Private Sub LoadPhase()
        chkPhase.DataSource = ctlM.Phase_Get()
        chkPhase.DataTextField = "PhaseName"
        chkPhase.DataValueField = "UID"
        chkPhase.DataBind()
    End Sub
    Private Sub LoadLawType()
        ddlType.DataSource = ctlM.LawType_Get()
        ddlType.DataTextField = "Name"
        ddlType.DataValueField = "UID"
        ddlType.DataBind()
    End Sub
    Private Sub LoadLawMaster()
        ddlLawMaster.DataSource = ctlM.LawMaster_Get
        ddlLawMaster.DataTextField = "Name"
        ddlLawMaster.DataValueField = "UID"
        ddlLawMaster.DataBind()
    End Sub
    Private Sub LoadBusinessType()
        ddlBusinessType.DataSource = ctlM.BusinessType_Get()
        ddlBusinessType.DataTextField = "Name"
        ddlBusinessType.DataValueField = "UID"
        ddlBusinessType.DataBind()
    End Sub
    Private Sub LoadFactoryType()
        ddlFactoryType.DataSource = ctlM.FactoryType_Get()
        ddlFactoryType.DataTextField = "Name"
        ddlFactoryType.DataValueField = "UID"
        ddlFactoryType.DataBind()
    End Sub
    Private Sub LoadMinistry()
        ddlMinistry.DataSource = ctlM.Ministry_Get()
        ddlMinistry.DataTextField = "Name"
        ddlMinistry.DataValueField = "UID"
        ddlMinistry.DataBind()
    End Sub
    Private Sub LoadYear()
        ddlYear.DataSource = ctlL.LegalYear_Get()
        ddlYear.DataTextField = "YearName"
        ddlYear.DataValueField = "LegalYear"
        ddlYear.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        LoadLegal()
    End Sub

    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        chkKeyword.ClearSelection()
    End Sub
End Class