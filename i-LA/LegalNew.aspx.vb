﻿
Public Class LegalNew
    Inherits Page
    Public dtLegalNew As New DataTable

    Dim ctlR As New ReportController
    Public dtLegal As New DataTable
    Dim dtAll As New DataTable
    Dim dtNew As New DataTable
    Public Shared catebar1 As String
    Public Shared catebar2 As String
    Public Shared databar1 As String
    Public Shared databar2 As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            cmdDowload.Visible = False
        End If

        Dim ctlL As New LawController
        dtLegalNew = ctlL.Legal_GetLast()

        dtAll = ctlR.Legal_GetCountByType
        dtNew = ctlR.Legal_GetLastMonthCountByType

        catebar1 = ""
        catebar2 = ""
        databar1 = ""
        databar2 = ""

        For i = 0 To dtNew.Rows.Count - 1
            catebar1 = catebar1 + "'" + dtNew.Rows(i)("LawTypeName") + "'"
            databar1 = databar1 + dtNew.Rows(i)("LCount").ToString()

            If i < dtNew.Rows.Count - 1 Then
                catebar1 = catebar1 + ","
                databar1 = databar1 + ","
            End If
        Next


        For i = 0 To dtAll.Rows.Count - 1
            catebar2 = catebar2 + "'" + dtAll.Rows(i)("LawTypeName") + "'"
            databar2 = databar2 + dtAll.Rows(i)("LCount").ToString()

            If i < dtAll.Rows.Count - 1 Then
                catebar2 = catebar2 + ","
                databar2 = databar2 + ","
            End If
        Next


    End Sub

    Private Sub cmdDowload_Click(sender As Object, e As EventArgs) Handles cmdDowload.Click
        Response.Redirect("Report/rptLegalAll.aspx?r=new&comp=" & Request.Cookies("iLaw")("LoginCompanyUID"))

    End Sub
End Class