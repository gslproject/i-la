﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LegalDetail.aspx.vb" Inherits="iLA.LegalDetail" %>
<%@ Import Namespace="System.Data" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
 <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-ribbon icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>รายละเอียดกฎหมาย
                            <div class="page-title-subheading"></div>
                        </div>
                    </div>
                </div>
            </div>

    <section class="content">
       <div class="mb-3 card">
            <div class="card-header"><i class="header-icon fa fa-balance-scale icon-gradient bg-success">
                </i>
                <asp:Label ID="lblLegalName" runat="server" CssClass="text-primary" Text=""></asp:Label>
                <div class="btn-actions-pane-right">
                
                </div>
            </div> 
            <div class="card-body">
               <ul class="tabs-animated-shadow nav-justified tabs-animated nav">
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link active" id="tab-c1-0" data-toggle="tab" href="#tab-animated1-0">
                                                            <span class="nav-text">กฎหมายฉบับเต็ม</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link" id="tab-c1-1" data-toggle="tab" href="#tab-animated1-1">
                                                            <span class="nav-text">สาระสำคัญที่ต้องปฏิบัติ</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#tab-animated1-2">
                                                            <span class="nav-text">แบบฟอร์มที่เกี่ยวข้อง</span>
                                                        </a>
                                                    </li> 
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab-animated1-0" role="tabpanel">
                                                        <p class="mb-0">
<% If String.Concat(dtLegal.Rows(0)("FilePath")) <> "" %>
                                                            <iframe src="Documents/Legal/<% =String.Concat(dtLegal.Rows(0)("FilePath")) %>" title="" width="100%" height="1000" style="border:none;"></iframe>
                                                        <% End If %>
                                                        </p>
                                                    </div>
                                                    <div class="tab-pane" id="tab-animated1-1" role="tabpanel">

                                                          <asp:GridView ID="grdPractice" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-hover">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="ลำดับ" DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" /></asp:BoundField>
<asp:BoundField DataField="Descriptions" HeaderText="ข้อสาระสำคัญ">
                <ItemStyle HorizontalAlign="Left" />
</asp:BoundField>
            <asp:TemplateField>
              <itemtemplate>
                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' /> </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            <asp:TemplateField>
              <itemtemplate>
                <asp:ImageButton ID="imgDel" runat="server" 
             ImageUrl="images/delete.png" 
             CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' /> </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle ForeColor="White" HorizontalAlign="Center" CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True"                  VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>




                                                        <h6 class="text-blue">
                                                            สาระสำคัญที่ต้องปฏิบัติ
                                                        </h6>                                                    
                                                       
                                                          <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th style="width: 30px;" class="sorting_asc_disabled">ลำดับ</th>    
                    <th  style="width: 30px;"  class="text-center">รหัส</th>    
                  <th style="width: 1000px;" class="text-center">ข้อสาระสำคัญ</th>
                  <th  style="width: 150px;" class="text-center">ความถี่ที่ต้องปฏิบัติ</th>                 
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtPt.Rows %>
                <tr>
                     <td><% =String.Concat(row("nRow")) %> </td>
   <td class="text-center"><% =String.Concat(row("Code")) %></td>  
        <td><% =String.Concat(row("Descriptions")) %>    </td>                       
                  <td class="text-center"><% =String.Concat(row("RecurrenceText")) %></td>
                  
                </tr>
            <%  Next %>
                </tbody>               
              </table> 
                                                        </div>
                                                    <div class="tab-pane" id="tab-animated1-2" role="tabpanel">
                                                        <h6 class="text-blue">
                                                            แบบฟอร์มที่เกี่ยวข้อง
                                                        </h6>                                                      
                                                          <table id="tbdata2" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled">ลำดับ</th>    
                  <th class="text-center">ชื่อแบบฟอร์ม</th> 
                     <th class="sorting_asc_disabled sorting_desc_disabled text-center">ดาวน์โหลด</th> 
                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLegal.Rows %>
                <tr>
                  <td></td>
                  <td></td>                              
                  <td class="text-center"></td>
                </tr>
            <%  Next %>
                </tbody>               
              </table>     

                                                    </div>
                                                </div>
                                         
            <div class="row justify-content-center">
                  <asp:Button ID="cmdReject" cssclass="btn btn-danger" runat="server" Text="Reject"  data-dismiss="modal" Width="120px" />  
            </div>    
            </div>
                                        </div>

  
    </section>
</asp:Content>
