﻿Imports System.IO
Imports Newtonsoft.Json
Public Class Site
    Inherits System.Web.UI.MasterPage
    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Request.Cookies("iLaw")) Or IsNothing(Request.Cookies("iLaw2")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadUserDetail()
            LoadUserRole()
        End If
        LoadCalendarData()
    End Sub

    Private Sub LoadUserDetail()
        Dim ctlU As New UserController
        dt = ctlU.User_GetByUserID(Request.Cookies("iLaw")("userid"))
        If dt.Rows.Count > 0 Then
            lblUserName1.Text = String.Concat(dt.Rows(0)("Name"))
            lblUsername2.Text = String.Concat(dt.Rows(0)("Name"))

            lblProfileDesc.Text = Request.Cookies("iLaw2")("PeriodName")
            lblUserDesc.Text = Request.Cookies("iLaw2")("PeriodName")
            lblPeriod.Text = Request.Cookies("iLaw2")("PeriodName")
            lblUserDept.Text = String.Concat(dt.Rows(0)("DepartmentName"))

            If DBNull2Str(dt.Rows(0).Item("ImagePath")) <> "" Then
                Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & PictureUser & "/" & dt.Rows(0).Item("ImagePath")))

                If objfile.Exists Then
                    imgUser1.ImageUrl = "~/" & PictureUser & "/" & dt.Rows(0).Item("ImagePath")
                    imgUser2.ImageUrl = "~/" & PictureUser & "/" & dt.Rows(0).Item("ImagePath")
                Else
                    imgUser1.ImageUrl = "~/" & PictureUser & "/user_blank.jpg"
                    imgUser2.ImageUrl = "~/" & PictureUser & "/user_blank.jpg"
                End If

            End If

        End If
        dt = Nothing
    End Sub


    Private Sub LoadUserRole()
        Dim ctlU As New UserController
        dt = ctlU.User_GetUserRole(Request.Cookies("iLaw")("userid"))
        If dt.Rows.Count > 0 Then
            lblUserRole.Text = ""
            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i)("UserGroupUID")
                    Case "1"
                        lblUserRole.Text = lblUserRole.Text & "<div class='badge badge-primary'>" &
                                                                String.Concat(dt.Rows(0)("Name")) & "</div>"
                    Case "2"
                        lblUserRole.Text = lblUserRole.Text & "<div class='badge badge-info'>" &
                                                                String.Concat(dt.Rows(0)("Name")) & "</div>"
                    Case "3"
                        lblUserRole.Text = lblUserRole.Text & "<div class='badge badge-danger'>" &
                                                                   String.Concat(dt.Rows(0)("Name")) & "</div>"
                    Case "9"
                        lblUserRole.Text = lblUserRole.Text & "<div class='badge badge-success'>" &
                                                                String.Concat(dt.Rows(0)("Name")) & "</div>"
                End Select

            Next
        End If
        dt = Nothing
    End Sub

    Dim ctlE As New LawController
    Dim dtCalendar As New DataTable
    Public Shared json As String
    Private Sub LoadCalendarData()
        'If Request.Cookies("iLaw")("ROLE_APP") = True Or Request.Cookies("iLaw")("ROLE_ADM") = True Or Request.Cookies("iLaw")("ROLE_SPA") = True Then
        '    dtCalendar = ctlE.Event_GetByAdmin(StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iLaw")("LoginPersonUID")))
        'Else
        '    dtCalendar = ctlE.Event_GetByUser(StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iLaw")("LoginPersonUID")))
        'End If

        dtCalendar = ctlE.Event_GetByAdmin(1, 1)

        json = JsonConvert.SerializeObject(dtCalendar, Formatting.Indented)
    End Sub



End Class