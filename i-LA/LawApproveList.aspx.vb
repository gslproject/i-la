﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class LawApproveList
    Inherits System.Web.UI.Page
    Public dtApv As New DataTable
    Dim ctlL As New LawController
    'Dim ctlM As New MasterController
    'Private Const UploadDirectory As String = "~/imgLegal/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then

            'Request.Cookies("iLaw")("Legalimg1") = ""
            'Request.Cookies("iLaw")("picname1") = ""
            'Request.Cookies("iLaw")("Legalimg2") = ""
            'Request.Cookies("iLaw")("picname2") = ""
            'Request.Cookies("iLaw")("Legalimg3") = ""
            'Request.Cookies("iLaw")("picname3") = ""

            LoadLegalData()

        End If
        cmdSave.Attributes.Add("onClick", "javascript:return confirm(""ต้องการบันทึก Approve ผลการการพิจารณาใช่หรือไม่?"");")

    End Sub

    Private Sub LoadLegalData()
        Dim dt As New DataTable
        dt = ctlL.Legal_GetByUID(Request("lid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdLegalUID.Value = String.Concat(.Item("UID"))
                lblLegalCode.Text = String.Concat(.Item("Code"))
                lblLegalName.Text = String.Concat(.Item("LegalName"))
                LoadPracticeToGrid()
            End With
        Else
        End If
    End Sub


    Private Sub LoadPracticeToGrid()
        dtApv = ctlL.LegalAction_GetForApproval(StrNull2Zero(hdLegalUID.Value), Request.Cookies("iLaw2")("PeriodID"))

        If dtApv.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dtApv
                .DataBind()
            End With
            cmdSave.Visible = True
        Else
            cmdSave.Visible = False
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        Dim ActionUID As Integer
        Dim ActionComment, ActionStatus, ApproveStatus As String

        For i = 0 To grdData.Rows.Count - 1
            With grdData
                Dim ddlA As DropDownList = grdData.Rows(i).Cells(8).FindControl("ddlAction")
                Dim chkS As CheckBox = grdData.Rows(i).Cells(0).FindControl("chkSelect")
                If chkS.Checked Then
                    If ddlA.SelectedValue <> Nothing Then
                        ActionUID = DBNull2Zero(grdData.DataKeys(i).Value)
                        Dim txtC As TextBox = grdData.Rows(i).Cells(9).FindControl("txtComment")
                        If ddlA.SelectedValue = "Y" Then
                            ActionStatus = ACTION_APPROVED
                            ApproveStatus = "Y"
                        Else
                            ActionStatus = ACTION_REJECT
                            ApproveStatus = "N"
                        End If
                        ActionComment = txtC.Text
                        ctlL.LegalAction_UpdateApproveStatus(ActionUID, ApproveStatus, ActionStatus, ActionComment, Request.Cookies("iLaw")("UserID"))
                    End If
                End If
            End With
        Next

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        LoadPracticeToGrid()

    End Sub


End Class