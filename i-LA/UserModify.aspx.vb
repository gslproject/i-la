﻿Public Class UserModify
    Inherits System.Web.UI.Page
    Dim ctlC As New CompanyController
    Dim ctlU As New UserController
    Dim ctlR As New UserRoleController
    Dim enc As New CryptographyEngine
    Dim CompanyPackage As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            lblUserID.Text = ""
            cmdDelete.Enabled = False

            If Not Request("uid") = Nothing Then
                CompanyPackage = ctlC.PackageNo_GetByUserID(Request("uid"))
            Else
                CompanyPackage = ctlC.Company_GetPackage(Request.Cookies("iLaw")("LoginCompanyUID"))
            End If

            BindUserGroup()
            LoadCompany()
            LoadDepartment(ddlCompany.SelectedValue)

            If Request.Cookies("iLaw")("ROLE_ADM") = True Then

            Else
                ddlCompany.Enabled = False
            End If


            If Not Request("uid") = Nothing Then
                    EditUser()
                End If
            End If

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
        cmdDelete.Attributes.Add("onClick", scriptString)

    End Sub
    Private Sub LoadCompany()

        ddlCompany.DataSource = ctlC.Company_GetActive()
        ddlCompany.DataTextField = "CompanyName"
        ddlCompany.DataValueField = "UID"
        ddlCompany.DataBind()
        ddlCompany.SelectedValue = Request.Cookies("iLaw")("LoginCompanyUID")

    End Sub
    Private Sub LoadDepartment(CompanyUID As Integer)
        Dim ctlM As New MasterController
        ddlDepartment.DataSource = ctlM.Department_GetActive(CompanyUID)
        ddlDepartment.DataTextField = "DepartmentName"
        ddlDepartment.DataValueField = "DepartmentUID"
        ddlDepartment.DataBind()
    End Sub
    Private Sub BindUserGroup()
        'Dim dtG As New DataTable
        'Dim ctlG As New UserRoleController
        'dtG = ctlG.UserGroup_Get()
        With chkGroup
            .Items.Clear()

            If CompanyPackage = 1 Then
                .Items.Add("User")
                .Items(0).Value = "2"
            Else
                If Request.Cookies("iLaw")("ROLE_ADM") = True Or Request.Cookies("iLaw")("ROLE_SPA") = True Then
                    .Items.Add("Customer Admin")
                    .Items(0).Value = "1"
                    .Items.Add("User")
                    .Items(1).Value = "2"
                    .Items.Add("Approver")
                    .Items(2).Value = "3"
                End If
                If Request.Cookies("iLaw")("ROLE_SPA") = True Then
                    .Items.Add("NPC-SE Administrator")
                    .Items(3).Value = "9"
                End If
            End If

        End With
    End Sub


    'Private Sub LoadCompany()
    '    Dim ctlC As New CompanyController
    '    ddlCompany.DataSource = ctlC.Company_GetActive()
    '    ddlCompany.DataTextField = "CompanyName"
    '    ddlCompany.DataValueField = "UID"
    '    ddlCompany.DataBind()
    'End Sub

    'Private Sub EditUserCompanyToGrid()
    '    Dim dtC As New DataTable

    '    If lblUserID.Text <> "" Then
    '        dtC = ctlU.UserCompany_GetByUserID(StrNull2Zero(lblUserID.Text))
    '    Else
    '        dtC = ctlU.UserCompany_GetByUsername(txtUsername.Text)
    '    End If


    '    If dtC.Rows.Count > 0 Then
    '        With grdCompany
    '            .Visible = True
    '            .DataSource = dtC
    '            .DataBind()
    '        End With
    '    Else
    '        grdCompany.DataSource = Nothing
    '        grdCompany.Visible = False
    '    End If
    '    dtC = Nothing

    'End Sub

    Private Sub EditUser()
        Dim dt As New DataTable
        dt = ctlU.User_GetByUserID(Request("uid"))

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblUserID.Text = String.Concat(.Item("UserID"))
                txtFirstName.Text = String.Concat(.Item("FName"))
                txtLastName.Text = String.Concat(.Item("Lname"))
                txtTel.Text = String.Concat(.Item("Telephone"))
                txtEmail.Text = String.Concat(.Item("Email"))
                ddlCompany.SelectedValue = String.Concat(.Item("CompanyUID"))
                LoadDepartment(StrNull2Zero(.Item("CompanyUID")))
                ddlDepartment.SelectedValue = String.Concat(.Item("DepartmentUID"))

                'txtActiveDate.Text = DisplayShortDateTH(String.Concat(.Item("ActiveDate")))
                'txtActiveTo.Text = DisplayShortDateTH(String.Concat(.Item("ActiveTo")))

                Session("password") = ""
                txtUsername.Text = String.Concat(.Item("Username"))
                'txtPassword.Text = String.Concat(.Item("Passwords"))
                txtPassword.Text = "**********" ' enc.DecryptString(String.Concat(dt.Rows(0)("Passwords")), True)
                Session("password") = enc.DecryptString(String.Concat(dt.Rows(0)("Passwords")), True)
                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(.Item("StatusFlag")))

                'chkGroup.SelectedValue = String.Concat(dt.Rows(0)("UserGroupUID"))

                EditUserGroup()
                cmdDelete.Enabled = True
            End With

        End If
    End Sub

    Private Sub EditUserGroup()
        Dim dtG As New DataTable
        Dim ctlR As New UserRoleController
        chkGroup.ClearSelection()

        dtG = ctlR.UserAccessGroup_Get(lblUserID.Text)
        If dtG.Rows.Count > 0 Then
            For i = 0 To dtG.Rows.Count - 1
                For n = 0 To chkGroup.Items.Count - 1
                    If dtG.Rows(i)("UserGroupUID") = chkGroup.Items(n).Value Then
                        chkGroup.Items(n).Selected = True
                    End If
                Next
            Next
        Else
            chkGroup.Items(1).Selected = True
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If ddlCompany.SelectedIndex = -1 Or ddlCompany.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกหน่วยงานก่อน');", True)
            Exit Sub
        End If

        If txtFirstName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกชื่อ');", True)
            Exit Sub
        End If
        If txtLastName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกนามสกุล');", True)
            Exit Sub
        End If
        If txtEmail.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอก E-mail');", True)
            Exit Sub
        End If

        If txtUsername.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอก Username');", True)
            Exit Sub
        End If
        If txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุ Password');", True)
            Exit Sub
        End If

        'If chkGroup.SelectedValue = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุสิทธิ์ผู้ใช้งานก่อน');", True)
        '    Exit Sub
        'End If

        If lblUserID.Text = "" Then
            If ctlU.User_CheckDuplicate(txtUsername.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username นี้มีในระบบแล้ว');", True)
                Exit Sub
            End If
        End If
        Dim Password As String = ""
        If txtPassword.Text <> "**********" Then
            Password = enc.EncryptString(txtPassword.Text, True)
        Else
            Password = enc.EncryptString(Session("password"), True)
        End If

        ctlU.User_Save(StrNull2Zero(lblUserID.Text), txtUsername.Text, Password, txtFirstName.Text, txtLastName.Text, StrNull2Zero(ddlCompany.SelectedValue), StrNull2Zero(ddlDepartment.SelectedValue), txtTel.Text, txtEmail.Text, ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("iLaw")("userid"))


        Dim UserID As Integer
        UserID = ctlU.User_GetUID(txtUsername.Text)
        lblUserID.Text = UserID
        cmdDelete.Enabled = True

        For i = 0 To chkGroup.Items.Count - 1
            'ctlU.UserRole_Save(UserID, chkGroup.Items(i).Value, ConvertBoolean2YN(chkGroup.Items(i).Selected), Request.Cookies("iLaw")("uid"))
            ctlR.UserAccessGroup_Save(UserID, chkGroup.Items(i).Value, ConvertBoolean2StatusFlag(chkGroup.Items(i).Selected), Request.Cookies("iLaw")("uid"))
        Next

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        'Response.Redirect("Users?m=user&s=u")

    End Sub
    Private Sub ClearData()
        lblUserID.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtUsername.Text = ""
        txtPassword.Text = ""
        txtTel.Text = ""
        txtEmail.Text = ""
        cmdDelete.Enabled = False
    End Sub
    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlU.User_Delete(lblUserID.Text)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการทำงาน','ลบข้อมูลเรียบร้อย');", True)

        Response.Redirect("Users?m=u&s=u")
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    'Protected Sub grdCompany_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCompany.RowCommand
    '    If TypeOf e.CommandSource Is WebControls.ImageButton Then
    '        Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
    '        Select Case ButtonPressed.ID
    '            Case "imgDel"
    '                If ctlU.UserCompany_Delete(e.CommandArgument) Then
    '                    ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), "DEL", "UserCompany", "ลบ UserCompany :" & e.CommandArgument, "")
    '                    EditUserCompanyToGrid()
    '                Else
    '                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
    '                End If
    '        End Select
    '    End If
    'End Sub

    'Protected Sub cmdAddCompany_Click(sender As Object, e As EventArgs) Handles cmdAddCompany.Click
    '    If ddlCompany.SelectedValue = "0" Then
    '        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกบริษัทที่ให้สิทธิก่อน');", True)
    '        Exit Sub
    '    End If

    '    ctlU.UserCompany_Save(StrNull2Zero(lblUserID.Text), txtUsername.Text, ddlCompany.SelectedValue)
    '    ddlCompany.SelectedIndex =0
    '    EditUserCompanyToGrid()
    'End Sub
End Class