﻿
Public Class rptLawPractice
    Inherits Page
    Public dtLawP As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim ctlL As New LawController
        Dim ctlR As New ReportController
        Dim fileName As String = ""
        Select Case Request("r")
            Case "pt"
                dtLawP = ctlR.RPT_Practice_GetByLegal(StrNull2Zero(Request("lid")))
                fileName = "LawPractice_" & Request("lid") & ".xls"
            Case Else

        End Select

        'Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) & "\Downloads\"
        Response.AppendHeader("content-disposition", "attachment;filename=" & fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        'Context.Response.ContentType = "application/x-zip-compressed"
        'Context.Response.AddHeader("Content-Disposition", "attachment; filename=Legal.xls")
        'Context.Response.Clear()
        'Context.Response.WriteFile("Legal.xls")
        'Context.Response.End()
    End Sub
End Class