﻿<%@ Page Title="" Language="VB" AutoEventWireup="true" CodeBehind="rptLegalAll.aspx.vb" Inherits="iLA.rptLegalAll" %>
<%@ Import Namespace="System.Data" %>  
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <meta charset="utf-8">     
        <title>Export Legal to excell</title>  
    </head>
    <body style="margin:0;padding:0;">
        <form id="form1" runat="server"> 
              <table id="tableExport" border="1" width="100%">
                <thead>
                <tr>                 
                  <th>รหัสกฎหมาย</th>
                  <th>ชื่อกฎหมาย</th>
                  <th>ประเภท</th>
                     <th>กฎหมายแม่บท</th>
                     <th>กระทรวง</th>  
                      <th>วันที่ประกาศ</th>
                      <th>วันที่มีผลบังคับใช้</th>
                          <th>สถานะ</th>                            
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLegalNew.Rows %>
                <tr>               

                  <td><% =String.Concat(row("Code")) %></td>
                  <td><% =String.Concat(row("LegalName")) %></td>
                  <td><% =String.Concat(row("LawTypeName")) %></td>
                         <td><% =String.Concat(row("LawMaster")) %></td>
                         <td><% =String.Concat(row("MinistryName")) %></td>                 
                  <td><% =String.Concat(row("IssueDate")) %></td>
                   <td><% =String.Concat(row("StartDate")) %></td>   
                         <td><% =IIf(String.Concat(row("StatusFlag")) = "A", "ประกาศใช้", "") %> </td>                   
                </tr>
            <%  Next %>
                </tbody>               
              </table> 
        </form>     
    </body>
    </html>