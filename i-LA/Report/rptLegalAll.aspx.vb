﻿
Public Class rptLegalAll
    Inherits Page
    Public dtLegalNew As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'Dim ctlL As New LawController
        Dim ctlR As New ReportController
        Dim fileName As String = ""
        Select Case Request("r")
            Case "new"
                fileName = "Legal_" & Request("lid") & ".xls"
                If Request.Cookies("iLaw")("ROLE_SPA") = True Then
                    dtLegalNew = ctlR.Legal_GetLast()
                Else
                    dtLegalNew = ctlR.Legal_GetLast()
                End If
            Case "law"
                dtLegalNew = ctlR.Legal_GetByCompany(Request("comp"))
                fileName = "Legal_" & Request("comp") & ".xls"
            Case "pt"
                dtLegalNew = ctlR.RPT_Practice_GetByLegal(StrNull2Zero(Request("lid")))
                fileName = "LawPractice_" & Request("lid") & ".xls"
            Case "all"
                If Request("m") = "d" Then
                    dtLegalNew = ctlR.Legal_GetByCompany(Request.Cookies("iLaw")("LoginCompanyUID"))
                    fileName = "Legal_" & Request.Cookies("iLaw")("LoginCompanyUID") & ".xls"
                End If
            Case Else

        End Select



        'Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) & "\Downloads\"
        Response.AppendHeader("content-disposition", "attachment;filename=" & fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        'Context.Response.ContentType = "application/x-zip-compressed"
        'Context.Response.AddHeader("Content-Disposition", "attachment; filename=Legal.xls")
        'Context.Response.Clear()
        'Context.Response.WriteFile("Legal.xls")
        'Context.Response.End()
    End Sub
End Class