﻿<%@ Page Title="Home Page" Language="VB" AutoEventWireup="true" CodeBehind="rptLawPractice.aspx.vb" Inherits="iLA.rptLawPractice" %>
<%@ Import Namespace="System.Data" %>  
    <!DOCTYPE html>

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Export Legal to excell</title>  
    </head>
    <body style="margin:0;padding:0;">
        <form id="form1" runat="server"> 
              <table id="tbdata" border="1" width="100%">
                <thead>
                <tr>
                  <th>รหัสข้อสาระ</th> 
                     <th>ข้อสาระสำคัญที่ต้องปฏิบัติ</th>
                     <th>ความถี่</th>
                  <th>รหัสกฎหมาย</th>
                  <th>ชื่อกฎหมาย</th>
                  <th>ประเภท</th>
                     <th>กฎหมายแม่บท</th>
                     <th>กระทรวง</th>  
                      <th>วันที่ประกาศ</th>
                      <th>วันที่มีผลบังคับใช้</th>               
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLawP.Rows %>
                <tr>               
                     <td><% =String.Concat(row("Code")) %></td>
                      <td><% =String.Concat(row("Descriptions")) %></td>
                      <td><% =String.Concat(row("RecurrenceText")) %></td>
                  <td><% =String.Concat(row("LegalCode")) %></td>
                  <td><% =String.Concat(row("LegalName")) %></td>
                  <td><% =String.Concat(row("LawTypeName")) %></td>
                         <td><% =String.Concat(row("LawMasterName")) %></td>
                         <td><% =String.Concat(row("MinistryName")) %></td>                 
                  <td><% =String.Concat(row("IssueDate")) %></td>
                   <td><% =String.Concat(row("StartDate")) %></td>  
                     
                </tr>
            <%  Next %>
                </tbody>               
              </table> 
        </form>     
    </body>

    </html>