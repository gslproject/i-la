﻿<%@ Page Title="Customer Manager" MetaDescription="จัดการสมาชิก" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CustomerManage.aspx.vb" Inherits="iLA.CustomerManage" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-config icon-gradient bg-success"></i>
                        </div>
                        <div><%: Title %>    
                            <div class="page-title-subheading"><%: MetaDescription %>   </div>
                        </div>
                    </div>
                </div>
      </div>   

    <section class="content">   
      <div class="row"> 
        <section class="col-lg-12 connectedSortable"> 
          <div class="main-card mb-3 card">
    <div class="card-header"><i class="header-icon lnr-home icon-gradient bg-primary">
            </i>ข้อมูลสมาชิก : <asp:Label ID="txtCompanyCode" runat="server" cssclass="text-center" ></asp:Label>  </h3><asp:HiddenField ID="hdCompanyUID" runat="server" />    
            <div class="btn-actions-pane-right"> 
            </div>
        </div>
        <div class="card-body"> 
                 <div class="row">           
            <div class="col-md-8">
              <div class="form-group">
                <label></label> <asp:Label ID="txtNameTH" runat="server" CssClass="text-bold text-blue"></asp:Label>
                 </div> 
            </div>
                            
                      <div class="col-md-4">
              <div class="form-group">
                <label>สาขา : </label> <asp:Label ID="txtBranch" runat="server" ></asp:Label>
                 </div>
            </div>
                    </div>
           
                <div class="row">
                     <div class="col-md-8">
                <div class="form-group">
                    <label>ที่อยู่ : </label>
                   <asp:Label ID="lblAddress" runat="server" ></asp:Label>
                </div>
              </div>    
                  <div class="col-md-4">
              <div class="form-group">
                <label>เลขประจำตัวผู้เสียภาษี : </label> <asp:Label ID="txtTaxID" runat="server" ></asp:Label>
                 </div>
            </div>    

            </div> 
              <div class="row">   
          
               <div class="col-md-8">
              <div class="form-group">
                <label>Tel : </label> <asp:Label ID="txtTel" runat="server" ></asp:Label>
                 </div>
            </div>
                 <div class="col-md-4">
              <div class="form-group">
                <label>Fax : </label> <asp:Label ID="txtFax" runat="server" ></asp:Label>
                 </div>
            </div> 
            </div>         
         
             <div class="row">                          
              <div class="col-md-8">
                <div class="form-group">
                    <label>ที่อยู่สำหรับออกใบเสร็จ : </label>                 
                   <asp:Label ID="txtAddressInvoice" runat="server" ></asp:Label>
                </div>
              </div>
                 
                   <div class="col-md-4">
              <div class="form-group">
                            <label>สถานะ</label>  <asp:Label ID="lblStatus" runat="server" Text="Active" CssClass="badge badge-pill badge-success"></asp:Label>
                   
                  </div>
            </div> 
             </div> 
                
            </div> 
            
</div>
           
        <div class="main-card mb-3 card">
    <div class="card-header"><i class="header-icon lnr-gift icon-gradient bg-primary">
            </i>Package การใช้งาน
            <div class="btn-actions-pane-right"> 
            </div>
        </div>
        <div class="card-body"> 
             <div class="row"> 
               <div class="col-md-3">
              <div class="form-group">
                <label>Package</label>
                  <asp:DropDownList ID="ddlPackage" runat="server" cssclass="form-control select2" Width="100%" >
                      <asp:ListItem Selected="True" Value="1">Package 1</asp:ListItem>
                      <asp:ListItem Value="2">Package 2</asp:ListItem>
                  </asp:DropDownList>
                 </div>
            </div>                 
                  <div class="col-md-3">
                                        <div class="form-group">
                                            <label>วันที่เริ่มใช้งาน</label>
                                            <div class="input-group">                                             
                                                <asp:textbox ID="txtStartDate" runat="server" CssClass="form-control"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:textbox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                   <div class="col-md-3">
                                        <div class="form-group">
                                            <label>วันที่หมดอายุ</label>
                                            <div class="input-group">                                             
                                                <asp:TextBox ID="txtDueDate" runat="server" CssClass="form-control"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                  <div class="col-md-3">
              <div class="form-group">
                            <label>สถานะ</label>  
                  <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2">
                      <asp:ListItem Selected="True" Value="A">Active</asp:ListItem>
                      <asp:ListItem Value="D">Deactive</asp:ListItem>
                      <asp:ListItem Value="O">Overdue</asp:ListItem>
                            </asp:DropDownList>

                   
                  </div>
            </div> 
        </div>
             <div class="row">                          
              <div class="col-md-4">
                <div class="form-group">
                    <label>ชื่อ(ไม่ต้องระบุคำนำหน้า)</label> 
                    <asp:TextBox ID="txtFname" runat="server" CssClass="form-control text-center"  placeholder="ชื่อผู้ติดต่อ" ></asp:TextBox>            
                </div>
              </div>
                   <div class="col-md-4">
                <div class="form-group">
                    <label>นามสกุล</label> 
                    <asp:TextBox ID="txtLname" runat="server" CssClass="form-control text-center"  placeholder="นามสกุลผู้ติดต่อ" ></asp:TextBox>            
                </div>
              </div>
                 <div class="col-md-4">
              <div class="form-group">
                <label>E-mail</label>
                  <asp:TextBox ID="txtContactMail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
                 </div>
            </div>
   </div> 
  <div class="row">  
                <div class="col-md-6">
              <div class="form-group">
                <label>Username (Admin)</label>
                  <asp:TextBox ID="txtUsername" runat="server" cssclass="form-control text-center" placeholder="ชื่อผู้ใช้งาน"></asp:TextBox>   
                 </div> 
            </div>
                  <div class="col-md-6">
              <div class="form-group">
                <label>Password</label>
                  <asp:TextBox ID="txtPassword" runat="server" cssclass="form-control text-center" placeholder="รหัสผ่าน" TextMode="Password"></asp:TextBox>   
                 </div> 
            </div>
               
              
                    </div>
             <div class="row"> 
                 <div class="col-md-12">
              <div class="text-center"> 
                  <asp:Button ID="cmdSave" cssclass="btn btn-primary" runat="server" Text="Save"  data-dismiss="modal" Width="100px" />                 
                  </div>
            </div> 
  </div> 
</div>
             <div class="card-footer justify-content-center">    
                   <asp:Button ID="cmdSendInvoice" cssclass="btn btn-primary" runat="server" Text="ส่งอีเมล์แจ้งค่าบริการ"/>
                 <asp:Button ID="cmdConfirm" cssclass="btn btn-primary" runat="server" Text="ส่งอีเมล์ยืนยันการต่ออายุ"/>
                  <div id="btnInvoice" runat="server" class="dropdown d-inline-block">
<button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle btn btn-primary">สร้างใบแจ้งค่าบริการ</button>
<div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-hover-primary dropdown-menu" style="">
<button id="btnCreateInvoice15" runat="server" type="button" tabindex="0" class="dropdown-item">ใบแจ้งค่าบริการล่วงหน้า 15 วัน</button>
<button id="btnCreateInvoice30" runat="server" type="button" tabindex="0" class="dropdown-item">ใบแจ้งค่าบริการล่วงหน้า 30 วัน</button>
<button id="btnCreateInvoice90" runat="server" type="button" tabindex="0" class="dropdown-item">ใบแจ้งค่าบริการล่วงหน้า 3 เดือน</button>
</div>
</div>
              </div>  
</div>
       </section>
          <section class="col-lg-6 connectedSortable">    
    </section>
       </div>
         <div class="row"> 
        <section class="col-lg-6 connectedSortable">
        </section>
          <section class="col-lg-6 connectedSortable">
        </section>
       </div>

        
        <div class="text-center">
           <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="100%" ProcessingMode="Remote" ShowParameterPrompts="False" ShowPrintButton="true" BackColor="White" DocumentMapWidth="100%" ZoomMode="PageWidth" Visible="false">
    </rsweb:ReportViewer>
         </div>
      </section>
</asp:Content>