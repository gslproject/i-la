﻿<%@ Page Title="Home Page" Language="VB" AutoEventWireup="false"  MasterPageFile="~/Site.Master"  CodeBehind="ChartLegalAll.aspx.vb" Inherits="iLA.ChartLegalAll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
    <link href="assets/styles.css" rel="stylesheet" />  

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>

    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      // Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
      // Based on https://gist.github.com/blixt/f17b47c62508be59987b
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
      <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-ribbon icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>จำนวนกฎหมายหมด
                            <div class="page-title-subheading"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-login">        
                <div class="row">                    
                    <section class="col-lg-12 connectedSortable">  
                        
       <div class="row">  
          <div class="col-md-6">
                <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-license icon-gradient bg-success">
            </i>กฎหมายทั้งหมดแยกตามสถานะ (ฉบับ)
            <div class="btn-actions-pane-right">
                
            </div>
        </div>     
              <div class="card-body">
              <div id="chart"></div>
    </div>
                    </div>
              
          </div>
          
          <div class="col-md-6">
                <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-license icon-gradient bg-success">
            </i>กฎหมายทั้งหมดแยกตามประเภท (ฉบับ)
            <div class="btn-actions-pane-right">
                
            </div>
        </div>     
              <div class="card-body">
              <div id="chart2"></div>
           </div>
                    </div>

          </div>
           </div>

                    </section>



                </div>
      
            </div>

     
     
        

    <script>
      
        var options = {
          series:  <%=datachart1 %>,
          chart: {
          width: 380,
          type: 'pie',
        },
        labels: ['สอดคล้อง', 'ไม่สอดคล้อง', 'อยู่ระหว่างการประเมิน'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
          chart.render();


      var options2 = {
          series: [{
          data: [<%=databar2 %>]
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
            bar: {
              borderRadius: 10,
            columnWidth: '45%',
              distributed: true,
            dataLabels: {
              position: 'top', // top, center, bottom
            }
          }
        },
        dataLabels: {
          enabled: true,
            offsetX: -6,          
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [<%=catebar2 %>
          ],
          labels: {
            style: {
              colors: colors,
              fontSize: '12px'
            }
          }
          },
         yaxis: {
          axisBorder: {
            show: true
          },
          axisTicks: {
            show: true,
          },
          labels: {
            show: true,
            formatter: function (val) {
              return val;
            }
          }
        
        }
        };

        var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);
        chart2.render();    
      
      
    </script>

    </asp:Content>
