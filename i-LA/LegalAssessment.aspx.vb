﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class LegalAssessment
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LawController
    Dim ctlM As New MasterController
    Dim ctlU As New UserController
    'Private Const UploadDirectory As String = "~/imgLegal/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            cmdSend.Enabled = False
            hlnkDoc.Visible = False
            ''Request.Cookies("iLaw")("Legalimg")="blankimage.png"
            'Request.Cookies("iLaw")("Legalimg1") = ""
            'Request.Cookies("iLaw")("picname1") = ""
            'Request.Cookies("iLaw")("Legalimg2") = ""
            'Request.Cookies("iLaw")("picname2") = ""
            'Request.Cookies("iLaw")("Legalimg3") = ""
            'Request.Cookies("iLaw")("picname3") = ""

            If Not Request("id") Is Nothing Then
                LoadLegalData(Request("id"))
                LoadActionData()
                'Else
                '    If Not Request("lid") Is Nothing Then
                '        LoadLegalData(Request("lid"))
                '        LoadPracticeDetail(Request("pid"))
                '    End If
            End If
        End If

        cmdSend.Attributes.Add("onClick", "javascript:return confirm(""ต้องการส่ง Approver พิจารณาข้อมูลนี้ใช่หรือไม่?"");")

    End Sub
    Private Sub LoadActionData()
        dt = ctlL.LegalAction_GetByUID(Request("id"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdActionUID.Value = String.Concat(.Item("UID"))
                txtActionDescription.Text = String.Concat(.Item("Descriptions"))
                optResult.SelectedValue = String.Concat(.Item("AsmResult"))

                If DBNull2Str(dt.Rows(0)("filePath")) <> "" Then
                    'hlnkDoc.Text = DBNull2Str(dt.Rows(0)("filePath"))
                    hlnkDoc.NavigateUrl = DocumentAction & "/" & DBNull2Str(dt.Rows(0)("filePath"))
                    hlnkDoc.Visible = True
                End If

                cmdSend.Enabled = True

                LoadLegalData(.Item("LegalUID"))
                LoadPracticeDetail(.Item("PracticeUID"))
            End With
        Else
        End If
    End Sub
    Private Sub LoadLegalData(LegalUID As Integer)
        dt = ctlL.Legal_GetDetail(LegalUID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdLegalUID.Value = String.Concat(.Item("UID"))
                lblCode.Text = String.Concat(.Item("Code"))
                lblName.Text = String.Concat(.Item("LegalName"))
                lblLawMaster.Text = String.Concat(.Item("LawMasterName"))
                'lblOrganize.Text = String.Concat(.Item("OrganizeName"))
                lblArea.Text = String.Concat(.Item("AreaName"))
                lblType.Text = String.Concat(.Item("LawTypeName"))

                lblIssueDate.Text = DisplayShortDateTH(String.Concat(.Item("IssueDate")))
                lblStartDate.Text = DisplayShortDateTH(String.Concat(.Item("StartDate")))
                'lblRegisDate.Text = DisplayShortDateTH(String.Concat(.Item("RegisDate")))

                'lblYear.Text = String.Concat(.Item("LegalYear"))
                lblMinistry.Text = String.Concat(.Item("MinistryName"))

            End With
        Else
        End If
    End Sub
    Private Sub LoadPracticeDetail(ByVal pID As String)
        dt = ctlL.Practice_GetByUID(pID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdPracticeUID.Value = DBNull2Str(dt.Rows(0)("UID"))
                'lblPracticeCode.Text = DBNull2Str(dt.Rows(0)("Code"))
                lblPracticeDescription.Text = DBNull2Str(dt.Rows(0)("Descriptions"))
                lblRecurence.Text = String.Concat(dt.Rows(0)("RecurrencText"))
                'If DBNull2Str(dt.Rows(0)("Duedate")) <> "" Then
                '    lblDuedate.Text = DisplayShortDateTH(dt.Rows(0)("Duedate"))
                'End If
            End With
        End If
        dt = Nothing
    End Sub

    'Private Sub LoadLegalImage(LegalUID As Integer)
    '    Dim dtPic As New DataTable
    '    Dim str() As String
    '    dtPic = ctlL.LegalImage_Get(LegalUID)
    '    If dtPic.Rows.Count > 0 Then
    '        For i = 0 To dtPic.Rows.Count - 1

    '            If String.Concat(dtPic.Rows(i)("ImagePath")) <> "" Then
    '                str = Split(dtPic.Rows(i)("ImagePath"), ".")

    '                Select Case Right(str(0), 1)
    '                    Case "1"
    '                        Request.Cookies("iLaw")("Legalimg1") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                        Request.Cookies("iLaw")("picname1") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                    Case "2"
    '                        Request.Cookies("iLaw")("Legalimg2") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                        Request.Cookies("iLaw")("picname2") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                    Case "3"
    '                        Request.Cookies("iLaw")("Legalimg3") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                        Request.Cookies("iLaw")("picname3") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
    '                End Select
    '            End If
    '        Next
    '    End If

    'End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtActionDescription.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ระบุผลการปฏิบัติก่อน');", True)
            Exit Sub
        End If
        If optResult.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ระบุผลการประเมินความสอดคล้อง');", True)
            Exit Sub
        End If
        'If hlnkDoc.Text = "" Then
        '    If FileUpload1.PostedFile.FileName = "" Then
        '        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้เลือกไฟล์สำหรับอัพโหลด');", True)
        '        Exit Sub
        '    End If
        'End If

        Dim sFilePath As String
        sFilePath = ""

        If FileUpload1.HasFile Then
            sFilePath = lblCode.Text & "-" & hdPracticeUID.Value & "-" & Request.Cookies("iLaw")("LoginCompanyUID") & "-" & Request.Cookies("iLaw")("userid") & "-" & Request.Cookies("iLaw2")("PeriodID") & Path.GetExtension(FileUpload1.PostedFile.FileName)

            UploadFile(FileUpload1, sFilePath)
        End If

        ctlL.LegalAction_Save(StrNull2Long(hdActionUID.Value), StrNull2Zero(hdLegalUID.Value), StrNull2Zero(hdPracticeUID.Value), StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iLaw")("userid")), StrNull2Zero(Request.Cookies("iLaw2")("PeriodID")), txtActionDescription.Text, optResult.SelectedValue, sFilePath, Request.Cookies("iLaw")("userid"))

        'ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Legal", "บันทึก/แก้ไข Legal :{LegalUID=" & hdLegalUID.Value & "}{LegalCode=" & lblCode.Text & "}", "")


        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        cmdSend.Enabled = True

    End Sub

    'Private Sub RenamePictureName(fname As String, pname As String)

    '    If pname <> "" Then
    '        Dim Path As String = Server.MapPath(UploadDirectory)
    '        Dim Fromfile As String = Path + pname
    '        Dim Tofile As String = Path + fname


    '        Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & PictureLegal & "/" & fname))
    '        If fname <> pname Then
    '            If objfile.Exists Then
    '                objfile.Delete()
    '            End If

    '            File.Move(Fromfile, Tofile)
    '        End If
    '    End If
    'End Sub
    Sub UploadFile(ByVal Fileupload As Object, sName As String)
        'Dim FileFullName As String = Fileupload.PostedFile.FileName
        'Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As FileInfo = New FileInfo(Server.MapPath(DocumentAction & "/" & sName))
        If objfile.Exists Then
            objfile.Delete()
        End If

        'If FileNameInfo <> "" Then
        Fileupload.PostedFile.SaveAs(Server.MapPath(DocumentAction & "/" & sName))
        'End If

        'objfile = Nothing
    End Sub

    Protected Function SavePostedFile(ByVal uploadedFile As UploadedFile, ByVal UploadPath As String, ByVal UploadFileName As String) As String
        If (Not uploadedFile.IsValid) Then
            Return String.Empty
        End If

        Dim fileName As String = Path.ChangeExtension(UploadFileName, ".jpg")

        Dim fullFileName As String = CombinePath(UploadPath, fileName)
        Using original As Image = Image.FromStream(uploadedFile.FileContent)
            Using thumbnail As Image = New ImageThumbnailCreator(original).CreateImageThumbnail(New Size(350, 350))
                ImageUtils.SaveToJpeg(CType(thumbnail, Bitmap), fullFileName)
            End Using
        End Using
        UploadingUtils.RemoveFileWithDelay(fileName, fullFileName, 5)
        Return fileName
    End Function
    Protected Function CombinePath(ByVal UploadPath As String, ByVal fileName As String) As String
        Return Path.Combine(Server.MapPath(UploadPath), fileName)
    End Function

    Protected Sub cmdBack_Click(sender As Object, e As EventArgs) Handles cmdBack.Click
        Response.Redirect("LawReleaseList?m=l&s=rel&lid=" & hdLegalUID.Value)
    End Sub

    Protected Sub cmdSend_Click(sender As Object, e As EventArgs) Handles cmdSend.Click

        ctlL.LegalAction_UpdateActionStatus(StrNull2Int(hdActionUID.Value), "", ACTION_WAIT, "", StrNull2Zero(Request.Cookies("iLaw")("userid")))

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ส่ง Approver เรียบร้อย');", True)
    End Sub
End Class