﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class LawRelease
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LawController
    Dim ctlM As New MasterController
    Dim ctlU As New UserController
    'Private Const UploadDirectory As String = "~/imgLegal/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then

            'Request.Cookies("iLaw")("Legalimg1") = ""
            'Request.Cookies("iLaw")("picname1") = ""
            'Request.Cookies("iLaw")("Legalimg2") = ""
            'Request.Cookies("iLaw")("picname2") = ""
            'Request.Cookies("iLaw")("Legalimg3") = ""
            'Request.Cookies("iLaw")("picname3") = ""
            grdPractice.PageIndex = 0
            LoadAnalyst()
            LoadLegalData()
        End If


    End Sub

    Private Sub LoadAnalyst()
        dt = ctlU.Analyst_GetActive(Request.Cookies("iLaw")("LoginCompanyUID"))
        'ddlUser.DataTextField = "Name"
        'ddlUser.DataValueField = "UserID"
        'ddlUser.DataBind()
        With grdUser
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With
    End Sub

    Private Sub LoadLegalData()
        dt = ctlL.Legal_GetByUID(Request("lid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdLegalUID.Value = String.Concat(.Item("UID"))
                lblLegalName.Text = String.Concat(.Item("LegalName"))
                lblLegalCode.Text = String.Concat(.Item("Code"))
                LoadPracticeToGrid()
                LoadLegalRelease()
            End With
            cmdSave.Visible = True
        Else
            cmdSave.Visible = False
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        LawReleaseToUser()
    End Sub
    Private Sub LoadPracticeToGrid()
        dt = ctlL.Practice_Get(StrNull2Zero(hdLegalUID.Value))
        With grdPractice
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                .Rows(i).Cells(5).Text = Replace(Replace(.Rows(i).Cells(5).Text, "[", "<div class='badge badge-warning'>"), "]", "</div>")
            Next
        End With
    End Sub

    Private Sub LawReleaseToUser()

        Dim LawPracticeID, UserID As Integer
        Dim LawPracticeCode As String

        'RYear = StrNull2Zero(ddlYear.SelectedValue)

        'strSubj = Split(ddlCourse.SelectedItem.Text, " : ")
        'gSubjCode = strSubj(0)

        'sSubjCode = ddlCourse.SelectedValue
        'UserID = DBNull2Zero(grdUser.DataKeys(dIndex).Value)

        For i = 0 To grdPractice.Rows.Count - 1
            With grdPractice
                Dim chkS As CheckBox = grdPractice.Rows(i).Cells(0).FindControl("chkLaw")
                If chkS.Checked Then
                    LawPracticeCode = grdPractice.Rows(i).Cells(2).Text
                    LawPracticeID = DBNull2Zero(grdPractice.DataKeys(i).Value)

                    For n = 0 To grdUser.Rows.Count - 1
                        Dim chkU As CheckBox = grdUser.Rows(n).Cells(0).FindControl("chkUser")
                        If chkU.Checked Then
                            UserID = DBNull2Zero(grdUser.DataKeys(n).Value)
                            ctlL.LawRelease_Save(StrNull2Zero(hdLegalUID.Value), LawPracticeID, StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), UserID, Request.Cookies("iLaw2")("PeriodID"), "A", Request.Cookies("iLaw")("UserID"))

                            'acc.User_GenLogfile(Request.Cookies("Username").Value, ACTTYPE_ADD, "Assessment", "คัดเลือกนิสิตด้วยวิธีกำหนดเอง:" & gSubjCode & ">>" & grdUser.DataKeys(dIndex).Value, "")

                        End If
                    Next
                End If

            End With
        Next

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        grdPractice.PageIndex = 0
        LoadPracticeToGrid()
        LoadLegalRelease()

    End Sub
    Private Sub LoadLegalRelease()
        dt = ctlL.LegalRelease_GetByLegal(Request.Cookies("iLaw")("LoginCompanyUID"), StrNull2Zero(hdLegalUID.Value))
        With grdRelase
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                .Rows(i).Cells(2).Text = Replace(Replace(.Rows(i).Cells(2).Text, "[", "<div class='badge badge-warning'>"), "]", "</div>")
            Next
        End With
    End Sub
    Protected Sub grdRelase_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdRelase.RowCommand
        If TypeOf e.CommandSource Is WebControls.Button Then
            Dim ButtonPressed As WebControls.Button = e.CommandSource
            Select Case ButtonPressed.ID
                Case "cmdDel"
                    If ctlL.LawRelease_Delete(e.CommandArgument) Then
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบเรียบร้อย');", True)
                        grdPractice.PageIndex = 0
                        LoadPracticeToGrid()
                        LoadLegalRelease()
                    End If
            End Select
        End If
    End Sub

    Private Sub grdRelase_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdRelase.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบรายการ Release นี้ใช่หรือไม่?"");"
            Dim imgD As Button = e.Row.Cells(2).FindControl("cmdDel")
            imgD.Attributes.Add("onClick", scriptString)
        End If
    End Sub

    Private Sub grdPractice_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdPractice.PageIndexChanging
        grdPractice.PageIndex = e.NewPageIndex
        LoadPracticeToGrid()
    End Sub
End Class