﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class LawEssence
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LawController
    Dim ctlM As New MasterController
    Dim ctlU As New UserController
    Private Const UploadDirectory As String = "~/imgLegal/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then

            cmdApprove.Visible = False
            cmdDelete.Visible = False

            'Request.Cookies("iLaw")("Legalimg")="blankimage.png"
            Request.Cookies("iLaw")("Legalimg1") = ""
            Request.Cookies("iLaw")("picname1") = ""
            Request.Cookies("iLaw")("Legalimg2") = ""
            Request.Cookies("iLaw")("picname2") = ""
            Request.Cookies("iLaw")("Legalimg3") = ""
            Request.Cookies("iLaw")("picname3") = ""
            LoadLawMaster()
            LoadOrganize()
            LoadArea()
            LoadCategory()
            LoadKeyword()
            LoadAnalyst()
            LoadBusinessType()
            LoadMinistry()

            If Request("tid") Is Nothing Then
                txtCode.Text = ctlM.RunningNumber_New(CODE_LEGAL)
            Else
                LoadLegalData()

            End If

            'If Request.Cookies("iLaw")("ROLE_ADM") = False And Request.Cookies("iLaw")("ROLE_SPA") = False Then
            '    ddlOrganize.Enabled = False
            'Else
            '    ddlOrganize.Enabled = True
            'End If

        End If

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")

    End Sub
    Private Sub LoadLawMaster()
        ddlLawMaster.DataSource = ctlL.LawMaster_GetActive()
        ddlLawMaster.DataTextField = "Name"
        ddlLawMaster.DataValueField = "UID"
        ddlLawMaster.DataBind()
    End Sub
    Private Sub LoadOrganize()
        ddlOrganize.DataSource = ctlM.Organize_GetActive()
        ddlOrganize.DataTextField = "Name"
        ddlOrganize.DataValueField = "UID"
        ddlOrganize.DataBind()
    End Sub
    Private Sub LoadArea()
        ddlArea.DataSource = ctlM.Province_Get()
        ddlArea.DataTextField = "ProvinceName"
        ddlArea.DataValueField = "ProvinceID"
        ddlArea.DataBind()
    End Sub

    Private Sub LoadCategory()
        chkCategory.DataSource = ctlM.Category_GetActive()
        chkCategory.DataTextField = "Name"
        chkCategory.DataValueField = "UID"
        chkCategory.DataBind()
    End Sub
    Private Sub LoadKeyword()
        chkKeyword.DataSource = ctlM.Keyword_Get()
        chkKeyword.DataTextField = "Name"
        chkKeyword.DataValueField = "UID"
        chkKeyword.DataBind()
    End Sub

    Private Sub LoadBusinessType()
        ddlBusinessType.DataSource = ctlM.BusinessType_Get()
        ddlBusinessType.DataTextField = "Name"
        ddlBusinessType.DataValueField = "UID"
        ddlBusinessType.DataBind()
    End Sub
    Private Sub LoadMinistry()
        ddlMinistry.DataSource = ctlM.Ministry_Get()
        ddlMinistry.DataTextField = "Name"
        ddlMinistry.DataValueField = "UID"
        ddlMinistry.DataBind()
    End Sub

    Private Sub LoadAnalyst()
        ddlUser.DataSource = ctlU.Analyst_GetActive()
        ddlUser.DataTextField = "Name"
        ddlUser.DataValueField = "UserID"
        ddlUser.DataBind()
    End Sub

    Private Sub LoadLegalData()
        dt = ctlL.Legal_GetByUID(Request("tid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdLegalUID.Value = String.Concat(.Item("UID"))
                txtCode.Text = String.Concat(.Item("Code"))
                txtName.Text = String.Concat(.Item("LegalName"))
                ddlLawMaster.SelectedValue = String.Concat(.Item("LawMasterUID"))
                ddlOrganize.SelectedValue = String.Concat(.Item("OrganizeUID"))
                ddlArea.SelectedValue = String.Concat(.Item("AreaUID"))
                ddlType.SelectedValue = String.Concat(.Item("TypeUID"))

                txtDescription.Text = String.Concat(.Item("Descriptions"))
                optPhase.SelectedValue = String.Concat(.Item("PhaseUID"))

                txtIssueDate.Text = DisplayShortDateTH(String.Concat(.Item("IssueDate")))
                txtStartDate.Text = DisplayShortDateTH(String.Concat(.Item("StartDate")))
                txtRegisDate.Text = DisplayShortDateTH(String.Concat(.Item("RegisDate")))
                ddlUser.SelectedValue = String.Concat(.Item("AnalystUID"))

                txtYear.Text = String.Concat(.Item("LegalYear"))
                ddlBusinessType.SelectedValue = String.Concat(.Item("BusinessTypeUID"))
                ddlMinistry.SelectedValue = String.Concat(.Item("MinistryUID"))

                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(.Item("StatusFlag")))

                hlnkDoc.Text = DBNull2Str(dt.Rows(0)("filePath"))
                hlnkDoc.NavigateUrl = "../" & DocumentLegal & "/" & DBNull2Str(dt.Rows(0)("filePath"))
                hlnkDoc.Visible = True

                'If String.Concat(.Item("Keyword")) <> "" Then
                '    Dim sKey() As String
                '    sKey = Split(String.Concat(.Item("Keyword")), "|")
                '    For i = 0 To chkKeyword.Items.Count - 1
                '        For n = 0 To sKey.Length - 1
                '            If chkKeyword.Items(i).Text = sKey(n) Then
                '                chkKeyword.Items(i).Selected = True
                '            End If
                '        Next
                '    Next

                'End If
                'LoadLegalImage(DBNull2Zero(.Item("UID")))
                LoadLegalCategoryDetail(String.Concat(.Item("UID")))
                LoadLegalKeyword(String.Concat(.Item("UID")))

                If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_ADM")) = True Then
                    cmdSave.Visible = True
                    cmdDelete.Visible = True
                Else
                    cmdSave.Visible = False
                    cmdDelete.Visible = False
                End If
                If ctlU.User_IsApprover(Request.Cookies("iLaw")("UserID")) = True Then
                    cmdApprove.Visible = True
                Else
                    cmdApprove.Visible = False
                End If
            End With
        Else
        End If
    End Sub

    Private Sub LoadLegalCategoryDetail(LUID As Integer)
        Dim dtC As New DataTable
        dtC = ctlL.LegalCategoryDetail_GetByLegalUID(LUID)
        If dtC.Rows.Count > 0 Then
            chkCategory.ClearSelection()
            For i = 0 To chkCategory.Items.Count - 1
                For n = 0 To dtC.Rows.Count - 1
                    If chkCategory.Items(i).Value = dtC.Rows(n)("CategoryUID") Then
                        chkCategory.Items(i).Selected = True
                    End If
                Next
            Next
        End If
    End Sub
    Private Sub LoadLegalKeyword(LUID As Integer)
        Dim dtC As New DataTable
        dtC = ctlL.LegalKeyword_GetByLegalUID(LUID)
        If dtC.Rows.Count > 0 Then
            chkKeyword.ClearSelection()
            For i = 0 To chkKeyword.Items.Count - 1
                For n = 0 To dtC.Rows.Count - 1
                    If chkKeyword.Items(i).Value = dtC.Rows(n)("KeywordUID") Then
                        chkKeyword.Items(i).Selected = True
                    End If
                Next
            Next
        End If
    End Sub


    Private Sub LoadLegalImage(LegalUID As Integer)
        Dim dtPic As New DataTable
        Dim str() As String
        dtPic = ctlL.LegalImage_Get(LegalUID)
        If dtPic.Rows.Count > 0 Then
            For i = 0 To dtPic.Rows.Count - 1

                If String.Concat(dtPic.Rows(i)("ImagePath")) <> "" Then
                    str = Split(dtPic.Rows(i)("ImagePath"), ".")

                    Select Case Right(str(0), 1)
                        Case "1"
                            Request.Cookies("iLaw")("Legalimg1") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                            Request.Cookies("iLaw")("picname1") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                        Case "2"
                            Request.Cookies("iLaw")("Legalimg2") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                            Request.Cookies("iLaw")("picname2") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                        Case "3"
                            Request.Cookies("iLaw")("Legalimg3") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                            Request.Cookies("iLaw")("picname3") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                    End Select
                End If


            Next
        End If

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        'Dim BDate As String = ddlDay.SelectedValue
        'BDate = ddlYear.SelectedValue & ddlMonth.SelectedValue & BDate
        'If txtGPAX.Text <> "" Then
        '    If Not IsNumeric(txtGPAX.Text) Then
        '        DisplayMessage(Me.Page, "GPAX ไม่ถูกต้อง")
        '        Exit Sub
        '    End If
        'End If

        If txtName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ระบุ Legal name');", True)
            Exit Sub
        End If

        If hlnkDoc.Text = "" Then
            If FileUpload1.PostedFile.FileName = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้เลือกไฟล์สำหรับอัพโหลด');", True)
                Exit Sub
            End If
        End If

        Dim sKeyword As String = ""

        For i = 0 To chkKeyword.Items.Count - 1
            If chkKeyword.Items(i).Selected Then
                sKeyword = sKeyword + chkKeyword.Items(i).Text & "|"
            End If
        Next
        sKeyword = Left(sKeyword, sKeyword.Length - 1)

        ctlL.Legal_Save(StrNull2Zero(hdLegalUID.Value), txtCode.Text, txtName.Text, StrNull2Zero(ddlType.SelectedValue), StrNull2Zero(ddlLawMaster.SelectedValue), StrNull2Zero(ddlOrganize.SelectedValue), StrNull2Zero(ddlArea.SelectedValue), StrNull2Zero(optPhase.SelectedValue), txtIssueDate.Text, txtStartDate.Text, txtRegisDate.Text, StrNull2Zero(ddlUser.SelectedValue), StrNull2Zero(txtYear.Text), StrNull2Zero(ddlBusinessType.SelectedValue), StrNull2Zero(ddlMinistry.SelectedValue), sKeyword, txtDescription.Text, ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("iLaw")("userid"))

        Dim LegalUID As Integer

        If StrNull2Zero(hdLegalUID.Value) = 0 Then
            ctlM.RunningNumber_Update(CODE_LEGAL)
            LegalUID = ctlL.Legal_GetUID(txtCode.Text)
        Else
            LegalUID = hdLegalUID.Value
        End If

        ctlL.LegalCategoryDetail_Delete(LegalUID)

        For i = 0 To chkCategory.Items.Count - 1
            If chkCategory.Items(i).Selected Then
                ctlL.LegalCategoryDetail_Save(LegalUID, chkCategory.Items(i).Value, Request.Cookies("iLaw")("userid"))
            End If
        Next

        ctlL.LegalKeyword_Delete(LegalUID)

        For i = 0 To chkKeyword.Items.Count - 1
            If chkKeyword.Items(i).Selected Then
                ctlL.LegalKeyword_Save(LegalUID, chkKeyword.Items(i).Value, Request.Cookies("iLaw")("userid"))
            End If
        Next


        Dim f_Path As String 'f_Extension,f_Icon,, cate 
        'f_Icon = ""
        'f_Extension = ""
        f_Path = ""
        'cate = ""

        If FileUpload1.HasFile Then
            f_Path = txtCode.Text & Path.GetExtension(FileUpload1.PostedFile.FileName)
            'f_Extension = Path.GetExtension(FileUpload1.PostedFile.FileName)
            'Select Case f_Extension.ToLower()
            '    Case ".pdf"
            '        f_Icon = "pdf.png"
            '    Case ".doc", ".docx"
            '        f_Icon = "word.png"
            '    Case ".jpg", ".jpeg", ".png"
            '        f_Icon = "jpg.png"
            '    Case ".ppt", ".pptx"
            '        f_Icon = "ppt.png"
            '    Case Else
            '        f_Icon = ""
            'End Select

            ctlL.Legal_UpdateFile(LegalUID, f_Path, Request.Cookies("iLaw")("userid"))
            UploadFile(FileUpload1, f_Path)

        End If


        'Dim imgName As String

        'Dim LegalUID As Integer
        'LegalUID = ctlE.Legal_GetUIDByCode(Request.Cookies("iLaw")("LoginCompanyUID"), txtLegalNo.Text)

        'imgName = txtLegalNo.Text & Format(Request.Cookies("iLaw")("LoginCompanyUID"), "00#")

        'If Not Request.Cookies("iLaw")("picname1") Is Nothing And Request.Cookies("iLaw")("picname1") <> "" Then
        '    ctlE.Legal_UpdateFileName(LegalUID, txtLegalNo.Text, imgName & "01" & Path.GetExtension(Request.Cookies("iLaw")("picname1")))
        '    'UploadFile(FileUpload1, lblPersonID.Text & Path.GetExtension(FileUpload1.PostedFile.FileName))
        '    RenamePictureName(imgName & "01" & Path.GetExtension(Request.Cookies("iLaw")("picname1")), Request.Cookies("iLaw")("picname1"))
        'Else
        '    ctlE.LegalImage_Delete(LegalUID, txtLegalNo.Text, imgName & "01" & Path.GetExtension(Request.Cookies("iLaw")("picname1")))
        'End If

        'If Not Request.Cookies("iLaw")("picname2") Is Nothing And Request.Cookies("iLaw")("picname2") <> "" Then
        '    ctlE.Legal_UpdateFileName(LegalUID, txtLegalNo.Text, imgName & "02" & Path.GetExtension(Request.Cookies("iLaw")("picname2")))
        '    'UploadFile(FileUpload1, lblPersonID.Text & Path.GetExtension(FileUpload1.PostedFile.FileName))
        '    RenamePictureName(imgName & "02" & Path.GetExtension(Request.Cookies("iLaw")("picname2")), Request.Cookies("iLaw")("picname2"))
        'Else
        '    ctlE.LegalImage_Delete(LegalUID, txtLegalNo.Text, imgName & "02" & Path.GetExtension(Request.Cookies("iLaw")("picname2")))
        'End If

        'If Not Request.Cookies("iLaw")("picname3") Is Nothing And Request.Cookies("iLaw")("picname3") <> "" Then
        '    ctlE.Legal_UpdateFileName(LegalUID, txtLegalNo.Text, imgName & "03" & Path.GetExtension(Request.Cookies("iLaw")("picname3")))
        '    'UploadFile(FileUpload1, lblPersonID.Text & Path.GetExtension(FileUpload1.PostedFile.FileName))
        '    RenamePictureName(imgName & "03" & Path.GetExtension(Request.Cookies("iLaw")("picname3")), Request.Cookies("iLaw")("picname3"))
        'Else
        '    ctlE.LegalImage_Delete(LegalUID, txtLegalNo.Text, imgName & "03" & Path.GetExtension(Request.Cookies("iLaw")("picname3")))
        'End If


        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Legal", "บันทึก/แก้ไข Legal :{LegalUID=" & hdLegalUID.Value & "}{LegalCode=" & txtCode.Text & "}", "")

        If Not hdLegalUID.Value = Nothing Then
            cmdSave.Visible = True
            cmdDelete.Visible = True
        Else
            cmdSave.Visible = False
            cmdDelete.Visible = False
        End If
        If ctlU.User_IsApprover(Request.Cookies("iLaw")("UserID")) = True Then
            cmdApprove.Visible = True
        Else
            cmdApprove.Visible = False
        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        'Response.Redirect("Legal.aspx?ActionType=tsk")
    End Sub

    'Private Sub RenamePictureName(fname As String, pname As String)

    '    If pname <> "" Then
    '        Dim Path As String = Server.MapPath(UploadDirectory)
    '        Dim Fromfile As String = Path + pname
    '        Dim Tofile As String = Path + fname


    '        Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & PictureLegal & "/" & fname))
    '        If fname <> pname Then
    '            If objfile.Exists Then
    '                objfile.Delete()
    '            End If

    '            File.Move(Fromfile, Tofile)
    '        End If
    '    End If
    'End Sub
    Sub UploadFile(ByVal Fileupload As Object, sName As String)
        'Dim FileFullName As String = Fileupload.PostedFile.FileName
        'Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As FileInfo = New FileInfo(Server.MapPath(DocumentLegal & "/" & sName))
        If objfile.Exists Then
            objfile.Delete()
        End If


        'If FileNameInfo <> "" Then
        Fileupload.PostedFile.SaveAs(Server.MapPath(DocumentLegal & "/" & sName))
        'End If




        'objfile = Nothing
    End Sub


    'Protected Sub UploadControl_FileUploadComplete1(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)

    '    If chkFileExist(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname1"))) Then
    '        File.Delete(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname1")))
    '    End If

    '    e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory, Path.GetRandomFileName())
    '    Request.Cookies("iLaw")("picname1") = e.CallbackData
    'End Sub

    'Protected Sub UploadControl_FileUploadComplete2(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)

    '    If chkFileExist(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname2"))) Then
    '        File.Delete(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname2")))
    '    End If

    '    e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory, Path.GetRandomFileName())
    '    Request.Cookies("iLaw")("picname2") = e.CallbackData
    'End Sub

    'Protected Sub UploadControl_FileUploadComplete3(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)

    '    If chkFileExist(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname3"))) Then
    '        File.Delete(Server.MapPath(UploadDirectory + Request.Cookies("iLaw")("picname3")))
    '    End If

    '    e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory, Path.GetRandomFileName())
    '    Request.Cookies("iLaw")("picname3") = e.CallbackData
    'End Sub

    Protected Function SavePostedFile(ByVal uploadedFile As UploadedFile, ByVal UploadPath As String, ByVal UploadFileName As String) As String
        If (Not uploadedFile.IsValid) Then
            Return String.Empty
        End If

        Dim fileName As String = Path.ChangeExtension(UploadFileName, ".jpg")

        Dim fullFileName As String = CombinePath(UploadPath, fileName)
        Using original As Image = Image.FromStream(uploadedFile.FileContent)
            Using thumbnail As Image = New ImageThumbnailCreator(original).CreateImageThumbnail(New Size(350, 350))
                ImageUtils.SaveToJpeg(CType(thumbnail, Bitmap), fullFileName)
            End Using
        End Using
        UploadingUtils.RemoveFileWithDelay(fileName, fullFileName, 5)
        Return fileName
    End Function
    Protected Function CombinePath(ByVal UploadPath As String, ByVal fileName As String) As String
        Return Path.Combine(Server.MapPath(UploadPath), fileName)
    End Function

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        'ctlE.Legal_Delete(StrNull2Zero(hdLegalUID.Value))
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบ Legal เรียบร้อย');", True)
    End Sub

    Protected Sub cmdApprove_Click(sender As Object, e As EventArgs) Handles cmdApprove.Click
        ctlL.Legal_Approve(hdLegalUID.Value, ACTION_APPROVED, Request.Cookies("iLaw")("userid"))
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','กฎหมายถูกอนุมัติเรียบร้อย');", True)
    End Sub
End Class