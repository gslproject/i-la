﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LawImportPractice.aspx.vb" Inherits="iLA.LawImportPractice" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        #blur{
            position: fixed;
            width:100%;
            height:100%;
            left:0;
            top:0;
            background-color: black;
            opacity:.2;
            z-index:120;
        }
        #progress{
            height:30px;
            width:200px;
            z-index:999;
            background-color:#f4f4f4;
            padding-top:5px;
            position:absolute;
            top:50%;
            left:45%;
            border: solid 1px #808080;
            border-radius:5px;
            text-align:center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-ribbon icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>Practice Import (ข้อสาระสำคัญที่ต้องปฏิบัติ)
                            <div class="page-title-subheading">Import ข้อสาระสำคัญที่ต้องปฏิบัติ </div>
                        </div>
                    </div>
                </div>
       </div>
    <section class="content">
     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-upload"></i>
              <h3 class="box-title">Import Data from Excel file</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body">
                        <table border="0" class="table table-no-bordered no-border">
       <tr>
         <td>         
                      <asp:FileUpload ID="FileUploadFile" runat="server" /> 
         </td>
         </tr>     
  
                 <tr>
         <td>
  <asp:Button ID="cmdImport" runat="server" CssClass="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example" Text="import" Width="100px" />             
             </td>
                     </tr>
     </table>           
                                    
                
</div>
            <div class="box-footer clearfix">          
                 <asp:Label ID="lblResult" runat="server" CssClass="form-control alert alert-success"></asp:Label> 
                         <asp:Label ID="lblAlert" runat="server" Text="" CssClass="form-control alert alert-danger"></asp:Label>  
             
            </div>
          </div>      
        </section>   
</asp:Content>
