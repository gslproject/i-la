﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LegalList.aspx.vb" Inherits="iLA.LegalList" %>
<%@ Import Namespace="System.Data" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">    

    <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-ribbon icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>
                            <asp:Label ID="lblTitle" runat="server" Text="รายการกฎหมายที่ต้องประเมินความสอดคล้อง"></asp:Label>                             
                        </div>
                    </div>
                </div>
            </div>

    <section class="content"> 
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon fa fa-balance-scale icon-gradient bg-success">
            </i>Legal List
            <div class="btn-actions-pane-right">
                <% If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = True Then%>  
                <a href="LegalModify?m=new"  class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> ลงทะเบียนกฎหมายใหม่</a>    
                <% End If %>
            </div>
        </div>     
              <div class="card-body">   
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>              
                  <th class="text-center">รหัส</th> 
                  <th class="text-center">ชื่อกฎหมาย</th>
                  <th class="text-center">ประเภท</th>
                  <th class="text-center">ออกภายใต้</th>
                  <th class="text-center">หน่วยงานที่ควบคุม</th>
                  <th class="text-center">สถานะ</th>
                  <th class="sorting_asc_disabled sorting_desc_disabled text-center">ไฟล์กฎหมาย</th>                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLegal.Rows %>
                <tr>                 
                  <td class="text-center"><% =String.Concat(row("Code")) %></td>
                  <td>
                      <% If Request("s") = "asm" Then %>
                      <a  href="LawReleaseList?m=l&s=rel&lid=<% =String.Concat(row("UID")) %>" ><% =String.Concat(row("LegalName")) %> </a> 
                      <% Else %>
                         <a  href="LawApproveList?m=l&s=apv&lid=<% =String.Concat(row("UID")) %>" ><% =String.Concat(row("LegalName")) %> </a> 
                      <% End If %>

                  </td>
                  <td><% =String.Concat(row("LawTypeName")) %></td>
                     <td><% =String.Concat(row("LawMaster")) %></td>
                     <td><% =String.Concat(row("MinistryName")) %></td>
                  <td class="text-center"><% =IIf(String.Concat(row("StatusFlag")) = "A", "ประกาศใช้งาน", "") %> </td>    
                    <td class="text-center"> 
                          <% If String.Concat(row("FilePath")) <> "" %>
                      <a href="Documents/Legal/<% =String.Concat(row("FilePath"))  %>" target="_blank" class="font-icon-button"><i class="fa fa-file-pdf text-primary" aria-hidden="true"></i></a>
                        <% End If %>
                    </td>
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
 
  
    </section>
</asp:Content>
