﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LawReleaseList.aspx.vb"
    Inherits="iLA.LawReleaseList" %>
<%@ Import Namespace="System.Data" %> 
        <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
   
        </asp:Content>
        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>สาระสำคัญข้อปฏิบัติ 
                            <div class="page-title-subheading">ประเมินความสอดคล้องของกฎหมาย</div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <section class="col-lg-12">                          
             <div class="app-page-header">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">                      
                        <div><asp:label ID="lblLegalName" runat="server"></asp:label><asp:HiddenField ID="hdLegalUID" runat="server" />
                            <div class="page-title-subheading">รหัสกฎหมาย <asp:label ID="lblLegalCode" runat="server"></asp:label></div>
                        </div>
                    </div>
                </div>
            </div>
     </section>
         
                    <section class="col-lg-12">
                        <div class="main-card mb-3 card">
                            <div class="card-header">ข้อสาระสำคัญที่ต้องปฏิบัติ</div>
                            <div class="card-body">
 <div class="table-responsive">                            
              <table id="tbdata" class="table table-hover">
                <thead>
                <tr>              
                  <th class="text-center">รหัส</th> 
                  <th class="text-center">ข้อสาระสำคัญ (ข้อปฏิบัติ Practice)</th>
                  <th class="text-center">ความถี่</th>
                  <th class="text-center">หน่วยงานที่รับผิดชอบ</th>
                  <th class="text-center">ผลการปฏิบัติ</th>
                  <th class="text-center">เอกสาร</th>
                  <th class="text-center">ผลการประเมินความสอดคล้อง</th>   
                       <th class="text-center">การอนุมัติ</th>  
                     <th class="text-center"></th>  
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLg.Rows %>
                <tr>                 
                  <td class="text-center"><% =String.Concat(row("Code")) %></td>
                  <td><% =String.Concat(row("PracticeDescriptions")) %></td>
                  <td><% =String.Concat(row("RecurrenceText")) %></td>
                     <td><% =String.Concat(row("DeptCode")) %></td>
                    <td><% =String.Concat(row("ActionDescriptions")) %></td>
                    <td class="text-center"> 
                        <% If String.Concat(row("FilePath")) <> "" Then %>
                            <a href="Documents/Action/<% =String.Concat(row("FilePath"))  %>" target="_blank" class="font-icon-button"><i class="fa fa-file-pdf text-primary" aria-hidden="true"></i></a>
                          <% End If %>      
                                </td>
                  <td class="text-center">
                      <% =IIf(String.Concat(row("AsmResult")) = "Y", "<img src='images/statusicon_yes.png' height='30px' />", IIf(String.Concat(row("AsmResult")) = "N", "<img src='images/statusicon_no.png' height='30px' />", "")) %> </td>  

                      <td class="text-center"><% =IIf(String.Concat(row("ApproveStatus")) = "Y", "อนุมัติ", IIf(String.Concat(row("ApproveStatus")) = "N", "ไม่อนุมัติ : " & String.Concat(row("ActionRemark")), "")) %> </td>  

                     <td class="text-center">                      
                        <a href="LegalAssessment?id=<% =String.Concat(row("UID")) %>" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></a>
                        
                    </td>
                    
                </tr>
            <%  Next %>
                </tbody>               
              </table>  
   </div>                                                           </div>
                            <div class="box-footer clearfix">
                            </div>
                        </div>                      
                                         
                    </section>
                </div>

               

            </section>
        </asp:Content>