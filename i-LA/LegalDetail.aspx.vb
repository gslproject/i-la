﻿Public Class LegalDetail
    Inherits System.Web.UI.Page
    Dim ctlL As New LawController
    Public dtLegal As New DataTable
    Public dtPt As New DataTable
    Dim dt As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        dtLegal = ctlL.Legal_GetByUID(StrNull2Zero(Request("id")))

        lblLegalName.Text = dtLegal.Rows(0)("LegalName")
        LoadPracticeToGrid()


        'cmdReject.Attributes.Add("onClick", "javascript:return confirm(""ต้องการ Reject กฎหมายฉบับบนี้ ใช่หรือไม่?"");")

    End Sub
    Private Sub LoadPracticeToGrid()
        dtPt = ctlL.Practice_Get(StrNull2Zero(Request("id")))
        'With grdPractice
        '    .Visible = True
        '    .DataSource = dt
        '    .DataBind()
        'End With
    End Sub

    Private Sub cmdReject_Click(sender As Object, e As EventArgs) Handles cmdReject.Click
        Response.Redirect("LawReject.aspx?lid=" & Request("id"))
    End Sub
End Class