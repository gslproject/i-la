﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LawReject.aspx.vb"
    Inherits="iLA.LawReject" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
   
        </asp:Content>
        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>Legal Reject
                            <div class="page-title-subheading">Reject กฎหมายที่ไม่เกี่ยวข้อง</div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <section class="col-lg-12">                          
             <div class="app-page-header">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">                      
                        <div><asp:label ID="lblLegalName" runat="server"></asp:label><asp:HiddenField ID="hdLegalUID" runat="server" />
                            <div class="page-title-subheading">รหัสกฎหมาย <asp:label ID="lblLegalCode" runat="server"></asp:label></div>
                        </div>
                    </div>
                </div>
            </div>
     </section>
         
                    <section class="col-lg-12">
                        <div class="main-card mb-3 card">
                            <div class="card-header">ระบุเหตุผล</div>
                            <div class="card-body">
                                <div class="row">
                                     <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:TextBox ID="txtReason" runat="server" CssClass="form-control" TextMode="MultiLine"  Height="100"> </asp:TextBox>
                               </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                            </div>
                        </div>                    
                 
                    </section>
                </div>

                <div class="row justify-content-center">               
                    <div class="col-md-12 text-center">
                   <asp:Button ID="cmdSave" CssClass="btn btn-danger" runat="server" Text="Confirm" Width="120px" />
                         <asp:Button ID="cmdCancel" CssClass="btn btn-secondary" runat="server" Text="Cancel" Width="120px" />
                          </div>
                </div>

            </section>
        </asp:Content>