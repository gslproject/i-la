﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="Site.Master" CodeBehind="ReportLegalAssessment.aspx.vb" Inherits="iLA.ReportLegalAssessment" %>
<%@ Import Namespace="System.Data" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">    
    <link href="assets/styles.css" rel="stylesheet" />
    <style>      
        #chart {
      padding: 0;
      max-width: 380px;
      margin: 20px auto;
    }
        #chart2 {
       max-width: 650px;
      margin: 0px auto;
    }
      
    </style>

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>

    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      // Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
      // Based on https://gist.github.com/blixt/f17b47c62508be59987b
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-graph icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>รายงานผลการประเมินความสอดคล้อง
                            <div class="page-title-subheading"></div>
                        </div>
                    </div>
                </div>
            </div>

    <section class="content">
       <div class="mb-3 card">
            <div class="card-header"><i class="header-icon lnr-chart-bars icon-gradient bg-success">
                </i>
                รายงานผลการประเมินความสอดคล้อง
                <div class="btn-actions-pane-right">
                
                </div>
            </div> 
            <div class="card-body">
               <ul class="tabs-animated-shadow nav-justified tabs-animated nav">
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link active" id="tab-c1-0" data-toggle="tab" href="#tab-animated1-0">
                                                            <span class="nav-text">กราฟ</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link" id="tab-c1-1" data-toggle="tab" href="#tab-animated1-1">
                                                            <span class="nav-text">ตาราง</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#tab-animated1-2">
                                                            <span class="nav-text">รายการกฎหมายที่ไม่สอดคล้อง</span>
                                                        </a>
                                                    </li> 
                                                </ul>
                                                <div class="tab-content">
  <div class="tab-pane active" id="tab-animated1-0" role="tabpanel">

       <div class="row">  
          <div class="col-md-6">
              <h6 class="card-title text-blue">ผลการประเมินความสอดคล้องภาพรวมของบริษัท</h6>
              <div id="chart"></div>
          </div>
          
          <div class="col-md-6">
              <h6 class="card-title text-blue">ผลการประเมินความสอดคล้องแยกตามหน่วยงาน</h6>
              <div id="chart2"></div>
           </div>
           </div>
     
</div>                                                  
<div class="tab-pane" id="tab-animated1-1" role="tabpanel">   
    <div class="table-responsive">
           <table id="tbdata" width="100%" class="table table-bordered table-striped">
                <thead>
                <tr>
                 
                  <th class="text-center align-middle" rowspan="2">หน่วยงาน</th>
                  <th class="text-center align-middle"  rowspan="2">จำนวนข้อปฏิบัติที่เกี่ยวข้อง</th>
                  <th class="text-center align-middle"  rowspan="2">จำนวนข้อที่ประเมินความสอดคล้องแล้ว</th>
                     <th class="text-center align-middle"  colspan="2">ผลการประเมินความสอดคล้อง</th>
                 
                      <th class="text-center align-middle"  rowspan="2">% ความสอดคล้อง</th>                            
                </tr>
                     <tr>
                  
                     <th class="text-center align-middle" >สอดคล้อง</th>                
                  <th class="text-center align-middle" >ไม่สอดคล้อง</th>                            
                </tr>
                </thead>
                <tbody>
      <% For Each row As DataRow In dtLegal.Rows %>
         <tr>
          <td class="text-left align-middle" ><% =String.Concat(row("DepartmentName")) %></td>
          <td class="text-center align-middle" ><a href="LegaListStatus?m=rpt&s=1&st=all" target="_blank"> <% =String.Concat(row("PracticeCount")) %>  </a>  </td>
          <td class="text-center align-middle" ><a href="LegaListStatus?m=rpt&s=1&st=asm" target="_blank"> <% =String.Concat(row("AsmCount")) %> </a> </td>
          <td class="text-center align-middle" ><a href="LegaListStatus?m=rpt&s=1&st=yes" target="_blank"> <% =String.Concat(row("YCount")) %>  </a> </td>     
          <td class="text-center align-middle" ><a href="LegaListStatus?m=rpt&s=1&st=no" target="_blank">  <% =String.Concat(row("NCount")) %>   </a> </td>
          <td class="text-center align-middle" ><% =Math.Round((row("YCount") * 100 / row("PracticeCount")), 2) %> %</td> 
         </tr>
     <%  Next %>
                </tbody>               
              </table>                                                             
   </div>  
</div>
 <div class="tab-pane" id="tab-animated1-2" role="tabpanel">                                                      
         <table id="tbdata2" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled">รหัสกฎหมาย</th>    
                 <th class="text-center">ชื่อกฎหมาย</th> 
                 <th class="text-center">ประเภทกฎหมาย</th> 
                 <th class="text-center">รหัสข้อสาระสำคัญ</th> 
                 <th class="text-center">ข้อสาระสำคัญ</th> 
                 <th class="text-center">ผู้รับผิดชอบ</th> 
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtPt.Rows %>
                <tr>
                  <td width="50"><% =String.Concat(row("LegalCode")) %></td>
                    <td><% =String.Concat(row("LegalName")) %></td>
                  <td><% =String.Concat(row("LawTypeName")) %></td>   
                    <td><% =String.Concat(row("PracticeCode")) %></td>
                  <td><% =String.Concat(row("Descriptions")) %></td>                                
                  <td class="text-center"><% =String.Concat(row("DepartmentCode")) %></td>
                </tr>
            <%  Next %>
                </tbody>               
              </table>     

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

  
    </section>

    <script>
      
        var options = {
          series:  <% =datachart1 %>,
          chart: {
          width: 480,
          type: 'pie',
        },
        labels: ['สอดคล้อง', 'ไม่สอดคล้อง', 'อยู่ระหว่างการประเมิน'],
        responsive: [{
          breakpoint: 380,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
  </script>
   <script>
       var options2 = {
           series: [{
               name: "สอดคล้อง",
               data: [<% =databar1 %>]
           }, {
               name: "ไม่สอดคล้อง",
               data: [<% =databar2 %>]
        }],
          chart: {
          type: 'bar',
          height: 430
        },
        plotOptions: {
            bar: {
                borderRadius: 10,    
                 columnWidth: '45%',
            horizontal: true,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: -6,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: [<% =catebar %>],
        },
        };          

        var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);  
        chart2.render();
      
      
    </script>
</asp:Content>
