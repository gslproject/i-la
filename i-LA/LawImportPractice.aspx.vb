﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data
Public Class LawImportPractice
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlM As New MasterController
    Dim ctlC As New CompanyController
    Dim ctlE As New LawController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblResult.Visible = False
            lblAlert.Visible = False
        End If
    End Sub
    Private Sub cmdImport_Click(sender As Object, e As EventArgs) Handles cmdImport.Click
        If (Not FileUploadFile.HasFile) Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกไฟล์ Excel ก่อน');", True)
            Exit Sub
        End If

        Dim connectionString As String = ""
        Try

            lblResult.Visible = False

            Dim fileName As String = FileUploadFile.FileName
            Dim fileExtension As String = Path.GetExtension(FileUploadFile.PostedFile.FileName)
            Dim fileLocation As String = Server.MapPath("~/" + tmpUpload + "/" + fileName)

            Dim objfile As FileInfo = New FileInfo(fileLocation)
            If objfile.Exists Then
                objfile.Delete()
            End If

            FileUploadFile.SaveAs(Server.MapPath("~/" + tmpUpload + "/" + fileName))

            'Check whether file extension is xls or xslx

            If fileExtension = ".xls" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            ElseIf fileExtension = ".xlsx" Then
                If (Not FileUploadFile.HasFile) Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกไฟล์ Excel เวอร์ชั่น .xls เท่านั้น');", True)
                    Exit Sub
                End If
                lblAlert.Text = "ตรวจสอบเวอร์ชั่นไฟล์ Excel สามารถใช้ได้เฉพาะ .xls เท่านั้น"
                lblAlert.Visible = True
                Exit Sub
                'connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            End If

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            Dim LawCode As String = ""
            Dim PctCode As String = ""
            Dim k As Integer = 0

            'If VerifyData(dtExcelRecords) Then
            For i = 0 To dtExcelRecords.Rows.Count - 1

                With dtExcelRecords.Rows(i)
                    If .Item(0).ToString <> "" Then
                        If ctlE.Legal_CheckDuplicate(String.Concat(.Item(0))) = True Then
                            If ctlE.Practice_CheckDuplicate(String.Concat(.Item(0)), String.Concat(.Item(1))) Then
                                PctCode = PctCode & String.Concat(.Item(0)) & "=" & String.Concat(.Item(1)) & ","
                            Else
                                ctlE.Practice_SaveByImport(String.Concat(.Item(0)), String.Concat(.Item(1)), String.Concat(.Item(2)), String.Concat(.Item(3)), Request.Cookies("iLaw")("userid"))

                                k = k + 1
                            End If
                        Else
                            LawCode = LawCode & String.Concat(.Item(0)) & "," & vbCrLf
                        End If
                    End If
                End With
            Next

            Dim ctlU As New UserController
            ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_ADD, "Practice", "import Practice", "import Practice : " & k & " รายการ จากไฟล์ excel")

            lblResult.Text = "ข้อมูลทั้งหมด " & k & " รายการ ถูก import เรียบร้อย" & IIf(PctCode <> "", vbCrLf & " รหัส " & PctCode & " ไม่สามารถเพิ่มได้เนื่องจากมีในฐานข้อมูลแล้ว ", "")
            lblResult.Visible = True
            lblAlert.Visible = False
            dtExcelRecords = Nothing

            If objfile.Exists Then
                objfile.Delete()
            End If

            If LawCode <> "" Then
                lblAlert.Text = "ไม่พบกฎหมาย " & LawCode & " ในระบบ"
                lblAlert.Visible = True
            End If
            'Else
            '    lblAlert.Text = "Error! กรุณาตรวจสอบไฟล์ Excel แล้วลองใหม่อีกครั้ง"
            '    lblAlert.Visible = True
            'End If
            'UpdateProgress1.Visible = False
        Catch ex As Exception
            lblAlert.Text = "Error : " & ex.Message
            lblAlert.Visible = True
        End Try

        'UpdateProgress1.Visible = False
    End Sub
End Class