﻿Imports System.Net.Mail
Imports System.Web
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Net

Public Class CustomerManage
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New CompanyController
    Dim ctlM As New MasterController
    Dim ctlU As New UserController
    Dim enc As New CryptographyEngine
    Dim ReportFileName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Request.Cookies("iLaw")("ROLE_ADM") = False And Request.Cookies("iLaw")("ROLE_SPA") = False Then
            Response.Redirect("Error.aspx")
        End If

        If Not IsPostBack Then
            txtCompanyCode.Text = ""
            hdCompanyUID.Value = 0
            LoadCompanyData()
        End If

        'txtUserLimit.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtEmployeeQTY.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

    End Sub

    Private Sub LoadCompanyData()
        dt = ctlE.Company_GetByUID(Request("cid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Dim dr As DataRow = dt.Rows(0)
                hdCompanyUID.Value = String.Concat(dr("UID"))
                txtCompanyCode.Text = String.Concat(dr("CompanyCode"))
                txtNameTH.Text = String.Concat(dr("NameTH"))
                'txtNameEN.Text = String.Concat(dr("NameEN"))
                txtBranch.Text = String.Concat(dr("Branch"))
                txtTaxID.Text = String.Concat(dr("VATID"))
                lblAddress.Text = String.Concat(dr("CompanyAddress"))
                'txtEmail.Text = String.Concat(dr("Email"))
                txtFax.Text = String.Concat(dr("Fax"))
                txtTel.Text = String.Concat(dr("Telephone"))
                'txtWebsite.Text = String.Concat(dr("Website"))

                ddlStatus.SelectedValue = String.Concat(dr("StatusFlag"))
                If String.Concat(dr("StatusFlag")) = "A" Then
                    lblStatus.Text = "Active"
                    lblStatus.CssClass = "badge badge-success"
                Else
                    lblStatus.Text = "Inactive/Over due"
                    lblStatus.CssClass = "badge badge-danger"
                End If
                'txtUserLimit.Text = String.Concat(dr("MAXPERSON"))
                'txtEmployeeQTY.Text = String.Concat(dr("Employee"))

                txtAddressInvoice.Text = String.Concat(dr("AddressInvoice"))

                Try
                    Dim cName() As String
                    cName = Split(String.Concat(dr("ContactName")), " ")
                    txtFname.Text = cName(0)
                    txtLname.Text = cName(1)
                Catch ex As Exception

                End Try

                txtContactMail.Text = String.Concat(dr("ContactMail"))
                ddlPackage.SelectedValue = String.Concat(dr("PackageNo"))

                If String.Concat(.Item("StartDate")) <> "" Then
                    txtStartDate.Text = DisplayShortDateTH(String.Concat(.Item("StartDate")))
                End If

                If String.Concat(.Item("DueDate")) <> "" Then
                    txtDueDate.Text = DisplayShortDateTH(String.Concat(.Item("DueDate")))
                End If

                LoadAdminUser(hdCompanyUID.Value)


                dr = Nothing
            End With

        Else

        End If
    End Sub
    Private Sub LoadAdminUser(CompanyUID As Integer)
        Dim dtU As New DataTable
        dtU = ctlU.User_GetMainUserAdmin(CompanyUID)
        If dtU.Rows.Count > 0 Then
            txtUsername.Text = dtU.Rows(0)("Username")
            txtPassword.Text = dtU.Rows(0)("Passwords")
            txtFname.Text = dtU.Rows(0)("Fname")
            txtLname.Text = dtU.Rows(0)("Lname")
            cmdConfirm.Visible = True
        Else
            cmdConfirm.Visible = False
        End If
    End Sub
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If ctlU.User_CheckDuplicateCustomerAdmin(hdCompanyUID.Value, txtUsername.Text) Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ชื่อ Username มีในระบบแล้ว');", True)
            Exit Sub
        End If

        If txtDueDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุวันหมดอายุก่อน');", True)
            Exit Sub
        End If
        If txtFname.Text = "" Or txtLname.Text="" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อ-นามสกุลผู้ติดต่อ');", True)
            Exit Sub
        End If

        If txtUsername.Text = "" Or txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากำหนด Username และ Password ให้ครบถ้วนก่อน');", True)
            Exit Sub
        End If

        Dim CompanyUID As Integer
        CompanyUID = hdCompanyUID.Value

        ctlE.Customer_UpdatePackage(CompanyUID, StrNull2Zero(ddlPackage.SelectedValue), ConvertStrDate2DBDate(txtStartDate.Text), ConvertStrDate2DBDate(txtDueDate.Text), ddlStatus.SelectedValue, txtFname.Text & " " & txtLname.Text, txtContactMail.Text, Request.Cookies("iLaw")("UserID"))


        ctlU.UserMainAdmin_Add(txtUsername.Text, enc.EncryptString(txtPassword.Text, True), txtFname.Text, txtLname.Text, CompanyUID, txtTel.Text, txtContactMail.Text, "A", Request.Cookies("iLaw")("UserID"))

        ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_UPD, "Company", "บันทึก/แก้ไข ข้อมูล Package และ Invoice :{uid=" & CompanyUID & "}{code=" & txtCompanyCode.Text & "}", "สร้างข้อมูลลูกค้า")

        If StrNull2Zero(ddlPackage.SelectedValue) <> 1 Then
            ctlM.PeriodTime_Create(CompanyUID)
        End If

        GenPayin(hdCompanyUID.Value)

        cmdSendInvoice.Visible = True
        btnInvoice.Visible = True

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

    Private Sub btnCreateInvoice15_Click(sender As Object, e As EventArgs) Handles btnCreateInvoice15.ServerClick
        Dim InvNo, InvDate, ItemDesc As String

        dt = ctlE.Customer_GetExpireDate(hdCompanyUID.Value)
        InvDate = ConvertStrDate2DBDate(dt.Rows(0)("Expire15"))

        InvNo = Year(InvDate).ToString() + Month(InvDate).ToString("0#") + Day(InvDate).ToString("0#") + "_" + txtCompanyCode.Text  'ctlM.RunningNumber_New(CODE_INVOICE) 'Year + Month + CustomerID
        ItemDesc = "ค่าต่ออายุสมาชิก i-Law ระบบบริหารจัดการกฎหมาย Package " & ddlPackage.SelectedValue & " ระยะเวลา 1 ปี"

        GenInvoice(hdCompanyUID.Value, "2", InvNo, InvDate, ItemDesc)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','สร้างใบแจ้งค่าบริการล่วงหน้า 15 วัน เรียบร้อย');", True)
    End Sub
    Private Sub btnCreateInvoice30_Click(sender As Object, e As EventArgs) Handles btnCreateInvoice30.ServerClick
        Dim InvNo, InvDate, ItemDesc As String

        dt = ctlE.Customer_GetExpireDate(hdCompanyUID.Value)
        InvDate = ConvertStrDate2DBDate(dt.Rows(0)("Expire30"))

        InvNo = Year(InvDate).ToString() + Month(InvDate).ToString("0#") + Day(InvDate).ToString("0#") + "_" + txtCompanyCode.Text  'ctlM.RunningNumber_New(CODE_INVOICE) 'Year + Month + CustomerID
        ItemDesc = "ค่าต่ออายุสมาชิก i-Law ระบบบริหารจัดการกฎหมาย Package " & ddlPackage.SelectedValue & " ระยะเวลา 1 ปี"

        GenInvoice(hdCompanyUID.Value, "3", InvNo, InvDate, ItemDesc)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','สร้างใบแจ้งค่าบริการล่วงหน้า 30 วัน เรียบร้อย');", True)
    End Sub
    Private Sub btnCreateInvoice90_Click(sender As Object, e As EventArgs) Handles btnCreateInvoice90.ServerClick
        Dim InvNo, InvDate, ItemDesc As String

        dt = ctlE.Customer_GetExpireDate(hdCompanyUID.Value)
        InvDate = ConvertStrDate2DBDate(dt.Rows(0)("Expire90"))

        InvNo = Year(InvDate).ToString() + Month(InvDate).ToString("0#") + Day(InvDate).ToString("0#") + "_" + txtCompanyCode.Text  'ctlM.RunningNumber_New(CODE_INVOICE) 'Year + Month + CustomerID
        ItemDesc = "ค่าต่ออายุสมาชิก i-Law ระบบบริหารจัดการกฎหมาย Package " & ddlPackage.SelectedValue & " ระยะเวลา 1 ปี"

        GenInvoice(hdCompanyUID.Value, "4", InvNo, InvDate, ItemDesc)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','สร้างใบแจ้งค่าบริการล่วงหน้า 3 เดือน เรียบร้อย');", True)
    End Sub

    Private Sub cmdSendInvoice_Click(sender As Object, e As EventArgs) Handles cmdSendInvoice.Click
        If txtContactMail.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุอีเมล์ก่อน');", True)
            Exit Sub
        End If
        If txtFname.Text = "" Or txtLname.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อ-นามสกุลผู้ติดต่อ');", True)
            Exit Sub
        End If

        'If txtUsername.Text = "" Or txtPassword.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากำหนด Username และ Password ให้ครบถ้วนก่อน');", True)
        '    Exit Sub
        'End If
        Dim Username, Password As String
        dt = ctlU.User_GetMainUserAdmin(hdCompanyUID.Value)
        If dt.Rows.Count > 0 Then
            Username = String.Concat(dt.Rows(0)("Username"))
            Password = enc.DecryptString(String.Concat(dt.Rows(0)("Passwords")), True)
        End If


        Dim InvNo, InvDate, ItemDesc As String
        InvDate = ConvertStrDate2DBDate(ctlM.GET_DATE_SERVER())
        InvNo = Year(InvDate).ToString() + Month(InvDate).ToString("0#") + Day(InvDate).ToString("0#") + "_" + txtCompanyCode.Text  'ctlM.RunningNumber_New(CODE_INVOICE) 'Year + Month + CustomerID
        ItemDesc = "ค่าสมัครสมาชิก i-Law ระบบบริหารจัดการกฎหมาย Package " & ddlPackage.SelectedValue & " ระยะเวลา 1 ปี"

        GenInvoice(hdCompanyUID.Value, "1", InvNo, InvDate, ItemDesc)

        SendEmailInvoice(hdCompanyUID.Value, InvNo, txtFname.Text & " " & txtLname.Text, Username, Password, txtContactMail.Text)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ส่งอีเมล์แจ้งค่าบริการเรียบร้อย');", True)
    End Sub

    Private Sub GenPayin(CustomerID As Integer)
        LoadReport("payin", txtCompanyCode.Text, CustomerID)
        ctlE.Company_UpdatePayinFile(CustomerID, "Payin" & txtCompanyCode.Text & ".pdf")
    End Sub
    Private Sub GenInvoice(CustomerID As Integer, InvType As String, InvNo As String, InvDate As String, ItemDesc As String)

        'If InvNo = "Not Config Running" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้กำหนดรหัส Running Invoice');", True)
        '    Exit Sub
        'End If

        Dim dtPk As New DataTable
        Dim UnitPrice, Vat, Amount, NetTotal As Double
        dtPk = ctlE.Company_GetPackageDetail(CustomerID)
        NetTotal = dtPk.Rows(0)("Amount")
        'มูลค่า VAT = ราคารวม VAT x 7/107
        'ราคาก่อน VAT = ราคารวม VAT x 100/107
        UnitPrice = Math.Round(NetTotal * 100 / 107, 2)
        Vat = Math.Round(NetTotal * 7 / 107, 2)
        Amount = UnitPrice

        ctlE.Invoice_Add(InvNo, InvDate, CustomerID, InvType, ItemDesc, UnitPrice, Vat, Amount, NetTotal, Request.Cookies("iLaw")("userid"))

        'ctlM.RunningNumber_Update(CODE_INVOICE)

        LoadReport("inv", InvNo, CustomerID)
        'SendEmail(CustomerID, InvNo, txtFname.Text & " " & txtLname.Text, txtUsername.Text, txtPassword.Text, txtContactMail.Text)
    End Sub
    Private Sub LoadReport(DocumentType As String, DocumentNo As String, CompanyUID As Integer)

        'System.Threading.Thread.Sleep(1000)
        'UpdateProgress1.Visible = True

        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True

        Dim xParam As New List(Of ReportParameter)

        Dim filePath As String = ""
        Select Case DocumentType
            Case "payin"
                filePath = "Invoice\Payin\"
                ReportFileName = "Payin" & txtCompanyCode.Text
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("Payin")
                xParam.Add(New ReportParameter("CompanyUID", CompanyUID.ToString()))
            Case "inv"
                filePath = "Invoice\Invoice\"
                ReportFileName = "ใบแจ้งค่าบริการInvoice" & DocumentNo
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("Invoice")
                xParam.Add(New ReportParameter("InvoiceNo", DocumentNo))
            Case Else
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("")
                xParam.Add(New ReportParameter("UserID", Request.Cookies("iLaw")("userid").ToString()))
        End Select

        ReportViewer1.ServerReport.SetParameters(xParam)

        Dim warnings As Warning()
        Dim streamIds As String()
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim extension As String = String.Empty

        Dim result As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)

        Try
            Dim objfile As FileInfo = New FileInfo(Server.MapPath(filePath & ReportFileName & ".pdf"))
            If objfile.Exists Then
                objfile.Delete()
            End If

            Dim stream As FileStream = File.Create(Server.MapPath(filePath & ReportFileName & ".pdf"), result.Length)
            stream.Write(result, 0, result.Length)
            stream.Close()

        Catch e As Exception

        End Try
    End Sub

    Private Sub SendEmailInvoice(CompanyUID As Integer, DocumentNo As String, PersonName As String, Username As String, Password As String, sTo As String)
        Dim SenderDisplayName As String = "i-Law : Legal Management System"
        Dim MySubject As String = "เรื่อง การชำระค่าสมาชิกโปรแกรม i-Law : Legal Management System :" & txtNameTH.Text
        Dim MyMessageBody As String = ""

        MyMessageBody = " <font size='4'>
    
  <p> เรียน คุณ " & PersonName & "</p>
  <p> เรื่อง การชำระค่าสมาชิกโปรแกรม i-Law : Legal Management System </p>
  <p> บริษัทฯ ขอขอบพระคุณท่านเป็นอย่างสูงที่ได้ไว้วางใจในการเป็นสมาชิกโปรแกรม i-Law : Legal Management System ของเรา
        ซึ่งเป็นโปรแกรมการบริหารจัดการ (Legal Management) และตรวจประเมินความสอดคล้อง (Legal Compliance Audit)
        กฎหมายด้านความปลอดภัย อาชีวอนามัย พลังงานและสิ่งแวดล้อมอย่างเป็นระบบ
        ซึ่งรายละเอียดและสิทธิประโยชน์ของสมาชิกจะเป็นไปตาม Package ที่ท่านได้เลือกไว้ (รายละเอียดของ Package
        สามารถเข้าไปดูได้ตามลิงค์นี้ <a href='http://www.npc-legal.com'>http://www.npc-legal.com</a> ) และบริษัทฯ หวังเป็นอย่างยิ่งว่าโปรแกรมฯ นี้
        จะเป็นเครื่องมือหนึ่งที่ช่วยให้การจัดการด้านกฎหมายฯ และตรวจประเมินความสอดคล้องตามกฎหมายฯ
        ของท่านง่ายและสะดวกยิ่งขึ้น </p>

  <p>   เพื่อเป็นการเปิดการใช้งานโปรแกรมฯ ท่านสามารถนำใบชำระเงิน (Pay-in Slip)
        ที่ได้แนบมาตามอีเมลล์นี้ไปชำระค่าสมาชิกได้ตามช่องทางต่อไปนี้ <br />
        1.ชำระผ่านระบบธนาคาร <br />
        1.1 นำใบชำระเงิน (Pay-in Slip) ที่แนบมาพร้อมอีเมลล์ฉบับนี้ และนำไปชำระพร้อมเงินสดได้ที่ เคาน์เตอร์ธนาคารกรุงเทพ
        ทุกสาขา <br />
        1.2 โอนเงินเข้าบัญชีบริษัทฯ ชื่อบัญชี บริษัท เอ็นพีซี เซฟตี้ แอนด์ เอ็นไวเมนทอล เซอร์วิส จำกัด <br /><br />
        ธนาคารกรุงเทพ สาขามาบตาพุด เลขที่บัญชี 443-409850-7 <br /><br />
        1.3 ชำระผ่านระบบพร้อมเพย์หมายแลข 0105548019031 หรือแสกนผ่าน QR code นี้ <br /> <br /> 
            <img src='http://147.50.248.35/ila/images/qrprompay.jpg' /><br /> <br /> 

        2.ชำระด้วยเงินสด ณ สำนักงานบริษัทฯ วันจันทร์ – ศุกร์ เวลา 08:00 น. – 17:00 น. <br /><br /> 
        3.การหักภาษี ณ ที่จ่าย กรณีหักภาษี ณ ที่จ่าย ให้ส่งหนังสือรับรองการหักภาษี ณ ที่จ่าย มาที่ Email :
        <a href='mailtonpclegal@npc - se.co.th'>npclegal@npc-se.co.th</a>  หรือ ทางโทรสาร (FAX) 038 977701 ทันทีที่มีการชำระค่าบริการทุกช่องทาง <br /> <br />
</p>
        ชื่อผู้ใช้ (User name) : " & Username & " <br />
        รหัสผ่าน (Password) : " & Password & "  <br /> <br />

        หากท่านต้องการติดต่อสอบถามข้อมูลเพิ่มเติมสามารถติดต่อได้ตามช่องทางต่อไปนี้ <br />

        1. ติดต่อสอบถามเรื่องวิธีการสมัคร / ใบแจ้งค่าบริการ <br /><br />
        โทร. 061-345-4646 หรือ 098-324-4192 หรือ 092-594-6659 <br />
        อีเมลล์ <a href='mailto: npclegal@npc - se.co.th'>npclegal@npc-se.co.th</a> <br />
        Line id : @NPCi-LA (มี @ ด้วย) หรือ แสกน QR Code นี้ <br /><br />
        <img src='http://147.50.248.35/ila/images/qrline.jpg' />

        <br /><br />
        2.ติดต่อเรื่องใบเสร็จรับเงิน โทร. 038-977-641 หรือ 038-977-671 <br /> <br />

        บริษัทฯ ขอขอบพระคุณที่ท่านใช้บริการ i-Law : Legal Management System <br />
        ขอแสดงความนับถือ <br />
        บริษัท เอ็นพีซี เซฟตี้ แอนด์ เอ็นไวรอนเมนทอลเซอร์วิส จำกัด
    </font>"
        Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailMessage.From = New MailAddress("npcsafetyservice@gmail.com", SenderDisplayName)  'thaiergonomic
        mailMessage.Subject = MySubject
        mailMessage.Body = MyMessageBody
        mailMessage.IsBodyHtml = True
        mailMessage.[To].Add(New MailAddress(sTo))
        'Dim fStream As FileStream
        Dim dir As DirectoryInfo = New DirectoryInfo(Server.MapPath("Invoice\Payin\Payin" & txtCompanyCode.Text & ".pdf"))

        Dim objfile1 As FileInfo = New FileInfo(Server.MapPath("Invoice\Payin\Payin" & txtCompanyCode.Text & ".pdf"))
        If objfile1.Exists Then
            mailMessage.Attachments.Add(New Attachment(Server.MapPath("Invoice\Payin\Payin" & txtCompanyCode.Text & ".pdf")))
        End If

        Dim objfile2 As FileInfo = New FileInfo(Server.MapPath("Invoice\Invoice\ใบแจ้งค่าบริการInvoice" & DocumentNo & ".pdf"))
        If objfile2.Exists Then
            mailMessage.Attachments.Add(New Attachment(Server.MapPath("Invoice\Invoice\ใบแจ้งค่าบริการInvoice" & DocumentNo & ".pdf")))
        End If

        mailMessage.Attachments.Add(New Attachment(Server.MapPath("Invoice\20NPCS&E1.pdf")))

        Dim smtp As SmtpClient = New SmtpClient()
        smtp.Host = "smtp.gmail.com"
        smtp.EnableSsl = True
        Dim NetworkCred As NetworkCredential = New NetworkCredential()
        NetworkCred.UserName = mailMessage.From.Address
        NetworkCred.Password = "Qazxsw21"
        smtp.UseDefaultCredentials = True
        smtp.Credentials = NetworkCred
        smtp.Port = 587
        smtp.Send(mailMessage)
    End Sub

    Private Sub cmdConfirm_Click(sender As Object, e As EventArgs) Handles cmdConfirm.Click
        Dim Username, Password As String
        dt = ctlU.User_GetMainUserAdmin(hdCompanyUID.Value)
        If dt.Rows.Count > 0 Then
            Username = String.Concat(dt.Rows(0)("Username"))
            Password = enc.DecryptString(String.Concat(dt.Rows(0)("Passwords")), True)
        End If

        SendEmailConfirmRenew(hdCompanyUID.Value, txtFname.Text & " " & txtLname.Text, Username, Password, txtDueDate.Text, txtContactMail.Text)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ส่งอีเมล์ยืนยันการต่ออายุเรียบร้อย');", True)
    End Sub
    Private Sub SendEmailConfirmRenew(CompanyUID As Integer, PersonName As String, Username As String, Password As String, ExpireDate As String, sTo As String)
        Dim SenderDisplayName As String = "i-Law : Legal Management System"
        Dim MySubject As String = "ยืนยันการต่ออายุสมาชิกโปรแกรม i-Law : Legal Management System :" & txtNameTH.Text
        Dim MyMessageBody As String = ""

        MyMessageBody = " <font size='4'>
    
  <p> เรียน คุณ " & PersonName & "</p>
  <p> เรื่อง ยืนยันการต่ออายุสมาชิกโปรแกรม i-Law : Legal Management System </p>
  <p> บริษัทฯ ขอขอบพระคุณท่านเป็นอย่างสูงที่ได้ไว้วางใจในการเป็นสมาชิกโปรแกรม i-Law : Legal Management System ของเรา
        ซึ่งเป็นโปรแกรมการบริหารจัดการ (Legal Management) และตรวจประเมินความสอดคล้อง (Legal Compliance Audit)
        กฎหมายด้านความปลอดภัย อาชีวอนามัย พลังงานและสิ่งแวดล้อมอย่างเป็นระบบ
        ซึ่งรายละเอียดและสิทธิประโยชน์ของสมาชิกจะเป็นไปตาม Package ที่ท่านได้เลือกไว้ (รายละเอียดของ Package
        สามารถเข้าไปดูได้ตามลิงค์นี้ <a href='http://www.npc-legal.com'>http://www.npc-legal.com</a> ) และบริษัทฯ หวังเป็นอย่างยิ่งว่าโปรแกรมฯ นี้
        จะเป็นเครื่องมือหนึ่งที่ช่วยให้การจัดการด้านกฎหมายฯ และตรวจประเมินความสอดคล้องตามกฎหมายฯ
        ของท่านง่ายและสะดวกยิ่งขึ้น </p>

  <p>ข้อมูลสมาชิกและ Package การใช้งานของท่าน</p>
        ชื่อผู้ใช้ (User name) : " & Username & " <br />
        รหัสผ่าน (Password) : " & Password & "  <br /> <br />

        Package : " & ddlPackage.SelectedValue & "<br />  
        ระยะเวลาการใช้งาน : 1 ปี <br />  
        วันหมดอายุ (Expire date) : " & ExpireDate & " <br /><br />

        หากท่านต้องการติดต่อสอบถามข้อมูลเพิ่มเติมสามารถติดต่อได้ตามช่องทางต่อไปนี้ <br />

        1. ติดต่อสอบถามเรื่องวิธีการสมัคร / ใบแจ้งค่าบริการ <br /><br />
        โทร. 061-345-4646 หรือ 098-324-4192 หรือ 092-594-6659 <br />
        อีเมลล์ <a href='mailto: npclegal@npc - se.co.th'>npclegal@npc-se.co.th</a> <br />
        Line id : @NPCi-LA (มี @ ด้วย) หรือ แสกน QR Code นี้ <br /><br />
        <img src='http://147.50.248.35/ila/images/qrline.jpg' />

        <br /><br />
        2.ติดต่อเรื่องใบเสร็จรับเงิน โทร. 038-977-641 หรือ 038-977-671 <br /> <br />

        บริษัทฯ ขอขอบพระคุณที่ท่านใช้บริการ i-Law : Legal Management System <br />
        ขอแสดงความนับถือ <br />
        บริษัท เอ็นพีซี เซฟตี้ แอนด์ เอ็นไวรอนเมนทอลเซอร์วิส จำกัด
    </font>"
        Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailMessage.From = New MailAddress("npcsafetyservice@gmail.com", SenderDisplayName)  'thaiergonomic
        mailMessage.Subject = MySubject
        mailMessage.Body = MyMessageBody
        mailMessage.IsBodyHtml = True
        mailMessage.[To].Add(New MailAddress(sTo))
        'Dim fStream As FileStream
        'Dim dir As DirectoryInfo = New DirectoryInfo(Server.MapPath("Invoice\Payin\Payin" & txtCompanyCode.Text & ".pdf"))

        'Dim objfile1 As FileInfo = New FileInfo(Server.MapPath("Invoice\Payin\Payin" & txtCompanyCode.Text & ".pdf"))
        'If objfile1.Exists Then
        '    mailMessage.Attachments.Add(New Attachment(Server.MapPath("Invoice\Payin\Payin" & txtCompanyCode.Text & ".pdf")))
        'End If

        'Dim objfile2 As FileInfo = New FileInfo(Server.MapPath("Invoice\Invoice\ใบแจ้งค่าบริการInvoice" & DocumentNo & ".pdf"))
        'If objfile2.Exists Then
        '    mailMessage.Attachments.Add(New Attachment(Server.MapPath("Invoice\Invoice\ใบแจ้งค่าบริการInvoice" & DocumentNo & ".pdf")))
        'End If

        'mailMessage.Attachments.Add(New Attachment(Server.MapPath("Invoice\20NPCS&E1.pdf")))

        Dim smtp As SmtpClient = New SmtpClient()
        smtp.Host = "smtp.gmail.com"
        smtp.EnableSsl = True
        Dim NetworkCred As NetworkCredential = New NetworkCredential()
        NetworkCred.UserName = mailMessage.From.Address
        NetworkCred.Password = "Qazxsw21"
        smtp.UseDefaultCredentials = True
        smtp.Credentials = NetworkCred
        smtp.Port = 587
        smtp.Send(mailMessage)
    End Sub
End Class