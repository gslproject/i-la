﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Legal.aspx.vb" Inherits="iLA.Legal" %>
<%@ Import Namespace="System.Data" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">    

    <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-ribbon icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>ทะเบียนกฎหมาย
                            <div class="page-title-subheading">รายการทะเบียนกฎหมาย </div>
                        </div>
                    </div>
                </div>
            </div>

    <section class="content"> 
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon fa fa-balance-scale icon-gradient bg-success">
            </i>Legal List
            <div class="btn-actions-pane-right">
                <% If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = True Then%>  
                <a href="LegalModify?m=l&s=reg"  class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> ลงทะเบียนกฎหมายใหม่</a>    
                <% End If %>
            </div>
        </div>     
              <div class="card-body table-responsive">   
              <table id="tbdata" class="table table-bordered">
                <thead>
                <tr>
                 
                  <th class="text-center">Code</th>
                  <th class="text-center">ชื่อกฎหมาย</th>
                  <th class="text-center">ประเภท</th>
                     <th class="text-center">กฎหมายแม่บท</th>
                     <th class="text-center">กระทรวง</th>                
                  <th class="text-center">สถานะ</th>
                      <th class="text-center">วันที่ประกาศ</th>
                      <th class="text-center">วันที่มีผลบังคับใช้</th>
                   <% If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_ADM")) = True Then%>  
                  <th class="sorting_asc_disabled sorting_desc_disabled text-center"></th>       
                       <% End If %>
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLegal.Rows %>
                <tr>               

                  <td class="text-center"><% =String.Concat(row("Code")) %></td>
                  <td><a  href="LegalDetail?m=l&s=reg&id=<% =String.Concat(row("UID")) %>" ><% =String.Concat(row("LegalName")) %> </a>  </td>
                  <td><% =String.Concat(row("LawTypeName")) %></td>
                         <td class="text-center"><% =String.Concat(row("LawMaster")) %></td>
                         <td class="text-center"><% =String.Concat(row("MinistryName")) %></td>
                      <td class="text-center"><% =String.Concat(row("LegalStatusName")) %> </td>  
                  <td class="text-center"><% =String.Concat(row("IssueDate")) %></td>
                   <td class="text-center"><% =String.Concat(row("StartDate")) %></td>
                <% If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_ADM")) = True Then%>  
                    <td width="100" class="text-center">  
                        <% If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = True Then %>
                            <a href="LegalModify?m=l&s=reg&lid=<% =String.Concat(row("UID")) %>" class="btn btn-primary" ><i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;แก้ไข&nbsp;&nbsp;</a>
                        <% End If %>
                        <% If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_ADM")) = True And Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = False Then%>  
                            <a  href="LawRelease?m=l&s=rel&lid=<% =String.Concat(row("UID")) %>" class="btn btn-warning"><i class="fa fa-plus-circle" aria-hidden="true"></i> Release</a>
                        <% End If %>     
                    </td>
                <% End If %>
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body    <a href="LegalDetail?m=v&id=< % =String.Concat(row("UID")) %>" class="font-icon-button"><i class="fa fa-file-pdf" aria-hidden="true"></i> ดูรายละเอียด</a> -->
          </div>
 
  
    </section>
</asp:Content>
