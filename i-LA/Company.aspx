﻿<%@ Page Title="Company" MetaDescription="ข้อมูลบริษัทลูกค้า" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Company.aspx.vb" Inherits="iLA.Company" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-home icon-gradient bg-success"></i>
                        </div>
                        <div><%: Title %>    
                            <div class="page-title-subheading"><%: MetaDescription %>   </div>
                        </div>
                    </div>
                </div>
            </div>   

    <section class="content"> 
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-users icon-gradient bg-success">
            </i>ข้อมูลรายชื่อบริษัท
            <div class="btn-actions-pane-right">
                <a href="CompanyModify?m=comp" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus-circle"></i> เพิ่มใหม่</a>
            </div>
        </div>
        <div class="card-body">    
            <div class="table table-responsive">
              <table id="tbdata" class="table table-hover table-striped table-bordered dataTable dtr-inline">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled"></th>    
                    <th>รหัส</th>
                  <th>ชื่อบริษัท</th>   
                  <th class="text-center">สาขา</th>
                    <th class="text-center">Package</th>
                       <th class="text-center">DueDate</th> 
                       <th class="text-center">สถานะ</th>                     
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtC.Rows %>
                <tr>
                 <td width="30px"><a  href="CompanyModify?cid=<% =String.Concat(row("UID")) %>" ><img src="images/icon-edit.png"/></a>
                    </td>
                  <td><% =String.Concat(row("CompanyCode")) %></td>
                  <td><% =String.Concat(row("CompanyName")) %>    </td>     
                     <td><% =String.Concat(row("Branch")) %>    </td>  
                    <td class="text-center"><% =String.Concat(row("PackageNo")) %></td>
                    <td class="text-center"><% =Format(row("DueDate"), "dd/MM/yyyy") %></td> 
                    <td class="text-center<% If String.Concat(row("StatusFlag")) = "A" Then %> text-green<% End If %>"><% =String.Concat(row("StatusName")) %> </td>   
                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
          </div> 
        </div> 
          </div>
  
    </section>
</asp:Content>
