﻿Imports System.Data
Public Class Search
    Inherits System.Web.UI.Page
    Dim ctlL As New LawController
    Dim ctlM As New MasterController
    Public dtLegal As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadLawType()
            LoadKeyword()
            LoadBusinessType()
            LoadMinistry()
            LoadYear()
            LoadLegal()

        End If
    End Sub
    Private Sub LoadLegal()
        dtLegal = ctlL.Legal_GetBySearch(ddlMinistry.SelectedValue, ddlType.SelectedValue, ddlYear.SelectedValue, ddlBusinessType.SelectedValue, ddlKeyword.SelectedValue, txtSearch.Text)
    End Sub
    Private Sub LoadKeyword()
        ddlKeyword.DataSource = ctlM.Keyword_Get4Select()
        ddlKeyword.DataTextField = "Name"
        ddlKeyword.DataValueField = "UID"
        ddlKeyword.DataBind()
    End Sub
    Private Sub LoadBusinessType()
        ddlBusinessType.DataSource = ctlM.BusinessType_Get4Select()
        ddlBusinessType.DataTextField = "Name"
        ddlBusinessType.DataValueField = "UID"
        ddlBusinessType.DataBind()
    End Sub

    Private Sub LoadLawType()
        ddlType.DataSource = ctlL.LawType_Get4Select()
        ddlType.DataTextField = "Name"
        ddlType.DataValueField = "UID"
        ddlType.DataBind()
    End Sub


    Private Sub LoadMinistry()
        ddlMinistry.DataSource = ctlM.Ministry_Get4Select()
        ddlMinistry.DataTextField = "Name"
        ddlMinistry.DataValueField = "UID"
        ddlMinistry.DataBind()
    End Sub
    Private Sub LoadYear()
        ddlYear.DataSource = ctlL.LegalYear_Get4Select()
        ddlYear.DataTextField = "YearName"
        ddlYear.DataValueField = "LegalYear"
        ddlYear.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        LoadLegal()
    End Sub


End Class