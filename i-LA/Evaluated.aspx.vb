﻿Imports System.Data
Public Class Evaluated
    Inherits System.Web.UI.Page
    Dim ctlA As New AssessmentController
    Public dtAsm As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadPerson()
        End If
    End Sub
    Private Sub LoadPerson()
        If Not Request("pid") Is Nothing Then
            dtAsm = ctlA.Assessment_GetByPersonUID(Request("pid"))
        Else
            If Request.Cookies("iLaw")("ROLE_ADM") = True Or Request.Cookies("iLaw")("ROLE_SPA") = True Then
                dtAsm = ctlA.Assessment_Get
            Else
                dtAsm = ctlA.Assessment_GetByPersonUID(Request.Cookies("iLaw")("LoginPersonUID"))
            End If
        End If
    End Sub

End Class