﻿Imports System.Net
Imports System.Net.Mail
Public Class ForgotPassword
    Inherits Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lblResult.Visible = False
            lblAlert.Visible = False
        End If


    End Sub

    Private Sub SendEmail(PersonName As String, Username As String, Password As String, sTo As String)
        Dim SenderDisplayName As String = "i-Law : Legal Management System"
        Dim MySubject As String = "Request Password i-Law : Legal Management System"
        Dim MyMessageBody As String = ""

        MyMessageBody = " <font size='4'><p> เรียน คุณ " & PersonName & "</p><p>นี่คืออีเมล์แจ้งอัตโนมัติจากโปรแกรม i-Law : Legal Management System </p> "

        MyMessageBody &= " รหัสผ่านเพื่อเข้าใช้งานของท่าน คือ  <br/><br/>  

        ชื่อผู้ใช้ (Username) : " & Username & " <br />
        รหัสผ่าน (Password) : " & Password & "  <br /> <br />

        หากท่านต้องการติดต่อสอบถามข้อมูลเพิ่มเติมสามารถติดต่อได้ตามช่องทางต่อไปนี้ <br />

        1. ติดต่อสอบถามเรื่องวิธีการสมัคร / ใบแจ้งค่าบริการ <br /><br />
        โทร. 061-345-4646 หรือ 098-324-4192 หรือ 092-594-6659 <br />
        อีเมลล์ <a href='mailto: npclegal@npc - se.co.th'>npclegal@npc-se.co.th</a> <br />
        Line id : @NPCi-LA (มี @ ด้วย) หรือ แสกน QR Code นี้ <br /><br />
        <img src='http://147.50.248.35/ila/images/qrline.jpg' />

        <br /><br />
        2.ติดต่อเรื่องใบเสร็จรับเงิน โทร. 038-977-641 หรือ 038-977-671 <br /> <br />

        บริษัทฯ ขอขอบพระคุณที่ท่านใช้บริการ i-Law : Legal Management System <br />
        ขอแสดงความนับถือ <br />
        บริษัท เอ็นพีซี เซฟตี้ แอนด์ เอ็นไวรอนเมนทอลเซอร์วิส จำกัด
    </font>"
        Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailMessage.From = New MailAddress("npcsafetyservice@gmail.com", SenderDisplayName)  'thaiergonomic
        mailMessage.Subject = MySubject
        mailMessage.Body = MyMessageBody
        mailMessage.IsBodyHtml = True
        mailMessage.[To].Add(New MailAddress(sTo))


        Dim smtp As SmtpClient = New SmtpClient()
        smtp.Host = "smtp.gmail.com"
        smtp.EnableSsl = True
        Dim NetworkCred As NetworkCredential = New NetworkCredential()
        NetworkCred.UserName = mailMessage.From.Address
        NetworkCred.Password = "Qazxsw21"
        smtp.UseDefaultCredentials = True
        smtp.Credentials = NetworkCred
        smtp.Port = 587
        smtp.Send(mailMessage)
    End Sub

    Protected Sub cmdSubmit_Click(sender As Object, e As EventArgs) Handles cmdSubmit.Click
        Dim ctlU As New UserController
        Dim dtU As New DataTable
        Dim enc As New CryptographyEngine

        If txtUsername.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!!','Please input your username.');", True)
            Exit Sub
        End If

        dtU = ctlU.User_GetByUsername(txtUsername.Text)
        If dtU.Rows.Count > 0 Then
            Try
                SendEmail(dtU.Rows(0)("Fname") & " " & dtU.Rows(0)("Lname"), dtU.Rows(0)("Username"), enc.DecryptString(dtU.Rows(0)("Passwords"), True), dtU.Rows(0)("Email"))

                lblResult.Text = "ระบบดำเนินการส่งรหัสผ่านให้ท่านแล้ว กรุณาตรวจสอบอีเมล์ที่ท่านได้ลงทะเบียนไว้กับเรา"
                lblResult.Visible = Visible = True
            Catch ex As Exception
                'DisplayMessage(Me.Page, ex.Message)
                lblAlert.Text = ex.Message
                lblAlert.Visible = Visible = True
            End Try
        Else
            lblAlert.Text = "ตรวจสอบไม่พบ Username นี้ในระบบ กรุณาตรวจสอบและทำการ request ใหม่อีกครั้ง หรือ ติดต่อผู้ดูแลระบบ"
            lblAlert.Visible = Visible = True
        End If

    End Sub

End Class