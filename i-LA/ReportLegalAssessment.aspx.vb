﻿'Imports Newtonsoft.Json
Public Class ReportLegalAssessment
    Inherits System.Web.UI.Page
    Dim ctlR As New ReportController
    Public dtLegal As New DataTable
    Public dtPt As New DataTable
    Dim dt As New DataTable
    Public Shared datachart1 As String
    Public Shared catebar As String
    Public Shared databar1 As String
    Public Shared databar2 As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        dtLegal = ctlR.RPT_LegalAction_Assessment(StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iLaw2")("PeriodID")))

        dt = ctlR.RPT_LegalAction_Assessment_ForChart(StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iLaw2")("PeriodID")))

        datachart1 = dt.Rows(0)(0).ToString()   'JsonConvert.SerializeObject(dt, Formatting.None)

        dt = ctlR.RPT_LegalAction_AssessmentByDepartment_ForChart(StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iLaw2")("PeriodID")))

        catebar = ""
        databar1 = ""
        databar2 = ""
        For i = 0 To dt.Rows.Count - 1
            catebar = catebar + "'" + dt.Rows(i)("DepartmentCode") + "'"
            databar1 = databar1 + dt.Rows(i)("YCount").ToString()
            databar2 = databar2 + dt.Rows(i)("NCount").ToString()

            If i < dt.Rows.Count - 1 Then
                catebar = catebar + ","
                databar1 = databar1 + ","
                databar2 = databar2 + ","
            End If
        Next
        LoadLegalToGrid()
    End Sub
    Private Sub LoadLegalToGrid()
        Dim ctlL As New LawController
        dtPt = ctlL.LegalAction_GetByAsmStatus(StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iLaw2")("PeriodID")), "N")

    End Sub
End Class