﻿<%@ Page Title="Company" MetaDescription="ข้อมูลบริษัท/โรงงาน/ผู้ประกอบการ" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CompanyModify.aspx.vb" Inherits="iLA.CompanyModify" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-config icon-gradient bg-success"></i>
                        </div>
                        <div><%: Title %>    
                            <div class="page-title-subheading"><%: MetaDescription %>   </div>
                        </div>
                    </div>
                </div>
            </div>   

    <section class="content">   
         <div class="main-card mb-3 card">
    <div class="card-header"><i class="header-icon lnr-home icon-gradient bg-primary">
            </i>ข้อมูลทั่วไป
            <div class="btn-actions-pane-right"><asp:HiddenField ID="hdCompanyUID" runat="server" />
            </div>
        </div>
        <div class="card-body">         

                 <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>Code</label>
                  <asp:TextBox ID="txtCompanyCode" runat="server" cssclass="form-control text-center" ></asp:TextBox>   
                 </div> 
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>Name (ไทย)</label> <asp:TextBox ID="txtNameTH" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
                 </div> 
            </div>
                     <div class="col-md-5">
              <div class="form-group">
                <label>Name (English)</label> <asp:TextBox ID="txtNameEN" runat="server" cssclass="form-control" placeholder="English Name"></asp:TextBox>
                 </div>
            </div>
                     </div>
     <div class="row">
                      <div class="col-md-6">
              <div class="form-group">
                <label>Branch</label> <asp:TextBox ID="txtBranch" runat="server" cssclass="form-control" placeholder="สาขา"></asp:TextBox>
                 </div>
            </div>
 <div class="col-md-6">
              <div class="form-group">
                <label>TAX ID</label> <asp:TextBox ID="txtTaxID" runat="server" cssclass="form-control" placeholder="เลขประจำตัวผู้เสียภาษี"></asp:TextBox>
                 </div>
            </div> 

</div>
                <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>Address no.</label> <asp:TextBox ID="txtAddressNo" runat="server" cssclass="form-control" placeholder="บ้านเลขที่"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Lane</label> <asp:TextBox ID="txtLane" runat="server" cssclass="form-control" placeholder="ซอย"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Road</label> <asp:TextBox ID="txtRoad" runat="server" cssclass="form-control" placeholder="ถนน"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Sub District</label> <asp:TextBox ID="txtSubDistrict" runat="server" cssclass="form-control" placeholder="ตำบล/แขวง"></asp:TextBox>
                 </div>
            </div>
            </div>
                   <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>district</label> <asp:TextBox ID="txtDistrict" runat="server" cssclass="form-control" placeholder="อำเภอ"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Province</label> 
                  <asp:DropDownList ID="ddlProvince" runat="server" cssclass="form-control select2" Width="100%" ></asp:DropDownList>
                   
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Zip Code</label> <asp:TextBox ID="txtZipcode" runat="server" cssclass="form-control" placeholder="รหัสไปรษณีย์"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Country</label> 
                  <asp:DropDownList ID="ddlCountry" runat="server" cssclass="form-control select2" Width="100%" >
                      <asp:ListItem Selected="True" Value="TH">ประเทศไทย</asp:ListItem>
                  </asp:DropDownList>                   
                 </div>
            </div>     
            </div>
              <div class="row">             
          
               <div class="col-md-3">
              <div class="form-group">
                <label>Telephone</label> <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Fax</label> <asp:TextBox ID="txtFax" runat="server" cssclass="form-control" placeholder="แฟกซ์"></asp:TextBox>
                 </div>
            </div>
              <div class="col-md-3">
              <div class="form-group">
                <label>E-mail</label> <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
                 </div>
            </div>
 
                     <div class="col-md-3">
              <div class="form-group">
                <label>Website</label> <asp:TextBox ID="txtWebsite" runat="server" cssclass="form-control" placeholder="เว็บไซต์"></asp:TextBox>
                 </div>
            </div> 
                  
            </div>         
         
             <div class="row">                          
              <div class="col-md-12">
                <div class="form-group">
                     <div class="text-bold text-blue"><label>ที่อยู่สำหรับออกใบเสร็จ</label> </div>
                   <asp:TextBox ID="txtAddressInvoice" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100"></asp:TextBox>
                </div>
              </div>
               
   </div> 
             <div class="row">                          
              <div class="col-md-6">
                <div class="form-group">
                   <div class="text-bold text-blue"><label>ชื่อ-นามสกุลผู้ติดต่อ</label> </div>
                   <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control text-center"  placeholder="ชื่อผู้ติดต่อ" ></asp:TextBox>
                </div>
              </div>
                 <div class="col-md-6">
              <div class="form-group">
                <label>E-mail สำหรับส่งใบแจ้งค่าบริการ</label> <asp:TextBox ID="txtContactMail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
                 </div>
            </div>
   </div> 

          
                
            </div> 
             <div class="card-footer  text-center"> 
               
              </div> 
</div>
  <div class="main-card mb-3 card">
    <div class="card-header"><i class="header-icon lnr-user icon-gradient bg-primary">
            </i>ข้อมูลกิจการ
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body">     
              <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>ประเภทธุรกิจ</label>
                                            <asp:DropDownList ID="ddlBusinessType" runat="server" CssClass="form-control select2">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                      <div class="col-md-3">
                                        <div class="form-group">
                                            <label>กลุ่มโรงงานในเขตอุตสาหกรรม</label>
                                             <div class="button r" id="button-1"> 
              <input id="optFactoryGroup" type="checkbox" class="checkbox" runat="server" >          
          <div class="knobs"></div>
          <div class="layer"></div>
        </div>
                                           
                                        </div>

                                    </div>
                   <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Implementation phase</label>
                                            <asp:DropDownList ID="ddlPhase" runat="server" CssClass="form-control select2">                                                
                                            </asp:DropDownList>                                           
                                        </div>

                                    </div>
                </div>  
                     <div class="row">
 <div class="col-md-12">
                                        <div class="form-group">
                                            <label>ประเภทโรงงาน</label>
                                            <asp:DropDownList ID="ddlFactoryType" runat="server" CssClass="form-control select2">                                                
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                    

                                    </div>
             <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Keyword</label>
                                            <asp:CheckBoxList ID="chkKeyword" runat="server"   RepeatColumns="5">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    </div>
            <div class="row">
                                   
                                
           

 </div> 
</div>
    </div> 

 <div class="main-card mb-3 card">
    <div class="card-header"><i class="header-icon lnr-gift icon-gradient bg-primary">
            </i>Package การใช้งาน
            <div class="btn-actions-pane-right">
            </div>
        </div>
        <div class="card-body"> 
             <div class="row"> 
               <div class="col-md-2">
              <div class="form-group">
                <label>Package</label>
                  <asp:DropDownList ID="ddlPackage" runat="server" cssclass="form-control select2" Width="100%" >
                      <asp:ListItem Selected="True" Value="1">Package 1</asp:ListItem>
                      <asp:ListItem Value="2">Package 2</asp:ListItem>
                  </asp:DropDownList>
                 </div>
            </div>
                  <div class="col-md-2">
              <div class="form-group">
                <label>User Limit</label>
                  <asp:TextBox ID="txtUserLimit" runat="server" cssclass="form-control text-center" ></asp:TextBox>   
                 </div> 
            </div>
                  <div class="col-md-2">
                                        <div class="form-group">
                                            <label>วันที่เริ่มใช้งาน</label>
                                            <div class="input-group">                                             
                                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                   <div class="col-md-2">
                                        <div class="form-group">
                                            <label>วันที่สิ้นสุด</label>
                                            <div class="input-group">                                             
                                                <asp:TextBox ID="txtDueDate" runat="server" CssClass="form-control"
                                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                                    data-date-language="th-th" data-provide="datepicker"
                                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                  <div class="col-md-2">
              <div class="form-group">
                            <label>Active</label>  
        <div class="button r" id="button-1"> 
              <input id="chkStatus" type="checkbox" class="checkbox" runat="server" checked>          
          <div class="knobs"></div>
          <div class="layer"></div>
        </div> 
                  </div>
            </div> 
  </div> 
             <div class="card-footer  text-center"> 
               
              </div> 
</div>
     </div> 
     <div class="text-center">
  <asp:Button ID="cmdSave" cssclass="btn btn-primary" runat="server" Text="Save"  data-dismiss="modal" Width="120px" />  
                  <asp:Button ID="cmdDelete" cssclass="btn btn-danger" runat="server" Text="Delete"  data-dismiss="modal" Width="120px" />  
        </div>
        
     <div class="text-center">
           <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="100%" ProcessingMode="Remote" ShowParameterPrompts="False" ShowPrintButton="true" BackColor="White" DocumentMapWidth="100%" ZoomMode="PageWidth" Visible="false">
    </rsweb:ReportViewer>
         </div>
      </section>
</asp:Content>