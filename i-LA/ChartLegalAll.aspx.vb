﻿
Public Class ChartLegalAll
    Inherits Page
    Public dtLegalNew As New DataTable

    Dim ctlR As New ReportController
    'Public dtLegal As New DataTable
    Dim dtAll As New DataTable
    Dim dtNew As New DataTable
    Public Shared catebar2 As String
    Public Shared databar2 As String
    Public Shared datachart1 As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim ctlL As New LawController
        dtLegalNew = ctlL.Legal_GetLast()

        dtAll = ctlR.Legal_GetCountByType
        'dtNew = ctlR.Legal_GetLastMonthCountByType


        catebar2 = ""
        databar2 = ""

        dtNew = ctlR.RPT_LegalAction_Assessment_ForChart(StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), StrNull2Zero(Request.Cookies("iLaw2")("PeriodID")))

        datachart1 = dtNew.Rows(0)(0).ToString()


        For i = 0 To dtAll.Rows.Count - 1
            catebar2 = catebar2 + "'" + dtAll.Rows(i)("LawTypeName") + "'"
            databar2 = databar2 + dtAll.Rows(i)("LCount").ToString()

            If i < dtAll.Rows.Count - 1 Then
                catebar2 = catebar2 + ","
                databar2 = databar2 + ","
            End If
        Next


    End Sub
End Class