﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data
Public Class LawImport
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlM As New MasterController
    Dim ctlC As New CompanyController
    Dim ctlE As New LawController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblResult.Visible = False
            lblAlert.Visible = False
            'pnProgress.Visible = True
            'Select Case Request("m")
            '    Case "im1"

            'End Select

            'ImportData()
        End If
    End Sub
    'Private Function VerifyData(ByVal dtEmp As DataTable) As Boolean
    '    Dim sError As Boolean = False
    '    Try
    '        If dtEmp.Rows.Count > 0 Then

    '            Dim ctlM As MasterController = New MasterController()
    '            Dim ctlE As EmployeeController = New EmployeeController()
    '            dtEmp.Columns.Add("PositionUID")
    '            dtEmp.Columns.Add("DepartmentUID")
    '            dtEmp.Columns.Add("DivisionUID")
    '            dtEmp.Columns.Add("Error")

    '            For Each row As DataRow In dtEmp.Rows

    '                If Not Equals(row(0).ToString(), "") Then
    '                    row("PositionUID") = ctlM.Position_GetUID(row(4).ToString())
    '                    row("DepartmentUID") = ctlM.Department_GetUID(row(5).ToString())
    '                    row("DivisionUID") = ctlM.Division_GetUID(row(6).ToString())

    '                    If Equals(row("PositionUID").ToString(), "0") Then
    '                        row("Error") = row("Error") & "[ไม่พบตำแหน่ง]"
    '                        sError = True
    '                    End If

    '                    If Equals(row("DepartmentUID").ToString(), "0") Then
    '                        row("Error") = row("Error") & "[ไม่พบแผนก]"
    '                        sError = True
    '                    End If

    '                    If Equals(row("DivisionUID").ToString(), "0") Then
    '                        row("Error") = row("Error") & "[ไม่พบฝ่าย]"
    '                        sError = True
    '                    End If

    '                    If ctlE.Employee_Duplicate(row(0).ToString()) Then
    '                        row("Error") = row("Error") & "[พนักงานซ้ำ]"
    '                        sError = True
    '                    End If
    '                End If

    '                row.AcceptChanges()
    '            Next

    '            grdData.DataSource = dtEmp
    '            grdData.DataBind()


    '            If sError = True Then
    '                Return False
    '            Else
    '                Return True
    '            End If
    '        Else
    '            Return False
    '        End If
    '    Catch ex As Exception
    '        Return False
    '    End Try
    'End Function

    Private Sub ImportData()
        System.Threading.Thread.Sleep(3000)
        Dim fileName As String = ""
        Select Case Request("m")
            Case "im1"
                fileName = "LawTemplate.xls"
            Case "im2"
                fileName = "LawPracticeTemplate.xls"
        End Select

        pnProgress.Visible = True
        Dim connectionString As String = ""
        Try

            lblResult.Visible = False

            Dim fileExtension As String = Path.GetExtension(fileName)
            Dim fileLocation As String = Server.MapPath("~/" + tmpUpload + "/" + fileName)

            Dim objfile As FileInfo = New FileInfo(fileLocation)
            'If objfile.Exists Then
            '    objfile.Delete()
            'End If

            If fileExtension = ".xls" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            ElseIf fileExtension = ".xlsx" Then
                lblAlert.Text = "ตรวจสอบเวอร์ชั่นไฟล์ Excel สามารถใช้ได้เฉพาะ .xls เท่านั้น"
                lblAlert.Visible = True
                Exit Sub
            End If

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            Dim LawCode As String = ""
            Dim PctCode As String = ""
            Dim k As Integer = 0

            Select Case Request("m")
                Case "im1"
                    fileName = "LawTemplate"
                Case "im2"
                    For i = 0 To dtExcelRecords.Rows.Count - 1

                        With dtExcelRecords.Rows(i)
                            If .Item(0).ToString <> "" Then
                                If ctlE.Legal_CheckDuplicate(String.Concat(.Item(0))) = True Then
                                    If ctlE.Practice_CheckDuplicate(String.Concat(.Item(0)), String.Concat(.Item(1))) Then
                                        PctCode = PctCode & String.Concat(.Item(0)) & "=" & String.Concat(.Item(1)) & ","
                                    Else
                                        ctlE.Practice_SaveByImport(String.Concat(.Item(0)), String.Concat(.Item(1)), String.Concat(.Item(2)), StrNull2Zero(String.Concat(.Item(3))), Request.Cookies("iLaw")("userid"))

                                        k = k + 1
                                    End If
                                Else
                                    LawCode = LawCode & String.Concat(.Item(0)) & "," & vbCrLf
                                End If
                            End If
                        End With
                    Next

                    Dim ctlU As New UserController
                    ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_ADD, "Practice", "import Practice", "import Practice : " & k & " รายการ จากไฟล์ excel")
            End Select

            lblResult.Text = "ข้อมูลทั้งหมด " & k & " รายการ ถูก import เรียบร้อย" & IIf(PctCode <> "", vbCrLf & " รหัส " & PctCode & " ไม่สามารถเพิ่มได้เนื่องจากมีในฐานข้อมูลแล้ว ", "")
            lblResult.Visible = True
            lblAlert.Visible = False
            dtExcelRecords = Nothing

            If objfile.Exists Then
                objfile.Delete()
            End If

            If LawCode <> "" Then
                lblAlert.Text = "ไม่พบกฎหมาย " & LawCode & " ในระบบ"
                lblAlert.Visible = True
            End If
            'Else
            '    lblAlert.Text = "Error! กรุณาตรวจสอบไฟล์ Excel แล้วลองใหม่อีกครั้ง"
            '    lblAlert.Visible = True
            'End If

        Catch ex As Exception
            lblAlert.Text = "Error : " & ex.Message
            lblAlert.Visible = True
        End Try
    End Sub

    Private Sub cmdImport_Click(sender As Object, e As EventArgs) Handles cmdImport.Click
        'If (StrNull2Zero(lblRemain.Text) <= 0) Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บริษัทท่านได้เพิ่ม Legal ครบตามแพคเก็จแล้ว ไม่สามารถเพิ่มได้อีก');", True)
        'End If
        If (Not FileUploadFile.HasFile) Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกไฟล์ Excel ก่อน');", True)
            Exit Sub
        End If
        Dim connectionString As String = ""
        Try
            lblResult.Visible = False

            Dim fileName As String = FileUploadFile.FileName
            Dim fileExtension As String = Path.GetExtension(FileUploadFile.PostedFile.FileName)
            Dim fileLocation As String = Server.MapPath("~/" + tmpUpload + "/" + fileName)

            Dim objfile As FileInfo = New FileInfo(fileLocation)
            If objfile.Exists Then
                objfile.Delete()
            End If

            FileUploadFile.SaveAs(Server.MapPath("~/" + tmpUpload + "/" + fileName))


            'Check whether file extension is xls or xslx

            If fileExtension = ".xls" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            ElseIf fileExtension = ".xlsx" Then
                If (Not FileUploadFile.HasFile) Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกไฟล์ Excel เวอร์ชั่น .xls เท่านั้น');", True)
                    Exit Sub
                End If
                lblAlert.Text = "ตรวจสอบเวอร์ชั่นไฟล์ Excel สามารถใช้ได้เฉพาะ .xls เท่านั้น"
                lblAlert.Visible = True
                Exit Sub
                'connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            End If

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            Dim strCode As String = ""
            Dim k As Integer = 0
            Dim sD, sM, sY, sRegisterDate As String
            Dim RegisDate As Date
            sD = ""
            sM = ""
            sY = ""
            RegisDate = ctlM.GET_DATE_SERVER()

            sD = Day(RegisDate).ToString("0#")
            sM = Month(RegisDate).ToString("0#")
            sY = (Year(RegisDate)).ToString()

            If CInt(sY) < 2564 Then
                sY = (CInt(sY) + 543).ToString()
            End If

            sRegisterDate = ""
            sRegisterDate = sD & "/" & sM & "/" & sY


            'If VerifyData(dtExcelRecords) Then
            For i = 0 To dtExcelRecords.Rows.Count - 1

                'StartDate = ""

                With dtExcelRecords.Rows(i)
                    If .Item(0).ToString <> "" Then
                        If ctlE.Legal_CheckDuplicate(String.Concat(.Item(0))) Then
                            strCode = strCode & String.Concat(.Item(0)) & "," & vbCrLf
                        Else

                            'sD = Left(String.Concat(.Item(12)), 2)
                            'sM = Mid(String.Concat(.Item(12)), 4, 2)
                            'sY = Right(String.Concat(.Item(12)), 4)

                            'If CInt(sY) > 2500 Then
                            '    sY = (CInt(sY) - 543).ToString()
                            'End If

                            'IssueDate = sY & "-" & sM & "-" & sD

                            'sD = Left(String.Concat(.Item(13)), 2)
                            'sM = Mid(String.Concat(.Item(13)), 4, 2)
                            'sY = Right(String.Concat(.Item(13)), 4)

                            'If CInt(sY) > 2500 Then
                            '    sY = (CInt(sY) - 543).ToString()
                            'End If

                            'StartDate = sY & "-" & sM & "-" & sD

                            Dim sK As String = ""
                            sK = Right(String.Concat(.Item(14)), Len(String.Concat(.Item(14))) - 1)

                            ctlE.Legal_SaveByImport(String.Concat(.Item(0)), String.Concat(.Item(1)), String.Concat(.Item(2)), StrNull2Zero(String.Concat(.Item(3))), StrNull2Zero(String.Concat(.Item(4))), StrNull2Zero(String.Concat(.Item(5))), StrNull2Zero(String.Concat(.Item(6))), StrNull2Zero(String.Concat(.Item(7))), StrNull2Zero(String.Concat(.Item(8))), StrNull2Zero(String.Concat(.Item(9))), StrNull2Zero(String.Concat(.Item(10))), StrNull2Zero(String.Concat(.Item(11))), String.Concat(.Item(12)), String.Concat(.Item(13)), sRegisterDate, sK, String.Concat(.Item(15)), Request.Cookies("iLaw")("userid"))

                            Dim sKeyword() As String
                            Dim LegalUID As Integer
                            LegalUID = ctlE.Legal_GetUID(String.Concat(.Item(0)))

                            If sK <> "" Then
                                sKeyword = Split(sK, "|")
                                For n = 0 To sKeyword.Length - 1
                                    If IsNumeric(sKeyword(n)) Then
                                        ctlE.LegalKeyword_Save(LegalUID, sKeyword(n), Request.Cookies("iLaw")("userid"))
                                    End If
                                Next
                            End If

                            k = k + 1
                        End If
                    End If
                End With
            Next
            'grdData.DataSource = dtExcelRecords
            'grdData.DataBind()

            Dim ctlU As New UserController
            ctlU.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_ADD, "Legal", "import Legal", "import Legal : " & k & " รายการ จากไฟล์ excel")

            lblResult.Text = "ข้อมูลทั้งหมด " & k & " รายการ ถูก import เรียบร้อย" & IIf(strCode <> "", vbCrLf & " รหัส " & strCode & " ไม่สามารถเพิ่มได้เนื่องจากมีในฐานข้อมูลแล้ว ", "")
            lblResult.Visible = True
            lblAlert.Visible = False
            dtExcelRecords = Nothing

            If objfile.Exists Then
                objfile.Delete()
            End If
            lblAlert.Visible = False
            'Else
            '    lblAlert.Text = "Error! กรุณาตรวจสอบไฟล์ Excel แล้วลองใหม่อีกครั้ง"
            '    lblAlert.Visible = True
            'End If

        Catch ex As Exception
            lblAlert.Text = "Error : " & ex.Message
            lblAlert.Visible = True
        End Try
        'pnProgress.Visible = False
    End Sub
End Class