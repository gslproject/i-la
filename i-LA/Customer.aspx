﻿<%@ Page Title="Customer" MetaDescription="ระบบการต่ออายุสมาชิก" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Customer.aspx.vb" Inherits="iLA.Customer" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
     <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-home icon-gradient bg-success"></i>
                        </div>
                        <div><%: Title %>    
                            <div class="page-title-subheading"><%: MetaDescription %>   </div>
                        </div>
                    </div>
                </div>
            </div>   

    <section class="content"> 
         <div class="main-card mb-3 card">
        <div class="card-header"><i class="header-icon lnr-users icon-gradient bg-success">
            </i>ข้อมูลรายชื่อลูกค้า
            <div class="btn-actions-pane-right">
                
            </div>
        </div>
        <div class="card-body">                  
              <table id="tbdata" class="table table-hover table-striped table-bordered dataTable dtr-inline">
                <thead>
                <tr>               
                  <th>Code</th>
                  <th>Name</th>               
                  <th>Address</th>  
                    <th class="text-center">Package</th>
                    <th class="text-center">วันครบกำหนด</th>
                     <th>Payin</th> 
                     <th>15 วัน</th>
                     <th>30 วัน</th>
                     <th>90 วัน</th>
                     <th class="text-center">สถานะ</th>  
                      <th class="sorting_asc_disabled sorting_desc_disabled text-center">Manage</th>  
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtC.Rows %>
                <tr>                
                  <td><% =String.Concat(row("CompanyCode")) %></td>
                  <td><% =String.Concat(row("CompanyName")) %>    </td>               
                  <td><% =String.Concat(row("CompanyAddress")) %></td>
                    <td class="text-center"><% =String.Concat(row("PackageNo")) %></td>
                    <td class="text-center"><% =Format(row("DueDate"), "dd/MM/yyyy") %></td>
                    <td class="text-center"> 
                      <% If String.Concat(row("PayinFile")) <> "" %>
                        <a href="Invoice/Payin/<% =String.Concat(row("PayinFile")) %>" target="_blank" class="font-icon-button"><i class="fa fa-file-pdf" aria-hidden="true"></i></a>
                      <% End If %>
                    </td>
                      
                     <td class="text-center">
                         <% If String.Concat(row("Invoice15")) <> "" Then %>
                        <a href="Invoice/Invoice/<% =String.Concat(row("Invoice15")) %>" target="_blank" class="font-icon-button"><i class="fa fa-file-pdf" aria-hidden="true"></i></a>
                      <% End If %>
                     </td>
                     <td class="text-center">
                           <% If String.Concat(row("Invoice30")) <> "" Then %>
                        <a href="Invoice/Invoice/<% =String.Concat(row("Invoice30")) %>" target="_blank" class="font-icon-button"><i class="fa fa-file-pdf" aria-hidden="true"></i></a>
                      <% End If %>
                     </td>
                     <td class="text-center">
                           <% If String.Concat(row("Invoice90")) <> "" Then %>
                        <a href="Invoice/Invoice/<% =String.Concat(row("Invoice90")) %>" target="_blank" class="font-icon-button"><i class="fa fa-file-pdf" aria-hidden="true"></i></a>
                      <% End If %>
                     </td>
                    <td class="text-center<% If String.Concat(row("StatusFlag")) = "A" Then %> text-green<% End If %>"><% =String.Concat(row("StatusName")) %> </td>    
                    <td style="width:110px"><a href="CustomerManage?m=cus&cid=<% =String.Concat(row("CompanyUID")) %>" class="btn btn-primary btn-sm pull-right"><i class="fa fa-cogs"></i> Manage</a>
                    </td>
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
          </div>
  
    </section>
</asp:Content>
