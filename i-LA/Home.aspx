﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Home.aspx.vb" Inherits="iLA.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
       <link href="assets/styles.css" rel="stylesheet" />   

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>

    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      // Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
      // Based on https://gist.github.com/blixt/f17b47c62508be59987b
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-light icon-gradient bg-mean-fruit"></i>
                                </div>
                                <div>Dashboard
                                    <div class="page-title-subheading">ระบบบริหารจัดการกฎหมาย</div>
                                </div>
                            </div>
                        </div>
                    </div>

  <!-- Main content -->
  <section class="content">  
      <h5>จำนวนกฎหมายทั้งหมด</h5>
    <div class="row"> 
      <section class="col-lg-6 connectedSortable">
          <div class="main-card mb-3 card">
              <div class="card-body"> 
                  <h5 class="card-title">กฎหมายใหม่เดือนล่าสุด</h5> 
                  <div id="chart"></div> 
              </div>
          </div>
</section>   
        <section class="col-lg-6 connectedSortable">
  <div class="main-card mb-3 card">
                                            <div class="card-body">                                             
                                                <h5 class="card-title">แยกตามประเภท</h5>
                                                <div id="chart2"></div>
                                            </div>
                                        </div>
            </section>
        </div>  
    <div class="row">
      <section class="col-lg-6 connectedSortable">
            <div class="main-card mb-3 card">
                                            <div class="card-body">                                             
                                                <h5 class="card-title">แยกตามสถานะ</h5>
                                                <div id="piechart"></div>
                                            </div>
                                        </div>

          <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <h5 class="card-title">Shortcut Menu</h5>
                                            <div class="grid-menu grid-menu-3col">
                                                <div class="no-gutters row">                                                   
                                                    <div class="col-sm-6 col-xl-4">
                                                   
                                                        <a href="ChartLegalAll.aspx" target="_blank" class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                            <i class="fa fa-balance-scale btn-icon-wrapper"> </i>จำนวนกฎหมาย<br />ทั้งหมด
                                                    
                                                            </a>
                                                    </div>
                                                     <% If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = False Then %> 
                                                      <div class="col-sm-6 col-xl-4">
                                                   
                                                        <a href="LegalNew?m=new" target="_blank" class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                            <i class="lnr-diamond btn-icon-wrapper"> </i>กฎหมายใหม่<br />เดือนล่าสุด
                                                    
                                                            </a>
                                                    </div>
                                                     <% End If %>
                                                   <div class="col-sm-6 col-xl-4"> <a href="ReportLegalAssessment?m=rpt&s=1" target="_blank" class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                              <i class="lnr-magic-wand btn-icon-wrapper"> </i>ผลการประเมิน<br />ความสอดคล้อง                                                  </a>
                                                         
                                                    </div>
                                                    <div class="col-sm-6 col-xl-4">
                                                       <a href="LegaListStatus?m=rpt&s=1&st=no" target="_blank" class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">    
                                                            <i class="lnr-sad btn-icon-wrapper"> </i>กฎหมาย<br />ที่ไม่สอดคล้อง
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-6 col-xl-4"> <a href="LegaListStatus?m=rpt&s=1&st=yes" target="_blank" class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">                                                        
                                                            <i class="lnr-smile btn-icon-wrapper"> </i>กฎหมาย<br />ที่สอดคล้อง
                                                        </a>
                                                    </div>
                                                  
                                                     <% If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = True Then %>    
                                                      <div class="col-sm-6 col-xl-4">                                                   
                                                        <a href="LegalModify.aspx?m=l&s=reg" target="_blank" class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">
                                                            <i class="lnr-pencil btn-icon-wrapper"> </i>ลงทะเบียน<br />กฎหมายใหม่                                                      </a>
                                                    </div>
                                                    <% End If %>
                                                      <div class="col-sm-6 col-xl-4">
                                                       <a href="Calendar.aspx" target="_blank" class="btn-icon-vertical btn-square btn-transition btn btn-outline-primary">    
                                                            <i class="lnr-calendar-full btn-icon-wrapper"> </i>ปฏิทินรายการ<br />ที่ต้องดำเนินการ
                                                        </a>
                                                    </div>
                                                     
                                                </div>
                                            </div>                                          
                                        </div>
                                    </div> 
 
      </section>
        <section class="col-lg-6 connectedSortable">
        <div class="box box-primary">
          <div class="box-body no-padding">
            <!-- THE CALENDAR -->
            <div id="calendar2"></div>
          </div>
        </div>
      </section>
    </div>    
</section>


    <script>
      
        var options = {
            series: [{
              name:'',
          data: [<%=databar1 %>]
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
            bar: {
              borderRadius: 10,
            columnWidth: '45%',
              distributed: true,
            dataLabels: {
              position: 'top', // top, center, bottom
            }
          }
        },
        dataLabels: {
          enabled: true,
            offsetX: -1,
            offsetY: -15,
          style: {
            fontSize: '12px',
            colors: ['#000']
          }
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [<%=catebar1 %>],
          labels: {
            style: {
              colors: colors,
              fontSize: '12px'
            }
          }
            },
         yaxis: {
          axisBorder: {
            show: true
          },
          axisTicks: {
            show: true,
          },
          labels: {
            show: true,
            formatter: function (val) {
              return val;
            }
          }
        
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();  


      var options2 = {
          series: [{
          data: [<%=databar2 %>]
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
            bar: {
              borderRadius: 10,
            columnWidth: '45%',
              distributed: true,
            dataLabels: {
              position: 'top', // top, center, bottom
            }
          }
        },
        dataLabels: {
          enabled: true,
             offsetX: -1,
            offsetY: -15,         
          style: {
            fontSize: '12px',
            colors: ['#000']
          }
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [<%=catebar2 %>],
          labels: {
            style: {
              colors: colors,
              fontSize: '12px'
            }
          }
          },
         yaxis: {
          axisBorder: {
            show: true
          },
          axisTicks: {
            show: true,
          },
          labels: {
            show: true,
            formatter: function (val) {
              return val;
            }
          }
        
        }
        };

        var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);
        chart2.render();    
      
      
    </script>
      <script>
      
        var options3 = {
          series: [<%=hDatachart1 %>],
          chart: {
          width: 380,
          type: 'pie',
          toolbar: {
            show: true
          }
        },
        labels: ['ดำเนินการประเมินแล้ว', 'รอการอนุมัติ', 'อนุมัติแล้ว'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart3 = new ApexCharts(document.querySelector("#piechart"), options3);
        chart3.render();
      
      
    </script>
        
</asp:Content>