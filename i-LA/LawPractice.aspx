﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LawPractice.aspx.vb"
    Inherits="iLA.LawPractice" %>
<%@ Import Namespace="System.Data" %> 
        <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
   
        </asp:Content>
        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>สาระสำคัญข้อปฏิบัติ 
                            <div class="page-title-subheading">ประเมินความสอดคล้องของกฎหมาย</div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <section class="col-lg-12">                          
             <div class="app-page-header">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">                      
                        <div><asp:label ID="lblLegalName" runat="server"></asp:label><asp:HiddenField ID="hdLegalUID" runat="server" />
                            <div class="page-title-subheading">รหัสกฎหมาย <asp:label ID="lblLegalCode" runat="server"></asp:label></div>
                        </div>
                    </div>
                </div>
            </div>
     </section>
         
<section class="col-lg-12">
                        <div class="main-card mb-3 card">
                            <div class="card-header">ข้อสาระสำคัญที่ต้องปฏิบัติ</div>
                            <div class="card-body">
    <div class="table-responsive">                            
            <div id="exampleAccordion" data-children=".item">        
              <table id="tbdata" class="table table-hover">
                <thead>
                <tr>              
                  <th class="text-center">รหัส</th> 
                  <th class="text-center">ผลการประเมิน</th>
                  <th class="text-center">ข้อสาระสำคัญ (ข้อปฏิบัติ Practice)</th>
                  <th class="text-center">ความถี่</th>
                  <th class="text-center">หน่วยงานที่รับผิดชอบ</th>
                </tr>
                </thead>
                <tbody>
              
            <% For Each row As DataRow In dtLg.Rows %>                        
                <tr>
                  <td class="text-center"><% =String.Concat(row("Code")) %></td>
                  <td class="text-center">
                      <% =IIf(String.Concat(row("AsmResult")) = "Y", "<img src='images/statusicon_yes.png' height='25px' />", IIf(String.Concat(row("AsmResult")) = "N", "<img src='images/statusicon_no.png' height='25px' />", "")) %> </td>  
                  <td>
                      <a href="#collapseExample<% =String.Concat(row("nRow")) %>" aria-expanded="false" aria-controls="exampleAccordion<% =String.Concat(row("nRow")) %>" data-toggle="collapse"><% =String.Concat(row("PracticeDescriptions")) %></a>
                      <div  data-parent="#exampleAccordion" id="collapseExample<% =String.Concat(row("nRow")) %>" class="collapse">
   <div class="box box-solid">
            <div class="box-header">
              <i class="fa fa-grear"></i>
              <h3 class="box-title">Flow Status</h3>          
                 <div class="box-tools pull-right">
              </div>                                 
            </div>
            <div class="box-body"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">                             
           <div style="display:inline-block;width:100%;overflow-y:auto;">
					<ul class="rcs-timeline rcs-timeline-horizontal">
						<li class="rcs-timeline-item">
							<div class="rcs-timeline-badge success"><i class="fa fa-laptop"></i></div>
							<div class="rcs-timeline-panel">
								<div class="rcs-timeline-heading">
									<div class="rcs-timeline-title">Submit เข้าระบบ</div>
									 
								</div>
								<div class="rcs-timeline-body">
									<p><i class="fa fa-history"></i>  <% =String.Concat(row("SubmitDate")) %></p>
								</div>
							</div>
						</li>
						<li class="rcs-timeline-item">
							<div class="rcs-timeline-badge success"><i class="fa fa-wrench"></i></div>
							<div class="rcs-timeline-panel">
								<div class="rcs-timeline-heading">
									<div class="rcs-timeline-title">User ดำเนินการ</div>
								</div>
								<div class="rcs-timeline-body">
									<p><i class="fa fa-history"></i>  <% =String.Concat(row("ActionDate")) %></p>
								</div>
							</div>
						</li>
					 
						<li class="rcs-timeline-item">
							<div class="rcs-timeline-badge success"><i class="fa fa-check"></i></div>
							<div class="rcs-timeline-panel">
								<div class="rcs-timeline-heading">
									<div class="rcs-timeline-title">Approver อนุมัติ</div>
									 
								</div>
                <div class="rcs-timeline-body">
									<p><i class="fa fa-history"></i>  <% =String.Concat(row("ApproveDate")) %></p>
								</div>
							
							</div>
						</li>
					  
					</ul>
			 </div>     
                         </div>
                        </div>  
                </div>
                 <div class="row">
                    <div class="col-md-12">
                         <table style="width: 100%;font-size:10px;border-collapse: separate;">
                            <tr>
                                <td style="width: 150px;">หน่วยงานที่รับผิดชอบ</td>
                                <td class="textinfo textinfo-primary"> <% =String.Concat(row("DeptCode")) %></td> 
                            </tr>
                            <tr>
                                <td>ผลการปฏิบัติ/การติดตาม/วัดผล</td>
                                <td class="textinfo textinfo-primary">
                                    <% =String.Concat(row("ActionDescriptions")) %>
                                     <% If String.Concat(row("FilePath")) <> "" Then %>
                      <a href="Documents/Legal/<% =String.Concat(row("FilePath"))  %>" target="_blank" class="font-icon-button"><i class="fa fa-file-pdf text-primary" aria-hidden="true"></i></a>
                        <% End If %>
                                </td> 
                            </tr>
                            <tr>
                                <td>ผลการประเมินความสอดคล้อง</td>
                                <td class="textinfo textinfo-info"> <% =String.Concat(row("AsmResultName")) %></td> 
                            </tr>
                        </table>
                        </div>  
                      
                </div>

            </div> 
          </div> 
</div>
                  </td>
                  <td style="vertical-align:top !important"><% =String.Concat(row("RecurrenceText")) %></td>
                  <td style="vertical-align:top !important"><% =String.Concat(row("DeptCode")) %></td>                              
                </tr>          
 
            <%  Next %>
  
                </tbody>               
              </table>
            </div>
   </div>                                                           </div>
                            <div class="box-footer clearfix">
                            </div>
                        </div>                      
      
  

                    </section>
                </div>

               

            </section>
        </asp:Content>