﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class LawReleaseList
    Inherits System.Web.UI.Page
    Public dtLg As New DataTable
    Dim ctlL As New LawController
    'Dim ctlM As New MasterController
    'Private Const UploadDirectory As String = "~/imgLegal/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then

            'Request.Cookies("iLaw")("Legalimg1") = ""
            'Request.Cookies("iLaw")("picname1") = ""
            'Request.Cookies("iLaw")("Legalimg2") = ""
            'Request.Cookies("iLaw")("picname2") = ""
            'Request.Cookies("iLaw")("Legalimg3") = ""
            'Request.Cookies("iLaw")("picname3") = ""

            LoadLegalData()

        End If


    End Sub

    Private Sub LoadLegalData()
        Dim dt As New DataTable
        dt = ctlL.Legal_GetByUID(Request("lid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdLegalUID.Value = String.Concat(.Item("UID"))
                lblLegalCode.Text = String.Concat(.Item("Code"))
                lblLegalName.Text = String.Concat(.Item("LegalName"))
                LoadPracticeToGrid()
            End With
        Else
        End If
    End Sub


    Private Sub LoadPracticeToGrid()
        dtLg = ctlL.Practice_GetForAssessment(StrNull2Zero(hdLegalUID.Value), Request.Cookies("iLaw")("UserID"), Request.Cookies("iLaw2")("PeriodID"))
        'With grdPractice
        '    .Visible = True
        '    .DataSource = dt
        '    .DataBind()

        '    For i = 0 To .Rows.Count - 1
        '        .Rows(i).Cells(5).Text = Replace(Replace(.Rows(i).Cells(5).Text, "[", "<div class='badge badge-warning'>"), "]", "</div>")
        '    Next
        'End With
    End Sub

End Class