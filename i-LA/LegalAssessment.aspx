﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LegalAssessment.aspx.vb"
    Inherits="iLA.LegalAssessment" %>

    <%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
        namespace="DevExpress.Web" tagprefix="dx" %>

        <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
   
        </asp:Content>
        <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div class="page-title-icon">
                            <i class="pe-7s-users icon-gradient bg-mean-fruit"></i>
                        </div>
                        <div>บันทึกผลการปฏิบัติ 
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">
             

                        <div class="main-card mb-3 card">
                            <div class="card-header">ข้อมูลกฎหมาย<asp:HiddenField ID="hdLegalUID" runat="server" />
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>รหัสกฎหมาย</label>
                                            <asp:label ID="lblCode" runat="server" cssclass="form-control text-center" placeholder="Code"></asp:label>
                                        </div>
                                    </div>                               
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>ชื่อกฎหมาย</label>
                                            <asp:label ID="lblName" runat="server" cssclass="form-control" placeholder="Legal name"></asp:label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row"> 
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>ประเภทกฎหมาย</label>
                                            <asp:label ID="lblType" runat="server"  cssclass="form-control"> 
                                            </asp:label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>กฎหมายแม่บท</label>
                                            <asp:label ID="lblLawMaster" runat="server"  cssclass="form-control" Width="100%">
                                            </asp:label>
                                        </div>

                                    </div>                                                                
                                   
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>พื้นที่บังคับใช้</label>
                                            <asp:label ID="lblArea" runat="server"
                                                cssclass="form-control">
                                            </asp:label>
                                        </div>

                                    </div>
                                        </div>
                                      
                                  <div class="row"> 
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label>หน่วยงาน</label>
                                            <asp:label ID="lblMinistry" runat="server" cssclass="form-control">
                                            </asp:label>
                                        </div>
                                    </div>                     
                                
    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>วันที่กฎหมายออก</label>
                                            <div class="input-group">                                             
                                                <asp:label ID="lblIssueDate" runat="server" CssClass="form-control"      ></asp:label>
                                             
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>วันที่มีผลบังคับ</label>
                                            <div class="input-group">                                               
                                                <asp:label ID="lblStartDate" runat="server" CssClass="form-control"></asp:label>
                                                
                                            </div>
                                        </div>
                                    </div>                                
                                     </div>        
                            </div>
                            <div class="d-block text-right card-footer">

                            </div>
                        </div>
          
                              

                            <div class="main-card mb-3 card">
                            <div class="card-header">สาระสำคัญที่ต้องปฏิบัติ
                                   <asp:HiddenField ID="hdPracticeUID" runat="server" />
                                   </div>
                            <div class="card-body">
                             
                                <div class="row">
                               <div class="col-md-12">
                                        <div class="form-group">
                                            <label>สิ่งที่กฎหมายให้ปฏิบัติ</label>
                                            <div class="input-group">                                             
                                                <asp:label ID="lblPracticeDescription" runat="server" CssClass="form-control" Height="100px" TextMode="MultiLine"></asp:label>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>ความถี่</label>
                                            <div class="input-group">                                         
                                                 <asp:label ID="lblRecurence" runat="server"  CssClass="form-control"  > 
                                            </asp:label>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Duedate</label>
                                            <div class="input-group">                                             
                                                 <asp:label ID="lblDuedate" runat="server" CssClass="form-control text-center"></asp:label>                                              
                                            </div>
                                        </div>
                                    </div>  
                              
                                </div>
                          
                                   <div class="row">   
                    <div class="col-md-12 text-center">
                       
                    </div>
                </div>
                               

                   </div>                        
                        </div>
              

               
                         <div class="box box-success">
                        
                            <div class="box-body">
                             
                                <div class="row">
                              <div class="col-md-12">
                                        <div class="form-group">
                                            <label>บันทึกผลการปฏิบัติ<asp:HiddenField ID="hdActionUID" runat="server" />
                                            </label>
                                            &nbsp;<div class="input-group">                                         
                                                <asp:TextBox ID="txtActionDescription" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100px" MaxLength="1000"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>  

                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <label>เอกสาร/รูปภาพประกอบการปฏิบัติ</label>
                                      <asp:FileUpload ID="FileUpload1" runat="server" Width="80%" /> 
                                        <br />  <br />
                                            <asp:HyperLink ID="hlnkDoc" runat="server" CssClass="font-icon-button fa fa-file-pdf" Target="_blank"> &nbsp;ดูเอกสารหลักฐาน
                                            </asp:HyperLink> 
                                        </div>
                                    </div> 
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ผลประเมินความสอดคล้อง</label>
                                            <div class="input-group">                                             
                                                   <asp:RadioButtonList ID="optResult" runat="server"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Value="Y">สอดคล้อง</asp:ListItem>
                                                <asp:ListItem Value="N">ไม่สอดคล้อง</asp:ListItem>
                                            </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>  
                                    
                                </div>
                          
                                   <div class="row">   
                    <div class="col-md-12 text-center">
                        <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="บันทึก" Width="120px" />
                           <asp:Button ID="cmdBack" CssClass="btn btn-secondary" runat="server" Text="ย้อนกลับหน้ารายการ" />
                         <asp:Button ID="cmdSend" CssClass="btn btn-success" runat="server" Text="ส่ง Approver พิจารณา" />
                    </div>
                </div>
                               

                   </div>                        
                        </div>                

                     

            </section>
        </asp:Content>