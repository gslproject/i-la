﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class LawPractice
    Inherits System.Web.UI.Page
    Public dtLg As New DataTable
    Dim ctlL As New LawController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadLegalData()
        End If
    End Sub

    Private Sub LoadLegalData()
        Dim dt As New DataTable
        dt = ctlL.Legal_GetByUID(Request("lid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdLegalUID.Value = String.Concat(.Item("UID"))
                lblLegalCode.Text = String.Concat(.Item("Code"))
                lblLegalName.Text = String.Concat(.Item("LegalName"))
                LoadPracticeToGrid()
            End With
        Else
        End If
    End Sub

    Private Sub LoadPracticeToGrid()
        dtLg = ctlL.Practice_GetAssessmentResult(Request.Cookies("iLaw")("LoginCompanyUID"), Request.Cookies("iLaw2")("PeriodID"), StrNull2Zero(hdLegalUID.Value))
    End Sub

End Class