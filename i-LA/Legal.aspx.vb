﻿Imports System.Data
Public Class Legal
    Inherits System.Web.UI.Page
    Dim ctlL As New LawController
    Public dtLegal As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Or IsNothing(Request.Cookies("iLaw2")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadLegal()
        End If
    End Sub
    Private Sub LoadLegal()

        If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_SPA")) = True Then
            dtLegal = ctlL.Legal_Get()
        Else
            dtLegal = ctlL.Legal_GetByCompany(Request.Cookies("iLaw")("LoginCompanyUID"), Request.Cookies("iLaw2")("PeriodID"))
            'If Request("m") = "apv" Then
            '    dtLegal = ctlL.Legal_GetByStatus(ACTION_WAIT)
            'Else

            'End If
        End If
    End Sub

End Class