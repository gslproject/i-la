﻿Imports System.Data
Public Class LegalList
    Inherits System.Web.UI.Page
    Dim ctlL As New LawController
    Public dtLegal As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            Select Case Request("s")
                Case "asm"
                    lblTitle.Text = "รายการกฎหมายที่ต้องประเมินความสอดคล้อง"
                    If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_USR")) = True Then
                        dtLegal = ctlL.Legal_GetForAssessment(Request.Cookies("iLaw")("LoginCompanyUID"), Request.Cookies("iLaw")("UserID"), Request.Cookies("iLaw2")("PeriodID"))
                        'Else
                        '    If Request("m") = "apv" Then
                        '        dtLegal = ctlL.Legal_GetByStatus(ACTION_WAIT)
                        '    Else
                        '        dtLegal = ctlL.Legal_Get()
                        '    End If
                    End If
                Case "apv"
                    lblTitle.Text = "รายการกฎหมายที่ต้องอนุมัติความสอดคล้อง"
                    If Convert.ToBoolean(Request.Cookies("iLaw")("ROLE_APP")) = True Then
                        dtLegal = ctlL.Legal_GetForApproval(Request.Cookies("iLaw")("LoginCompanyUID"), Request.Cookies("iLaw")("UserID"), Request.Cookies("iLaw2")("PeriodID"))
                    End If
                Case Else
                    Response.Redirect("Home.aspx")
            End Select

        End If
    End Sub
End Class