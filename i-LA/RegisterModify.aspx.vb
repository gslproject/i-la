﻿
Public Class RegisterModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlM As New MasterController
    Dim ctlStd As New RegisterController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadBusinessTypeToDDL()
            LoadProvinceToDDL()

            If Not Request("pid") = Nothing Then
                LoadRegisterData()
            End If
        End If

        'txtGPAX.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
    End Sub
    Private Sub LoadBusinessTypeToDDL()
        dt = ctlM.BusinessType_Get
        If dt.Rows.Count > 0 Then
            With ddlBusinessType
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "UID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadProvinceToDDL()
        dt = ctlM.Province_Get
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadRegisterData()
        dt = ctlStd.Register_GetByID(Request("pid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdRegisterID.Value = String.Concat(.Item("UID"))
                optPersonType.SelectedValue = String.Concat(.Item("PersonType"))
                txtContactName.Text = DBNull2Str(.Item("ContactName"))
                txtCompanyName.Text = DBNull2Str(.Item("CompanyName"))
                ddlBusinessType.SelectedValue = .Item("BusinessTypeID")
                txtTel.Text = String.Concat(.Item("Tel"))
                txtMail.Text = String.Concat(.Item("Email"))
                txtAddressNo.Text = String.Concat(.Item("AddressNo"))
                txtMoo.Text = String.Concat(.Item("Moo"))
                txtSoi.Text = String.Concat(.Item("Soi"))
                txtRoad.Text = String.Concat(.Item("Road"))
                txtTumbol.Text = String.Concat(.Item("Subdistrict"))
                txtCity.Text = String.Concat(.Item("District"))
                ddlProvince.SelectedValue = .Item("ProvinceID")
                txtZipcode.Text = String.Concat(.Item("Zipcode"))
                txtWebsite.Text = String.Concat(.Item("Website"))
                txtAddress4Inv.Text = String.Concat(.Item("AddressInvoice"))

                If String.Concat(.Item("ApproveStatus")) = "Y" Then
                    cmdSave.Visible = False
                    lblAlert.Visible = True
                Else
                    lblAlert.Visible = True
                    lblAlert.Visible = False
                End If
            End With
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtCompanyName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อบริษัทก่อน');", True)
            Exit Sub
        End If
        If txtTel.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุเบอร์โทรสำหรับติดต่อก่อน');", True)
            Exit Sub
        End If
        Dim ctlC As New CompanyController
        ctlC.Company_AddFromRegister(0, StrNull2Zero(hdRegisterID.Value), txtCompanyName.Text, txtAddressNo.Text, txtSoi.Text, txtRoad.Text, txtTumbol.Text, txtCity.Text, ddlProvince.SelectedValue, txtZipcode.Text, "TH", txtTel.Text, txtMail.Text, txtWebsite.Text, StrNull2Zero(ddlBusinessType.SelectedValue), Request.Cookies("iLaw")("userid"), txtAddress4Inv.Text, txtContactName.Text)

        lblAlert.Visible = True
        lblAlert.Visible = False


        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','สร้าง Customer Company เรียบร้อย');", True)

    End Sub

End Class