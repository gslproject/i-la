﻿
Public Class PeriodTime
    Inherits System.Web.UI.Page

    Dim ctlM As New MasterController
    Dim dt As New DataTable
    Dim acc As New UserController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("iLaw")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            ClearData()
            txtUID.Text = ""
            LoadPeriodTimeToGrid()
        End If

    End Sub

    Private Sub LoadPeriodTimeToGrid()

        If Trim(txtSearch.Text) <> "" Then
            dt = ctlM.PeriodTime_GetBySearch(Request.Cookies("iLaw")("LoginCompanyUID"), txtSearch.Text)
        Else
            dt = ctlM.PeriodTime_GetAll(Request.Cookies("iLaw")("LoginCompanyUID"))
        End If
        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With

    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlM.PeriodTime_Delete(e.CommandArgument) Then

                        acc.User_GenLogfile(Request.Cookies("iLaw")("username"), ACTTYPE_DEL, "PeriodTime", "ลบชื่อหน่วยงาน :" & txtName.Text, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadPeriodTimeToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)

                    End If


            End Select


        End If
    End Sub
    Private Sub EditData(ByVal pID As String)
        dt = ctlM.PeriodTime_GetByUID(pID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                Me.txtUID.Text = DBNull2Str(dt.Rows(0)("PeriodID"))
                txtName.Text = DBNull2Str(dt.Rows(0)("PeriodName"))
                txtStartDate.Text = String.Concat(dt.Rows(0)("StartDate"))
                txtEndDate.Text = DBNull2Str(dt.Rows(0)("EndDate"))
                chkStatus.Checked = ConvertStatusFlag2CHK(dt.Rows(0)("StatusFlag"))
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub ClearData()
        txtUID.Text = ""
        txtUID.Text = ""
        txtName.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        chkStatus.Checked = True
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d9edf7';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลให้ครบถ้วน');", True)
            Exit Sub
        End If

        Dim Bdate, Edate As String

        Bdate = ConvertStrDate2DBDate(txtStartDate.Text)
        Edate = ConvertStrDate2DBDate(txtEndDate.Text)


        Dim item As Integer

        If txtUID.Text = "" Then
            If ctlM.PeriodTime_CheckDuplicate(Request.Cookies("iLaw")("LoginCompanyUID"), txtName.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ชื่อหน่วยงานนี้มีอยู่่ในระบบแล้ว');", True)
                Exit Sub
            End If
            item = ctlM.PeriodTime_Add(StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), txtName.Text, Bdate, Edate, ConvertBoolean2StatusFlag(chkStatus.Checked))
        Else
            item = ctlM.PeriodTime_Update(StrNull2Zero(txtUID.Text), StrNull2Zero(Request.Cookies("iLaw")("LoginCompanyUID")), txtName.Text, Bdate, Edate, ConvertBoolean2StatusFlag(chkStatus.Checked))
        End If


        LoadPeriodTimeToGrid()
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','บันทึกข้อมูลเรียบร้อย');", True)


    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        LoadPeriodTimeToGrid()
    End Sub

    Protected Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadPeriodTimeToGrid()
    End Sub
End Class

